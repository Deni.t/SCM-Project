﻿namespace FormBuatSPK
{
    partial class FormSPK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePickerTgl = new System.Windows.Forms.DateTimePicker();
            this.textBoxKodeProd = new System.Windows.Forms.TextBox();
            this.comboBoxKodPem = new System.Windows.Forms.ComboBox();
            this.labelTgl = new System.Windows.Forms.Label();
            this.labelJam = new System.Windows.Forms.Label();
            this.buttonBuatSPK = new System.Windows.Forms.Button();
            this.textBoxSyarat = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxLama = new System.Windows.Forms.TextBox();
            this.textBoxBiaya = new System.Windows.Forms.TextBox();
            this.textBoxPeker = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxPJ = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxAlamPem = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxNamPem = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxNoSpk = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // dateTimePickerTgl
            // 
            this.dateTimePickerTgl.Location = new System.Drawing.Point(575, 349);
            this.dateTimePickerTgl.Name = "dateTimePickerTgl";
            this.dateTimePickerTgl.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerTgl.TabIndex = 59;
            // 
            // textBoxKodeProd
            // 
            this.textBoxKodeProd.Location = new System.Drawing.Point(213, 101);
            this.textBoxKodeProd.Name = "textBoxKodeProd";
            this.textBoxKodeProd.Size = new System.Drawing.Size(100, 20);
            this.textBoxKodeProd.TabIndex = 58;
            // 
            // comboBoxKodPem
            // 
            this.comboBoxKodPem.FormattingEnabled = true;
            this.comboBoxKodPem.Location = new System.Drawing.Point(270, 141);
            this.comboBoxKodPem.Name = "comboBoxKodPem";
            this.comboBoxKodPem.Size = new System.Drawing.Size(161, 21);
            this.comboBoxKodPem.TabIndex = 57;
            this.comboBoxKodPem.SelectedIndexChanged += new System.EventHandler(this.comboBoxKodPem_SelectedIndexChanged_1);
            // 
            // labelTgl
            // 
            this.labelTgl.AutoSize = true;
            this.labelTgl.BackColor = System.Drawing.Color.Transparent;
            this.labelTgl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelTgl.Location = new System.Drawing.Point(11, 448);
            this.labelTgl.Name = "labelTgl";
            this.labelTgl.Size = new System.Drawing.Size(22, 13);
            this.labelTgl.TabIndex = 55;
            this.labelTgl.Text = "Tgl";
            // 
            // labelJam
            // 
            this.labelJam.AutoSize = true;
            this.labelJam.BackColor = System.Drawing.Color.Transparent;
            this.labelJam.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelJam.Location = new System.Drawing.Point(11, 433);
            this.labelJam.Name = "labelJam";
            this.labelJam.Size = new System.Drawing.Size(26, 13);
            this.labelJam.TabIndex = 56;
            this.labelJam.Text = "Jam";
            // 
            // buttonBuatSPK
            // 
            this.buttonBuatSPK.Location = new System.Drawing.Point(285, 417);
            this.buttonBuatSPK.Name = "buttonBuatSPK";
            this.buttonBuatSPK.Size = new System.Drawing.Size(132, 31);
            this.buttonBuatSPK.TabIndex = 53;
            this.buttonBuatSPK.Text = "Buat SPK";
            this.buttonBuatSPK.UseVisualStyleBackColor = true;
            this.buttonBuatSPK.Click += new System.EventHandler(this.buttonBuatSPK_Click_1);
            // 
            // textBoxSyarat
            // 
            this.textBoxSyarat.Location = new System.Drawing.Point(213, 374);
            this.textBoxSyarat.Name = "textBoxSyarat";
            this.textBoxSyarat.Size = new System.Drawing.Size(562, 20);
            this.textBoxSyarat.TabIndex = 52;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(60, 374);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 20);
            this.label10.TabIndex = 51;
            this.label10.Text = "Syarat :";
            // 
            // textBoxLama
            // 
            this.textBoxLama.Location = new System.Drawing.Point(213, 349);
            this.textBoxLama.Name = "textBoxLama";
            this.textBoxLama.Size = new System.Drawing.Size(204, 20);
            this.textBoxLama.TabIndex = 50;
            // 
            // textBoxBiaya
            // 
            this.textBoxBiaya.Location = new System.Drawing.Point(213, 317);
            this.textBoxBiaya.Name = "textBoxBiaya";
            this.textBoxBiaya.Size = new System.Drawing.Size(562, 20);
            this.textBoxBiaya.TabIndex = 49;
            // 
            // textBoxPeker
            // 
            this.textBoxPeker.Location = new System.Drawing.Point(213, 287);
            this.textBoxPeker.Name = "textBoxPeker";
            this.textBoxPeker.Size = new System.Drawing.Size(562, 20);
            this.textBoxPeker.TabIndex = 48;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(60, 347);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 20);
            this.label9.TabIndex = 47;
            this.label9.Text = "Lama :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(457, 347);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(115, 20);
            this.label8.TabIndex = 46;
            this.label8.Text = "Tanggal Mulai :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(60, 317);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 20);
            this.label7.TabIndex = 45;
            this.label7.Text = "Biaya :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(60, 287);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 20);
            this.label6.TabIndex = 44;
            this.label6.Text = "Pekerjaan :";
            // 
            // comboBoxPJ
            // 
            this.comboBoxPJ.FormattingEnabled = true;
            this.comboBoxPJ.Location = new System.Drawing.Point(270, 254);
            this.comboBoxPJ.Name = "comboBoxPJ";
            this.comboBoxPJ.Size = new System.Drawing.Size(161, 21);
            this.comboBoxPJ.TabIndex = 43;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(60, 255);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(204, 20);
            this.label5.TabIndex = 42;
            this.label5.Text = "Nama Penanggung Jawab :";
            // 
            // textBoxAlamPem
            // 
            this.textBoxAlamPem.Location = new System.Drawing.Point(213, 210);
            this.textBoxAlamPem.Name = "textBoxAlamPem";
            this.textBoxAlamPem.Size = new System.Drawing.Size(562, 20);
            this.textBoxAlamPem.TabIndex = 41;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(60, 208);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(127, 20);
            this.label4.TabIndex = 40;
            this.label4.Text = "Alamat Pembeli :";
            // 
            // textBoxNamPem
            // 
            this.textBoxNamPem.Location = new System.Drawing.Point(213, 178);
            this.textBoxNamPem.Name = "textBoxNamPem";
            this.textBoxNamPem.Size = new System.Drawing.Size(562, 20);
            this.textBoxNamPem.TabIndex = 39;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(60, 141);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 20);
            this.label1.TabIndex = 38;
            this.label1.Text = "Kode Pembeli";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(60, 99);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 20);
            this.label11.TabIndex = 37;
            this.label11.Text = "Kode Produk";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(60, 176);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 20);
            this.label3.TabIndex = 36;
            this.label3.Text = "Nama Pembeli :";
            // 
            // textBoxNoSpk
            // 
            this.textBoxNoSpk.Location = new System.Drawing.Point(213, 53);
            this.textBoxNoSpk.Name = "textBoxNoSpk";
            this.textBoxNoSpk.Size = new System.Drawing.Size(562, 20);
            this.textBoxNoSpk.TabIndex = 35;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(60, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 20);
            this.label2.TabIndex = 34;
            this.label2.Text = "No. SPK :";
            // 
            // FormSPK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(803, 512);
            this.Controls.Add(this.dateTimePickerTgl);
            this.Controls.Add(this.textBoxKodeProd);
            this.Controls.Add(this.comboBoxKodPem);
            this.Controls.Add(this.labelTgl);
            this.Controls.Add(this.labelJam);
            this.Controls.Add(this.buttonBuatSPK);
            this.Controls.Add(this.textBoxSyarat);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBoxLama);
            this.Controls.Add(this.textBoxBiaya);
            this.Controls.Add(this.textBoxPeker);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.comboBoxPJ);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxAlamPem);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxNamPem);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxNoSpk);
            this.Controls.Add(this.label2);
            this.Name = "FormSPK";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSPK_FormClosing);
            this.Load += new System.EventHandler(this.FormSPK_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePickerTgl;
        private System.Windows.Forms.TextBox textBoxKodeProd;
        private System.Windows.Forms.ComboBox comboBoxKodPem;
        private System.Windows.Forms.Label labelTgl;
        private System.Windows.Forms.Label labelJam;
        private System.Windows.Forms.Button buttonBuatSPK;
        private System.Windows.Forms.TextBox textBoxSyarat;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxLama;
        private System.Windows.Forms.TextBox textBoxBiaya;
        private System.Windows.Forms.TextBox textBoxPeker;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBoxPJ;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxAlamPem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxNamPem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxNoSpk;
        private System.Windows.Forms.Label label2;
    }
}

