﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormBuatSPK
{
    public class Jabatan
    {
        private string kode;
        private string nama;

        public string Kode
        {
            get { return kode; }
            set { kode = value; }
        }

        public string Nama
        {
            get { return nama; }
            set { nama = value; }
        }

        public Jabatan()
        {
            kode = "";
            nama = "";
        }
        public Jabatan(string pKode, string pNama)
        {
            kode = pKode;
            nama = pNama;
        }
    }
}
