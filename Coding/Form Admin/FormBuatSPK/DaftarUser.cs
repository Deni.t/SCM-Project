﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace FormBuatSPK
{
    public class DaftarUser
    {
        private List<User> listUser;

        public List<User> DaftarListUser
        {
            get { return listUser; }
        }

        public int JumlahUser
        {
            get { return listUser.Count; }
        }

        public DaftarUser()
        {
            listUser = new List<User>();
        }

        public string BacaUser()
        {
            Koneksi k = new Koneksi();
            k.Connect();

            string sql = "SELECT u.*,j.namaJabatan " +
                         "FROM userti u inner join jabatan j on u.kodejabatan = j.kodejabatan";
            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB);

            try
            {
                MySqlDataReader data = c.ExecuteReader();

                while (data.Read() == true)
                {
                    string id = data.GetValue(0).ToString();
                    string nama = data.GetValue(1).ToString();
                    string alamat = data.GetValue(2).ToString();
                    string telp = data.GetValue(3).ToString();
                    string username = data.GetValue(4).ToString();
                    string pass = data.GetValue(5).ToString();
                    Jabatan j = new Jabatan(data.GetValue(6).ToString(),
                                            data.GetValue(7).ToString());
                    User u = new User(id, nama, alamat, telp, username, pass, j);
                    DaftarListUser.Add(u);
                }
                c.Dispose();
                data.Dispose();
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}
