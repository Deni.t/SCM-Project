﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin
{
    public partial class Admin_FormPenggunaanBahanBaku : Form
    {
        string admin_username;
        string admin_kategori;
        int stok = 0;
        public Admin_FormPenggunaanBahanBaku()
        {
            InitializeComponent();
        }

        private void buttonSimpan_Click(object sender, EventArgs e)
        {
            var loadString = @"server=localhost;database=scm;userid=root;password=;";
            using (var connection = new MySqlConnection(loadString))
            {
                connection.Open();
                var query = "SELECT stok from bahanbaku WHERE kodeBahanBaku='" + comboBoxKode.Text + "'";
                using (var command = new MySqlCommand(query, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            stok = reader.GetInt32("stok");
                        }
                    }
                }

                int sisa = stok - int.Parse(textBoxJumlah.Text);
                string date = DateTime.Now.ToString("yyyy-MM-dd");

                var query1 = "INSERT INTO formpenggunaandetil(noSPK,kodeBahanBaku,jumlahKeluar,sisaPenggunaan,tanggalKeluar) VALUES('" + comboBoxSPK.Text + "','" + comboBoxKode.Text + "','" + textBoxJumlah.Text + "','" + sisa + "','" + date + "')";
                MySqlCommand cmd1 = new MySqlCommand(query1, connection);
                cmd1.ExecuteNonQuery();

                var query2 = "UPDATE bahanbaku SET stok='" + sisa + "'" + "WHERE kodeBahanBaku='" + comboBoxKode.Text + "'";
                MySqlCommand cmd2 = new MySqlCommand(query2, connection);
                cmd2.ExecuteNonQuery();

                loadDataGrid();
            }
        }
         public void loadDataGrid()
        {
            //load datagrid form penggunaan detil
            String loadString = @"server=localhost;database=scm;userid=root;password=;";
            MySqlConnection connection = new MySqlConnection(loadString);
            try
            {
                connection.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * from formpenggunaandetil", connection);
                DataTable t = new DataTable();
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(t);
                dataGridView1.DataSource = t;
                connection.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Fail");
            }
        }
        private void Admin_FormPenggunaanBahanBaku_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            labelDateTime.Text = DateTime.Now.ToString("yyyy-MM-dd/hh:mm:ss");
            labelUser.Text = admin_username + " - " + admin_kategori;


            String loadString = @"server=localhost;database=scm;userid=root;password=;";
            MySqlConnection connection = new MySqlConnection(loadString);

            //isi combobox kode bahan baku
            try
            {
                connection.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT kodeBahanBaku,nama from bahanbaku", connection);
                MySqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    string kode = rdr.GetString("kodeBahanBaku");
                    comboBoxKode.Items.Add(kode);
                    comboBoxKode.SelectedIndex = 0;
                }
                connection.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Fail");
            }

           
            //isi combobox no spk
            try
            {
                connection.Open();
                MySqlCommand cmd = new MySqlCommand("Select noSPK from SPK", connection);
                MySqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    comboBoxSPK.Items.Add(rdr["noSPK"]);
                }
                connection.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Fail");
            }

            loadDataGrid();

            
        }

        private void comboBoxKode_SelectedIndexChanged(object sender, EventArgs e)
        {
            //isi textbox nama bahan baku
            String loadString = @"server=localhost;database=scm;userid=root;password=;";
            MySqlConnection connection = new MySqlConnection(loadString);
            try
            {
                connection.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT nama from bahanbaku where kodeBahanBaku='" + comboBoxKode.Text + "'", connection);
                MySqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    textBoxNamaBahan.Text = rdr.GetString("nama");
                }
                connection.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Fail");
            }
        }

        private void buttonKembali_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
    }
}
