﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.OleDb;

namespace Admin
{
    public partial class TambahBahanBaku : Form
    {
        MySqlConnection conn = connectionService.getConnection();
        string u, k;
        public TambahBahanBaku(string u, string k)
        {
            InitializeComponent();
            this.u = u;
            this.k = k;
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            try
            {
                string loadstring = @"server=localhost;database=scm;userid=root;password=;";
                MySqlConnection conDataBase = new MySqlConnection(loadstring);
                string sql = "INSERT INTO bahanbaku (kodeBahanBaku, nama, satuan) VALUES ('" + this.txtKodeBahan.Text + "','" + this.txtNamaBahan.Text + "','" + this.txtSatuan.Text + "');";
                MySqlConnection MyConn2 = new MySqlConnection(loadstring);
                MySqlCommand MyCommand2 = new MySqlCommand(sql, MyConn2);
                MySqlDataReader MyReader2;
                MyConn2.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                MessageBox.Show("Bahan Baku BERHASIL ditambahkan");
            }
            catch (Exception)
            {
                MessageBox.Show("Kode Bahan telah ada!");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Admin_FormHome fh = new Admin_FormHome(u, k);
            fh.ShowDialog();
        }

        private void TambahBahanBaku_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            label5.Text = u + " - " + k;
        }
    }
}
