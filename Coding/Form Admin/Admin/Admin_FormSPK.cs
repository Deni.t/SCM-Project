﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Admin
{
    public partial class Admin_FormSPK : Form
    {
        string user, kategori;
        public Admin_FormSPK()
        {
            InitializeComponent();
        }
        public Admin_FormSPK(string a, string b)
        {
            InitializeComponent();
            timer1.Start();
            this.user = a;
            this.kategori = b;

        }

        private void btnStatusSPK_Click(object sender, EventArgs e)
        {
            FormStatusSPK formStatusSPK = new FormStatusSPK(user, kategori);
            formStatusSPK.ShowDialog();
            this.Hide();
        }

        private void btnHapusSPK_Click(object sender, EventArgs e)
        {
            FormHapusSPK formHapusSPK = new FormHapusSPK(user, kategori);
            formHapusSPK.ShowDialog();
            this.Hide();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            this.labelDate.Text = dt.ToString();
        }

        private void Admin_FormSPK_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            labelUser.Text = FormConnect.username + " - " + FormConnect.kategori_user;
        }

        private void buttonBuatSPK_Click(object sender, EventArgs e)
        {
            FormBuatSPK.FormSPK f1 = new FormBuatSPK.FormSPK();
            f1.ShowDialog();
        }
    }
}
