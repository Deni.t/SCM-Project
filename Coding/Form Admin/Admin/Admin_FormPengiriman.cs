﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Admin
{
    public partial class Admin_FormPengiriman : Form
    {
        string admin_username;
        string admin_kategori;
        string[] tampung;

        public Admin_FormPengiriman(string u, string k)
        {
            InitializeComponent();
            this.admin_username = u;
            this.admin_kategori = k;
        }

        private void Admin_FormPengiriman_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            labelDateTime.Text = DateTime.Now.ToString("yyyy-MM-dd/hh:mm:ss");
            labelUser.Text = admin_username + " - " + admin_kategori;
            var loadstring = @"server=localhost;database=scm;userid=root;password=;";
            using (var connection = new MySqlConnection(loadstring))
            {
                connection.Open();

                var queryNoSPK = "SELECT noSPK from spk";
                using (var command1 = new MySqlCommand(queryNoSPK, connection))
                {
                    using (var reader1 = command1.ExecuteReader())
                    {
                        string a = "";
                        while (reader1.Read())
                        {
                            a = reader1.GetString("noSPK");
                            cmbSpk.Items.Add(a);
                            cmbSpk.SelectedIndex = -1;
                        }
                    }
                }
            }
        }

        private void btnKembali_Click(object sender, EventArgs e)
        {
            this.Hide();
            Admin_FormHome o = new Admin_FormHome(admin_username, admin_kategori);
            o.ShowDialog();
        }

        private void cmbSpk_SelectedIndexChanged(object sender, EventArgs e)
        {
            //select s.noSPK, s.tanggalMulai, s.pekerjaan, b.tanggal as TanggalPengiriman, c.nama as NamaCustomer, c.alamat as AlamatPengiriman
            //from spk s inner join buktipengiriman b on s.noSPK = b.noSPK inner join customer c on c.kodeCustomer = b.kodeCustomer
            //where s.noSPK = 1
            var loadstring = @"server=localhost;database=scm;userid=root;password=;";
            using (var connection = new MySqlConnection(loadstring))
            {
                connection.Open();
                

                var queryNoSPK = "select s.noSPK, s.tanggalMulai, s.pekerjaan, b.tanggal as TanggalPengiriman, c.nama as NamaCustomer, c.alamat as AlamatPengiriman from spk s inner join buktipengiriman b on s.noSPK = b.noSPK inner join customer c on c.kodeCustomer = b.kodeCustomer where s.noSPK ='"+cmbSpk.SelectedItem+"'";
                using (var command1 = new MySqlCommand(queryNoSPK, connection))
                {
                    using (var reader1 = command1.ExecuteReader())
                    {
                        string a = "";
                        while (reader1.Read())
                        {
                            tampung = new string[6];
                            listBox1.Items.Add("No SPK : " + reader1.GetString("noSPK"));
                            listBox1.Items.Add("Tanggal Mulai Kerja : " + reader1.GetString("tanggalMulai"));
                            listBox1.Items.Add("Pekerjaan : " + reader1.GetString("pekerjaan"));
                            listBox1.Items.Add("Tanggal Pengiriman : " + reader1.GetString("TanggalPengiriman"));
                            listBox1.Items.Add("Nama Customer : " + reader1.GetString("NamaCustomer"));
                            listBox1.Items.Add("Alamat Pengiriman : " + reader1.GetString("AlamatPengiriman"));
                            //listBox1.Items.Add("No SPK : " + reader1.GetString("noSPK"));

                            tampung[0] = "No SPK : " + reader1.GetString("noSPK");
                            tampung[1] = "Tanggal Mulai Kerja : " + reader1.GetString("tanggalMulai");
                            tampung[2] = "Pekerjaan : " + reader1.GetString("pekerjaan");
                            tampung[3] = "Tanggal Pengiriman : " + reader1.GetString("TanggalPengiriman");
                            tampung[4] = "Nama Customer : " + reader1.GetString("NamaCustomer");
                            tampung[5] = "Alamat Pengiriman : " + reader1.GetString("AlamatPengiriman");
                        }
                    }
                }
            }

        }

        private void btnCetak_Click(object sender, EventArgs e)
        {
            Document doc = new Document(iTextSharp.text.PageSize.LETTER, 10, 10, 42, 35);
            PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream(cmbSpk.SelectedItem + ".pdf", FileMode.Create));
            doc.Open();
            for (int i = 0; i < tampung.Length; i++)
            {
                Paragraph para = new Paragraph(tampung[i]);
                doc.Add(para);
            }
            MessageBox.Show("Lihat di Debug untuk lihat PDF");
            doc.Close();
        }
    }
}
