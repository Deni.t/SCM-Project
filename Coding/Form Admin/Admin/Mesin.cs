﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Admin
{
    public class Mesin
    {
        private string noMesin;
        private string nama;

        public string NoMesin
        {
            get
            {
                return noMesin;
            }

            set
            {
                noMesin = value;
            }
        }

        public string Nama
        {
            get
            {
                return nama;
            }

            set
            {
                nama = value;
            }
        }

        public Mesin(string pNo, string pNama)
        {
            nama = pNama;
            noMesin = pNo;
        }

        public string tambahMesin()
        {
            Koneksi k = new Koneksi();
            k.Connect();

            string sql = "Insert into mesin(noMesin,nama) values ('" + noMesin + "','" + nama + "')";

            MySqlCommand cmd = new MySqlCommand(sql, k.KoneksiDB);

            try
            {
                cmd.ExecuteNonQuery();
                return "sukses";
            }
            catch(Exception e)
            {
                return e.Message;
            }
        }
    }
}
