﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;

using System.Configuration;
namespace WindowsFormsApplication1
{
    public class Koneksi
    {
        private MySqlConnection koneksi;
        private string namaServer;
        private string namaDataBase;
        private string username;
        private string password;

        #region Properties
        public string NamaServer
        {
            get { return namaServer; }
            set { namaServer = value; }
        }
        public string NamaDataBase
        {
            get { return namaDataBase; }
            set { namaDataBase = value; }
        }
        public string Username
        {
            get { return username; }
            set { username = value; }
        }
        public string Password
        {
            set { password = value; }
        }

        public MySqlConnection KoneksiDB
        {
            get { return koneksi; }
        }
        #endregion

        #region method
        public string Connect()
        {
            try
            {
                if (koneksi.State == System.Data.ConnectionState.Open)
                {
                    koneksi.Close();
                }
                koneksi.Open();
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public void UpdateAppConfig(string _stringKoneksi)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.ConnectionStrings.ConnectionStrings["KonfigurasiKoneksi"].ConnectionString = _stringKoneksi;
            config.Save(ConfigurationSaveMode.Modified, true);
            ConfigurationManager.RefreshSection("connectionStrings");
        }
        #endregion

        #region Constructor
        public Koneksi()
        {
            koneksi = new MySqlConnection();
            koneksi.ConnectionString = ConfigurationManager.ConnectionStrings["KonfigurasiKoneksi"].ConnectionString;
            string hasilConnect = Connect();
        }

        public Koneksi(string server, string db, string uid, string pass)
        {
            namaServer = server;
            namaDataBase = db;
            username = uid;
            password = pass;

            koneksi = new MySqlConnection();

            string koneksiString = "server=" + namaServer + "; database=" + namaDataBase + "; uid=" + username + "; pwd=" + password;
            koneksi.ConnectionString = koneksiString;
            string hasilConnect = Connect();

            if (hasilConnect == "sukses")
            {
                UpdateAppConfig(koneksiString);
            }
        }
        #endregion
    }
}
