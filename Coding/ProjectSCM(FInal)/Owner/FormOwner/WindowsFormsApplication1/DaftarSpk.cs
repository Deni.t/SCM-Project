﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
        
    public class DaftarSpk
    {
        private List<spk> listSpk;
        string noSpk = "";
        int status = 0;
        string namaPembeli = "";
        string alamatPembeli = "";
        string namaUser = "";
        string alamatUser = "";
        string telepon = "";
        string pekerjaan = "";
        int biaya = 0;
        int lama = 0;

        List<string> listofSpk = new List<string>();

        public List<spk> ListSpk
        {
            get { return listSpk; }
        }

        public int jumlahData
        {
            get { return listSpk.Count; }
        }
        public DaftarSpk()
        {
            listSpk = new List<spk>();
        }

        public string bacaData()
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "SELECT s.nospk,s.status, c.nama, c.alamat, u.nama, u.alamat, u.telepon, s.pekerjaan, s.biaya, s.lama "+
                "From spk s inner join userti u on s.idUser=u.idUser" +
                " inner join customer c on s.kodeCustomer=c.kodeCustomer";

            
            MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
            try
            {
                MySqlDataReader Data = MSC.ExecuteReader();
                while (Data.Read() == true)
                {
                    noSpk = Data.GetValue(0).ToString();
                     status= int.Parse(Data.GetValue(1).ToString());
                     namaPembeli=Data.GetValue(2).ToString();
                     alamatPembeli = Data.GetValue(3).ToString();
                     namaUser = Data.GetValue(4).ToString();
                     alamatUser = Data.GetValue(5).ToString();
                     telepon = Data.GetValue(6).ToString();
                     pekerjaan = Data.GetValue(7).ToString();
                     biaya = int.Parse(Data.GetValue(8).ToString());
                     lama = int.Parse(Data.GetValue(9).ToString());
                     spk spk = new spk(noSpk, status, namaPembeli, alamatPembeli, namaUser, alamatUser, telepon, pekerjaan, biaya, lama);
                     listSpk.Add(spk);
                }
                MSC.Dispose();
                Data.Dispose();
                return "sukses";
            }
            catch (Exception E)
            {
                return E.Message;
            }
        }

        public string CariData(string noSp)
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "SELECT s.nospk, s.status, c.nama, c.alamat, u.nama, u.alamat, u.telepon, s.pekerjaan, s.biaya, s.lama " +
                "From spk s inner join userti u on s.idUser=u.idUser" +
                " inner join customer c on s.kodeCustomer=c.kodeCustomer" + " where s.nospk= '" + noSp +"'";
;


            MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
            try
            {
                MySqlDataReader Data = MSC.ExecuteReader();
                while (Data.Read() == true)
                {

                    noSpk = Data.GetValue(0).ToString();
                    status = int.Parse(Data.GetValue(1).ToString());
                    namaPembeli = Data.GetValue(2).ToString();
                    alamatPembeli = Data.GetValue(3).ToString();
                    namaUser = Data.GetValue(4).ToString();
                    alamatUser = Data.GetValue(5).ToString();
                    telepon = Data.GetValue(6).ToString();
                    pekerjaan = Data.GetValue(7).ToString();
                    biaya = int.Parse(Data.GetValue(8).ToString());
                    lama = int.Parse(Data.GetValue(9).ToString());
                    spk spk = new spk(noSpk, status, namaPembeli, alamatPembeli, namaUser, alamatUser, telepon, pekerjaan, biaya, lama);
                    listSpk.Add(spk);
                }
                MSC.Dispose();
                Data.Dispose();
                return "sukses";
            }
            catch (Exception E)
            {
                return E.Message;
            }
        }

        public string Update(string noSp, string text)
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "UPDATE spk SET komentar = '"+ text + "'" +" WHERE spk.noSPK = '"+noSp+"'";
            MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
            try
            {
                MySqlDataReader Data = MSC.ExecuteReader();
                while (Data.Read() == true)
                {
                    MSC.ExecuteNonQuery();
                }
                return "sukses";
            }
            catch (Exception E)
            {
                return E.Message;
            }
        }
    }
}
