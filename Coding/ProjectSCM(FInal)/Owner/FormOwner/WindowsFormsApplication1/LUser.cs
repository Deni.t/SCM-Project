﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace WindowsFormsApplication1
{
    public class LUser
    {
         private List<User> listPegawai;
        private string kodeTerakhir;

        #region Properties
        public List<User> ListPegawai
        {
            get { return listPegawai; }
            
        }
        public int jumlahList
        {
            get { return listPegawai.Count; }
          
        }
        public string KodeTerakhir
        {
            get { return kodeTerakhir; }
          
        }
        #endregion

        #region Constructor
        public LUser()
        {
            listPegawai = new List<User>();
            kodeTerakhir = "1";
        }
        #endregion

        #region Method
        public string beriHakAkses(string username, string namaServer)
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "GRANT ALL PRIVILEGES ON *.* TO '" + username + "'@'" + namaServer + "' WITH GRANT OPTION";
            MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
            try
            {
                MSC.ExecuteNonQuery();
                return "sukses";
            }
            catch (Exception E)
            {
                return E.Message;
            }
        }
        public string BacaSemuaData()
        {
            Koneksi k = new Koneksi();
            k.Connect();
            string sql = "SELECT P.idUser, P.Nama, P.Alamat, P.Telepon, P.Username, P.Password,J.kodejabatan, J.NamaJabatan"+
                " FROM userti P INNER JOIN jabatan J ON P.kodeJabatan = J.kodeJabatan"; //mengisikan perintah sql yg akan dijalankan
            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB); //membuat mysqlcommandnya
            try
            {
                MySqlDataReader data = c.ExecuteReader(); //gunakan MySqlReader dan ExecuteReader untuk menjalankan perintah SELECT

                while (data.Read() == true) //Selama data reader masih bisa membaca (selama masih ada data)
                {
                    string IdUser = data.GetValue(0).ToString();
                    string NamaUser = data.GetValue(1).ToString();
                    string AlamatUser = data.GetValue(2).ToString();
                    string TeleponUser = data.GetValue(3).ToString();
                    string UsernameUser = data.GetValue(4).ToString();
                    string PasswordUser = data.GetValue(5).ToString();
                    string kodeJabatan = data.GetValue(6).ToString();
                    string namaJabatan = data.GetValue(7).ToString();
                    Jabatan J = new Jabatan(kodeJabatan,namaJabatan);
                    User U = new User(IdUser,NamaUser,AlamatUser,TeleponUser,UsernameUser,PasswordUser,J);
                    listPegawai.Add(U);
                }
                c.Dispose(); //hapus MySqlCommand setelah selesai
                data.Dispose(); //hapus data reader setelah selesai

                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        public string CariData(string kriteria, string nilaiKriteria)
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "SELECT Distinct P.idUser, P.Nama, P.Alamat, P.Telepon, P.Username, P.Password,J.kodejabatan, J.NamaJabatan" +
                " FROM userti P INNER JOIN jabatan J ON P.kodeJabatan = J.kodeJabatan WHERE " + kriteria + " LIKE '%" + nilaiKriteria + "%'";
            MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
            try
            {
                MySqlDataReader data = MSC.ExecuteReader(); //gunakan MySqlReader dan ExecuteReader untuk menjalankan perintah SELECT

                while (data.Read() == true) //Selama data reader masih bisa membaca (selama masih ada data)
                {

                    string IdUser = data.GetValue(0).ToString();
                    string NamaUser = data.GetValue(1).ToString();
                    string AlamatUser = data.GetValue(2).ToString();
                    string TeleponUser = data.GetValue(3).ToString();
                    string UsernameUser = data.GetValue(4).ToString();
                    string PasswordUser = data.GetValue(5).ToString();
                    string kodeJabatan = data.GetValue(6).ToString();
                    string namaJabatan = data.GetValue(7).ToString();
                    Jabatan J = new Jabatan(kodeJabatan, namaJabatan);
                    User U = new User(IdUser, NamaUser, AlamatUser, TeleponUser, UsernameUser, PasswordUser, J);
                    listPegawai.Add(U);
                }
                MSC.Dispose(); //hapus MySqlCommand setelah selesai
                data.Dispose(); //hapus data reader setelah selesai

                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        public string buatUserBaru(string username,string password, string namaServer)
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "CREATE USER '" +username + "'@'" + namaServer + "' IDENTIFIED BY '" + password + "'";
            MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
            try
            {
                MSC.ExecuteNonQuery();
                return "sukses";
            }
            catch (Exception E)
            {
                return E.Message;
            }
        }
        public string GenerateKode()
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "SELECT KodePegawai FROM pegawai ORDER BY KodePegawai DESC LIMIT 1";
            MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
            try
            {
                MySqlDataReader Data = MSC.ExecuteReader();
                if (Data.Read() == true)
                {
                    int kdTerbaru = int.Parse(Data.GetValue(0).ToString()) + 1;
                    kodeTerakhir = kdTerbaru.ToString();
                }
                Data.Dispose();
                MSC.Dispose();
                return "sukses";
            }
            catch (Exception E)
            {
                return E.Message;
            }
        }
        //}
        //public string TambahData(Pegawai P)
        //{
        //    Koneksi k = new Koneksi();
        //    k.Connect();
        //    string sql = "INSERT INTO pegawai(KodePegawai, Nama, TglLahir, Alamat, Gaji, Username, Password, IdJabatan) VALUES (" + P.KodePegawai + ",'" + P.NamaPegawai + "','" + P.TanggalLahir.ToString("yyyy-MM-dd") + "','" + P.Alamat + "'," + P.Gaji + ",'" + P.Username + "','" + P.Password + "','" + P.JabatanPegawai.KodeJabatan + "')";
        //    MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB);

        //    try
        //    {
        //        c.ExecuteNonQuery();
        //        string HasilUserBaru = buatUserBaru(P,"localhost");
        //        if(HasilUserBaru == "sukses")
        //        {
        //            string hasilberiHakUser = beriHakAkses(P, "localhost");
        //            if(hasilberiHakUser == "sukses")
        //            {
        //                return "sukses";
        //            }
        //            else
        //            {
        //                return "Gagal Memberikan hak akses pada user. Pesan Kesalahan = " + hasilberiHakUser;
        //            }
        //        }
        //        else
        //        {
        //            return "Gagal Membuat User Baru. Pesan Kesalahan = " + HasilUserBaru;

        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return e.Message;
        //    }
        ////}
        //public string UbahData(Pegawai P)
        //{
        //    Koneksi K = new Koneksi();
        //    K.Connect();

        //    string sql = "UPDATE pegawai SET Nama = '" + P.NamaPegawai +
        //        "', TglLahir = '" + P.TanggalLahir.ToString("yyyy-MM-dd") + "', Alamat = '" + P.Alamat + 
        //        "', Gaji = '" + P.Gaji + "', Username = '" + P.Username + 
        //        "', Password = '" + P.Password + "', IdJabatan = '" + P.JabatanPegawai.KodeJabatan+
        //        "' WHERE KodePegawai = '" + P.KodePegawai+"'";


        //    MySqlCommand c = new MySqlCommand(sql, K.KoneksiDB);

        //    try
        //    {
        //        c.ExecuteNonQuery();
                
        //        return "sukses";
        //    }
        //    catch (Exception e)
        //    {
        //        return e.Message;
        //    }
        //}
        //public string HapusData(Pegawai P)
        //{
        //    Koneksi K = new Koneksi();
        //    K.Connect();
        //    string sql = "DELETE FROM PEGAWAI WHERE KodePegawai = '" + P.KodePegawai+"'";
        //    MySqlCommand c = new MySqlCommand(sql, K.KoneksiDB);

        //    try
        //    {
        //        c.ExecuteNonQuery();
        //        return "sukses";
        //    }
        //    catch (Exception e)
        //    {
        //        return e.Message;
        //    }
        //}
        
        //}
        //public string buatUserBaru(Pegawai P, string namaServer)
        //{
        //    Koneksi K = new Koneksi();
        //    K.Connect();
        //    string sql = "CREATE USER '" + P.Username + "'@'" + namaServer + "' IDENTIFIED BY '" + P.Password + "'";
        //    MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
        //    try
        //    {
        //        MSC.ExecuteNonQuery();
        //        return "sukses";
        //    }
        //    catch(Exception E )
        //    {
        //        return E.Message;
        //    }
        //}
        //public string beriHakAkses(Pegawai P, string namaServer)
        //{
        //    Koneksi K = new Koneksi();
        //    K.Connect();
        //    string sql = "GRANT ALL PRIVILEGES ON *.* TO '" + P.Username + "'@'" + namaServer + "' WITH GRANT OPTION";
        //    MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
        //    try
        //    {
        //        MSC.ExecuteNonQuery();
        //        return "sukses";
        //    }
        //    catch (Exception E)
        //    {
        //        return E.Message;
        //    }
        //}
        //public string UbahPasswordUser(Pegawai P, string namaServer,string passwordbaru)
        //{
        //    Koneksi K = new Koneksi();
        //    K.Connect();
        //    string sql = "SET PASSWORD FOR '" + P.Username + "'@'" + namaServer + "' = PASSWORD" +   "('" +passwordbaru + "')";
        //    MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
        //    try
        //    {
        //        MSC.ExecuteNonQuery();
        //        return "sukses";
        //    }
        //    catch (Exception E)
        //    {
        //        return E.Message;
        //    }
        //}

       
        #endregion
    }
}
