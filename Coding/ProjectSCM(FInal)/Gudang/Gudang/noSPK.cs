﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gudang
{
    public class noSPK
    {
        private string id;
        private DateTime tgl;


        public DateTime Tgl
        {
            get { return tgl; }
            set { tgl = value; }
        }

        public string Id
        {
            get { return id; }
            set { id = value; }
        }
        public noSPK()
        {
            id = "";
            tgl = new DateTime();
        }
        public noSPK(string _id, DateTime _tgl)
        {
            id = _id;
            Tgl = _tgl;
        }

    }
}
