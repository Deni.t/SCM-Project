﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Globalization;

namespace Gudang
{
    public class DaftarKonfirm
    {
        private List<konfrim> listKonfirm;

        public List<konfrim> ListKonfirm
        {
            get { return listKonfirm; }
        }
        public int JumlahList
        {
            get { return listKonfirm.Count; }
        }

        public DaftarKonfirm()
        {
            listKonfirm = new List<konfrim>();
        }


        public string BacaSemuaData()
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "select fpd.kodebahanbaku, bb.nama, s.namaSupplier, dbbs.hargaSatuan, fpd.jumlah, fp.tanggal, fpd.status, fpd.tanggalDatang from bahanbaku bb inner join detilbahanbakusupplier dbbs on bb.kodebahanbaku = dbbs.kodebahanbaku inner join supplier s on s.kodeSupplier = dbbs.kodeSupplier inner join formpembelian fp on fp.kodesupplier = s.kodesupplier inner join formpembeliandetil fpd on fpd.kodepembelian = fp.kodepembelian where fpd.kodebahanbaku = bb.kodebahanbaku";

            MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
            try
            {
                MySqlDataReader Data = MSC.ExecuteReader();
                while (Data.Read() == true)
                {
                    string kodeP = Data.GetValue(0).ToString();
                    string nama = Data.GetValue(1).ToString();
                    string supp = Data.GetValue(2).ToString();
                    int harga = int.Parse(Data.GetValue(3).ToString());
                    int jumlah = int.Parse(Data.GetValue(4).ToString());
                    DateTime tanggalB = DateTime.Parse(Data.GetValue(5).ToString());
                    int status = int.Parse(Data.GetValue(6).ToString());
                    DateTime tanggalD = DateTime.Parse(Data.GetValue(7).ToString());
                    konfrim kon = new konfrim(kodeP, nama, supp, harga, jumlah, tanggalB, status, tanggalD);
                    listKonfirm.Add(kon);
                }
                MSC.Dispose();
                Data.Dispose();
                return "sukses";
            }
            catch (Exception E)
            {
                return E.Message;
            }
        }

        public string Update(string kode, DateTime date)
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "UPDATE `formpembeliandetil` SET `status` = 1, tanggalDatang = '"+date.ToString("yyyy-MM-dd hh:mm:ss")+"' WHERE `formpembeliandetil`.`kodeBahanBaku` = '"+kode+"'";


            MySqlCommand c = new MySqlCommand(sql, K.KoneksiDB);

            try
            {
                c.ExecuteNonQuery();

                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string CariData(string kode)
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "select fpd.kodebahanbaku, bb.nama, s.namaSupplier, dbbs.hargaSatuan, fpd.jumlah, fp.tanggal, fpd.status, fpd.tanggalDatang from bahanbaku bb inner join detilbahanbakusupplier dbbs on bb.kodebahanbaku = dbbs.kodebahanbaku inner join supplier s on s.kodeSupplier = dbbs.kodeSupplier inner join formpembelian fp on fp.kodesupplier = s.kodesupplier inner join formpembeliandetil fpd on fpd.kodepembelian = fp.kodepembelian where fpd.kodebahanbaku = '"+ kode +"' and fpd.kodebahanbaku = bb.kodebahanbaku";

            MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
            try
            {
                MySqlDataReader Data = MSC.ExecuteReader();
                while (Data.Read() == true)
                {
                    string kodeBB = Data.GetValue(0).ToString();
                    string nama = Data.GetValue(1).ToString();
                    string supp = Data.GetValue(2).ToString();
                    int harga = int.Parse(Data.GetValue(3).ToString());
                    int jumlah = int.Parse(Data.GetValue(4).ToString());
                    DateTime tanggalB = DateTime.Parse(Data.GetValue(5).ToString());
                    int status = int.Parse(Data.GetValue(6).ToString());
                    DateTime tanggalD = DateTime.Parse(Data.GetValue(7).ToString());

                    konfrim kon = new konfrim(kodeBB, nama, supp, harga, jumlah, tanggalB, status, tanggalD);
                    listKonfirm.Add(kon);
                }
                MSC.Dispose();
                Data.Dispose();
                return "sukses";
            }
            catch (Exception E)
            {
                return E.Message;
            }
        }
    }
}
