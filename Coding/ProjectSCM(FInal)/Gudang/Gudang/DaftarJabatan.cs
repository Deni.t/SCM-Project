﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Gudang
{
    public class DaftarJabatan
    {
        private List<Jabatan> listJabatan;
        private string kodeTerakhir;

        #region Properties
        public List<Jabatan> ListJabatan
        {
            get { return listJabatan; }
        }

        public int JumlahJabatan
        {
            get { return listJabatan.Count; }
        }

        public string KodeTerakhir
        {
            get { return kodeTerakhir; }
        }
        #endregion
        #region Constructor
        public DaftarJabatan()
        {
            listJabatan = new List<Jabatan>();
            kodeTerakhir = "J1";
        }
        #endregion
        #region Method
        public string BacaSemuaData()
        {
            Koneksi k = new Koneksi();
            k.Connect();
            string sql = "SELECT * FROM jabatan"; //mengisikan perintah sql yg akan dijalankan
            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB); //membuat mysqlcommandnya
            try
            {
                MySqlDataReader data = c.ExecuteReader(); //gunakan MySqlReader dan ExecuteReader untuk menjalankan perintah SELECT

                while (data.Read() == true) //Selama data reader masih bisa membaca (selama masih ada data)
                {
                    string kode = data.GetValue(0).ToString(); //Mendapatkan kode kategori dari hasil data reader
                    string nama = data.GetValue(1).ToString(); // mendapatkan nama kategori dari hasil data reader

                    Jabatan kt = new Jabatan(kode, nama); //create objek bertipe kategori
                    listJabatan.Add(kt); //simpan ke list
                }
                c.Dispose(); //hapus MySqlCommand setelah selesai
                data.Dispose(); //hapus data reader setelah selesai

                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string CariData(string kriteria, string nilaiKriteria)
        {
            Koneksi k = new Koneksi();
            k.Connect();
            string sql = "SELECT * FROM jabatan WHERE " + kriteria + " LIKE'%" + nilaiKriteria + "%'"; //mengisikan perintah sql yg akan dijalankan
            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB); //membuat mysqlcommandnya
            try
            {
                MySqlDataReader data = c.ExecuteReader(); //gunakan MySqlReader dan ExecuteReader untuk menjalankan perintah SELECT

                while (data.Read() == true) //Selama data reader masih bisa membaca (selama masih ada data)
                {
                    string kode = data.GetValue(0).ToString(); //Mendapatkan kode kategori dari hasil data reader
                    string nama = data.GetValue(1).ToString(); // mendapatkan nama kategori dari hasil data reader

                    Jabatan kt = new Jabatan(kode, nama); //create objek bertipe kategori
                    listJabatan.Add(kt); //simpan ke list
                }
                c.Dispose(); //hapus MySqlCommand setelah selesai
                data.Dispose(); //hapus data reader setelah selesai

                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string GenerateKode()
        {
            Koneksi k = new Koneksi();
            k.Connect();
            string sql = "SELECT SUBSTRING(Idjabatan,2,1) FROM jabatan ORDER BY IdJabatan DESC LIMIT 1";
            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB); //membuat mysqlcommandnya
            try
            {
                MySqlDataReader data = c.ExecuteReader(); //gunakan MySqlReader dan ExecuteReader untuk menjalankan perintah SELECT

                if (data.Read() == true)//jika data reader masih bisa membaca(masih ada data)
                {
                    int kdTerbaru = int.Parse(data.GetValue(0).ToString()) + 1; //mendapatkan kode kategori dari hasil data reader
                    kodeTerakhir = kdTerbaru.ToString();


                    kodeTerakhir = "j" + kodeTerakhir;

                }
                c.Dispose(); //hapus MySqlCommand setelah selesai
                data.Dispose(); //hapus data reader setelah selesai

                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string TambahData(Jabatan kat)
        {
            Koneksi k = new Koneksi();
            k.Connect();

            string sql = "INSERT INTO jabatan(Idjabatan, Nama) VALUES ('" + kat.KodeJabatan + "','" + kat.NamaJabatan + "')";

            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB);

            try
            {
                c.ExecuteNonQuery();
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string UbahData(Jabatan kat)
        {
            Koneksi k = new Koneksi();
            k.Connect();

            string sql = "UPDATE jabatan SET nama = '" + kat.NamaJabatan + "' WHERE idjabatan = '" + kat.KodeJabatan + "'";

            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB);

            try
            {
                c.ExecuteNonQuery();
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string HapusData(Jabatan kat)
        {
            Koneksi k = new Koneksi();
            k.Connect();

            string sql = "DELETE FROM jabatan WHERE Idjabatan = '" + kat.KodeJabatan + "'";

            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB);

            try
            {
                c.ExecuteNonQuery();
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        #endregion
    }
}
