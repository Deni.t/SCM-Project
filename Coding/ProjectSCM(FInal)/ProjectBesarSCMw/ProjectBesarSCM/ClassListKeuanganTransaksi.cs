﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.IO;
using System.Drawing;
using System.Drawing.Printing;

namespace ProjectBesarSCM
{
    class ClassListKeuanganTransaksi
    {
        ClassKeuanganTransaksi CKT = new ClassKeuanganTransaksi();

        private List<ClassKeuanganTransaksi> listKT;

        public List<ClassKeuanganTransaksi> ListKT
        {
            get { return listKT; }
            set { listKT = value; }
        }
        public int jmlh
        {
            get { return listKT.Count; }
        }

        public ClassListKeuanganTransaksi()
        {
            listKT = new List<ClassKeuanganTransaksi>();
        }

        #region method

        public string InsertTransaksiInput(string kode, int pemb1, int pemb2, string val1, string val2, string tgl1, string tgl2, int kurang, string nospka)
        {
            Koneksi k = new Koneksi();
            k.Connect();
            string sql = "INSERT INTO formpembayaran (kodePembayaran, pembayaranSatu, tanggalPembayaranSatu, caraPembayaranSatu, pembayaranDua, tanggalPembayaranDua, CaraPembayaranDua, kekurangan, noSPK) VALUES ('" + kode + "','" + pemb1 + "','" + tgl1 + "','" + val1 + "','" + pemb2 + "','" + tgl2 + "','" + val2 + "','" + kurang + "','" + nospka + "')";
            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB);
            try
            {
                c.ExecuteNonQuery();
                return "sukses";
            }
            catch(Exception e)
            {
                return e.Message;
            }
        }

        public int CariNomor()
        {
            Koneksi k = new Koneksi();
            k.Connect();

            string sql = "SELECT SUBSTRING(kodePembayaran,4) from formpembayaran order by kodePembayaran DESC limit 1";
            MySqlCommand msc = new MySqlCommand(sql, k.KoneksiDB);

                int kode = 0;
                MySqlDataReader data = msc.ExecuteReader();
                while (data.Read() == true)
                {
                     kode = int.Parse(data.GetValue(0).ToString())+1;

                }
                msc.Dispose();
                data.Dispose();
                return kode;
        }

        public string CariSpk(string id)
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "SELECT s.tanggal, c.nama, c.alamat, s.pekerjaan, s.biaya, s.noSPK FROM spk s INNER JOIN customer c ON s.kodeCustomer=c.kodeCustomer where s.noSPK='"+id+"'";
            MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
            try
            {
                MySqlDataReader data = MSC.ExecuteReader();
                while (data.Read() == true)
                {
                    DateTime tanggal = DateTime.Parse(data.GetValue(0).ToString());
                    string nama = data.GetValue(1).ToString();
                    string alamat = data.GetValue(2).ToString();
                    string pekerjaan = data.GetValue(3).ToString();
                    int juml = int.Parse(data.GetValue(4).ToString());
                    string no = data.GetValue(5).ToString();

                    ClassKeuanganTransaksi trans = new ClassKeuanganTransaksi(tanggal, nama, alamat, pekerjaan, juml, no);
                    listKT.Add(trans);

                }
                MSC.Dispose();
                data.Dispose();
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        

        #endregion
    }
}
