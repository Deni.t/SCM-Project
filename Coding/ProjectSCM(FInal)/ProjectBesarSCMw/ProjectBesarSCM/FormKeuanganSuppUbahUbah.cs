﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectBesarSCM
{
    public partial class FormKeuanganSuppUbahUbah : Form
    {
        public FormKeuanganSuppUbahUbah()
        {
            InitializeComponent();
        }
        public List<string> Nama = new List<string>();
        string kodeSupplier;
        string kodeBahanBaku;
        int stok;
        string satuan;
        private void FormKeuanganSuppUbahUbah_Load(object sender, EventArgs e)
        {
            ListSupplier ls = new ListSupplier();
            string hasilSupplier = ls.BacaSemuaData();
            if(hasilSupplier == "sukses")
            {
                //comboBox1.Items.Clear();
                for(int i=0; i< ls.JumlahList;i++)
                {
                    string kode = ls.LSupplier[i].KodeSup;
                    string nama = ls.LSupplier[i].NamaSup;
                    comboBox1.Items.Add(kode + "-" + nama);
                }
            }
            else
            {
                MessageBox.Show(hasilSupplier);
            }

            ListBahanBaku lb = new ListBahanBaku();
            string hasilBahan = lb.BacaSemuaData();
            if (hasilBahan == "sukses")
            {
                //comboBox1.Items.Clear();
                for (int i = 0; i < lb.JumlahBarang; i++)
                {
                    string kode = lb.LBahanBaku[i].KodeBarang;
                    string nama = lb.LBahanBaku[i].NamaBarang;
                    comboBox2.Items.Add(kode + "-" + nama);
                }
            }
            else
            {
                MessageBox.Show(hasilBahan);
            }
            //FormKeuanganHome F1 = (FormKeuanganHome)this.Owner;
            //labelHello.Text = F1.labelHello.Text;
            //Nama.Add(F1.nama);
            //labelHello.Text = "Hello, " + F1.nama + "-" + F1.nama;

        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {

        }

        private void buttonSimpan_Click(object sender, EventArgs e)
        {
            ListSupplier DS = new ListSupplier();
            int panjangcombo = comboBox1.Text.Length;
            string namaSupplier = comboBox1.Text.Substring(3,panjangcombo-3);
            MessageBox.Show(kodeSupplier + "" + namaSupplier);
            Supplier Sup = new Supplier(kodeSupplier, namaSupplier, textBox1.Text);
            string hasil = DS.UbahDataSupplier(Sup);
            if (hasil == "sukses")
            {
                MessageBox.Show("Data Telah Diubah");
                ListBahanBaku lb = new ListBahanBaku();
                
                string namaBahan = comboBox2.Text.Substring(5, 2);
                BahanBaku bb = new BahanBaku(kodeBahanBaku, namaBahan, satuan, stok);
                DetilBahanBaku dbb = new DetilBahanBaku(bb, Sup, int.Parse(textBoxHargaSatuan.Text));
                string res = lb.UbahHargaBahan(dbb);
                if (res == "sukses")
                {
                    MessageBox.Show("Data Telah Diubah");
                }
                else
                {
                    MessageBox.Show("Data Gagal Diubah, Pesan : " + res, "Info");
                }
            }
            else
            {
                MessageBox.Show("Data Gagal Diubah, Pesan : " + hasil, "Info");
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            kodeSupplier = comboBox1.Text.Substring(0, 2);
            ListSupplier daftarSup = new ListSupplier();
            daftarSup.CariData("kodeSupplier", kodeSupplier);
            for (int i = 0; i < daftarSup.JumlahList; i++ )
            {
                textBox1.Text = daftarSup.LSupplier[i].Alamat;
            }
                
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            kodeBahanBaku = comboBox2.Text.Substring(0, 5);
            ListDetilBahanBaku daftarSup = new ListDetilBahanBaku();
            daftarSup.cariData("kodeSupplier", kodeSupplier, "kodeBahanBaku", kodeBahanBaku);
            for (int i = 0; i < daftarSup.JumlahList; i++)
            {
                textBoxHargaSatuan.Text = daftarSup.LDetilBahanBaku[i].HargaSatuan.ToString();
            }
        }
    }
}
