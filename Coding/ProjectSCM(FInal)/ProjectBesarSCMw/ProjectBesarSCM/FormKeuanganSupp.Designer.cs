﻿namespace ProjectBesarSCM
{
    partial class FormKeuanganSupp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelHello = new System.Windows.Forms.Label();
            this.buttonTambahSupplier = new System.Windows.Forms.Button();
            this.buttonUpdateSupplier = new System.Windows.Forms.Button();
            this.buttonHapusSupplier = new System.Windows.Forms.Button();
            this.buttonUbahSupplier = new System.Windows.Forms.Button();
            this.buttonHapusBahan = new System.Windows.Forms.Button();
            this.buttonTambahBahan = new System.Windows.Forms.Button();
            this.buttonSimpan = new System.Windows.Forms.Button();
            this.comboBoxId = new System.Windows.Forms.ComboBox();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.textBoxAlamat = new System.Windows.Forms.TextBox();
            this.textBoxSupplier = new System.Windows.Forms.TextBox();
            this.labelID = new System.Windows.Forms.Label();
            this.labelAlamat = new System.Windows.Forms.Label();
            this.labelSupplier = new System.Windows.Forms.Label();
            this.buttonHapus = new System.Windows.Forms.Button();
            this.labelId2 = new System.Windows.Forms.Label();
            this.buttonLogin = new System.Windows.Forms.Button();
            this.buttonBatal = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelHello
            // 
            this.labelHello.AutoSize = true;
            this.labelHello.Location = new System.Drawing.Point(504, 14);
            this.labelHello.Name = "labelHello";
            this.labelHello.Size = new System.Drawing.Size(43, 13);
            this.labelHello.TabIndex = 0;
            this.labelHello.Text = "Hello  , ";
            // 
            // buttonTambahSupplier
            // 
            this.buttonTambahSupplier.Location = new System.Drawing.Point(117, 87);
            this.buttonTambahSupplier.Name = "buttonTambahSupplier";
            this.buttonTambahSupplier.Size = new System.Drawing.Size(126, 102);
            this.buttonTambahSupplier.TabIndex = 6;
            this.buttonTambahSupplier.Text = "TAMBAH SUPPLIER";
            this.buttonTambahSupplier.UseVisualStyleBackColor = true;
            this.buttonTambahSupplier.Click += new System.EventHandler(this.buttonTambahSupplier_Click);
            // 
            // buttonUpdateSupplier
            // 
            this.buttonUpdateSupplier.Location = new System.Drawing.Point(288, 87);
            this.buttonUpdateSupplier.Name = "buttonUpdateSupplier";
            this.buttonUpdateSupplier.Size = new System.Drawing.Size(126, 102);
            this.buttonUpdateSupplier.TabIndex = 7;
            this.buttonUpdateSupplier.Text = "UBAH DETIL SUPPLIER";
            this.buttonUpdateSupplier.UseVisualStyleBackColor = true;
            this.buttonUpdateSupplier.Click += new System.EventHandler(this.buttonUpdateSupplier_Click);
            // 
            // buttonHapusSupplier
            // 
            this.buttonHapusSupplier.Location = new System.Drawing.Point(460, 87);
            this.buttonHapusSupplier.Name = "buttonHapusSupplier";
            this.buttonHapusSupplier.Size = new System.Drawing.Size(126, 102);
            this.buttonHapusSupplier.TabIndex = 8;
            this.buttonHapusSupplier.Text = "HAPUS SUPPLIER";
            this.buttonHapusSupplier.UseVisualStyleBackColor = true;
            this.buttonHapusSupplier.Click += new System.EventHandler(this.buttonHapusSupplier_Click);
            // 
            // buttonUbahSupplier
            // 
            this.buttonUbahSupplier.Enabled = false;
            this.buttonUbahSupplier.Location = new System.Drawing.Point(270, 330);
            this.buttonUbahSupplier.Name = "buttonUbahSupplier";
            this.buttonUbahSupplier.Size = new System.Drawing.Size(135, 39);
            this.buttonUbahSupplier.TabIndex = 2;
            this.buttonUbahSupplier.Text = "UBAH SUPPLIER";
            this.buttonUbahSupplier.UseVisualStyleBackColor = true;
            this.buttonUbahSupplier.Click += new System.EventHandler(this.buttonUbahSupplier_Click);
            // 
            // buttonHapusBahan
            // 
            this.buttonHapusBahan.Enabled = false;
            this.buttonHapusBahan.Location = new System.Drawing.Point(416, 330);
            this.buttonHapusBahan.Name = "buttonHapusBahan";
            this.buttonHapusBahan.Size = new System.Drawing.Size(154, 39);
            this.buttonHapusBahan.TabIndex = 1;
            this.buttonHapusBahan.Text = "HAPUS BAHAN";
            this.buttonHapusBahan.UseVisualStyleBackColor = true;
            this.buttonHapusBahan.Click += new System.EventHandler(this.buttonHapusBahan_Click);
            // 
            // buttonTambahBahan
            // 
            this.buttonTambahBahan.Enabled = false;
            this.buttonTambahBahan.Location = new System.Drawing.Point(125, 330);
            this.buttonTambahBahan.Name = "buttonTambahBahan";
            this.buttonTambahBahan.Size = new System.Drawing.Size(139, 39);
            this.buttonTambahBahan.TabIndex = 0;
            this.buttonTambahBahan.Text = "TAMBAH BAHAN";
            this.buttonTambahBahan.UseVisualStyleBackColor = true;
            this.buttonTambahBahan.Click += new System.EventHandler(this.buttonTambahBahan_Click);
            // 
            // buttonSimpan
            // 
            this.buttonSimpan.Enabled = false;
            this.buttonSimpan.Location = new System.Drawing.Point(441, 407);
            this.buttonSimpan.Name = "buttonSimpan";
            this.buttonSimpan.Size = new System.Drawing.Size(85, 31);
            this.buttonSimpan.TabIndex = 19;
            this.buttonSimpan.Text = "SIMPAN";
            this.buttonSimpan.UseVisualStyleBackColor = true;
            this.buttonSimpan.Click += new System.EventHandler(this.buttonSimpan_Click_1);
            // 
            // comboBoxId
            // 
            this.comboBoxId.Enabled = false;
            this.comboBoxId.FormattingEnabled = true;
            this.comboBoxId.Location = new System.Drawing.Point(432, 226);
            this.comboBoxId.Name = "comboBoxId";
            this.comboBoxId.Size = new System.Drawing.Size(138, 21);
            this.comboBoxId.TabIndex = 22;
            this.comboBoxId.SelectedIndexChanged += new System.EventHandler(this.comboBoxSupplier_SelectedIndexChanged);
            // 
            // textBoxID
            // 
            this.textBoxID.Enabled = false;
            this.textBoxID.Location = new System.Drawing.Point(180, 279);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.Size = new System.Drawing.Size(100, 20);
            this.textBoxID.TabIndex = 17;
            // 
            // textBoxAlamat
            // 
            this.textBoxAlamat.Enabled = false;
            this.textBoxAlamat.Location = new System.Drawing.Point(180, 253);
            this.textBoxAlamat.Name = "textBoxAlamat";
            this.textBoxAlamat.Size = new System.Drawing.Size(234, 20);
            this.textBoxAlamat.TabIndex = 16;
            // 
            // textBoxSupplier
            // 
            this.textBoxSupplier.Enabled = false;
            this.textBoxSupplier.Location = new System.Drawing.Point(180, 227);
            this.textBoxSupplier.Name = "textBoxSupplier";
            this.textBoxSupplier.Size = new System.Drawing.Size(161, 20);
            this.textBoxSupplier.TabIndex = 15;
            // 
            // labelID
            // 
            this.labelID.AutoSize = true;
            this.labelID.Location = new System.Drawing.Point(152, 282);
            this.labelID.Name = "labelID";
            this.labelID.Size = new System.Drawing.Size(27, 13);
            this.labelID.TabIndex = 14;
            this.labelID.Text = "ID : ";
            // 
            // labelAlamat
            // 
            this.labelAlamat.AutoSize = true;
            this.labelAlamat.Location = new System.Drawing.Point(132, 256);
            this.labelAlamat.Name = "labelAlamat";
            this.labelAlamat.Size = new System.Drawing.Size(48, 13);
            this.labelAlamat.TabIndex = 13;
            this.labelAlamat.Text = "Alamat : ";
            // 
            // labelSupplier
            // 
            this.labelSupplier.AutoSize = true;
            this.labelSupplier.Location = new System.Drawing.Point(126, 230);
            this.labelSupplier.Name = "labelSupplier";
            this.labelSupplier.Size = new System.Drawing.Size(54, 13);
            this.labelSupplier.TabIndex = 12;
            this.labelSupplier.Text = "Supplier : ";
            // 
            // buttonHapus
            // 
            this.buttonHapus.Enabled = false;
            this.buttonHapus.Location = new System.Drawing.Point(350, 407);
            this.buttonHapus.Name = "buttonHapus";
            this.buttonHapus.Size = new System.Drawing.Size(85, 31);
            this.buttonHapus.TabIndex = 24;
            this.buttonHapus.Text = "HAPUS";
            this.buttonHapus.UseVisualStyleBackColor = true;
            this.buttonHapus.Click += new System.EventHandler(this.buttonHapus_Click);
            // 
            // labelId2
            // 
            this.labelId2.AutoSize = true;
            this.labelId2.Location = new System.Drawing.Point(399, 229);
            this.labelId2.Name = "labelId2";
            this.labelId2.Size = new System.Drawing.Size(27, 13);
            this.labelId2.TabIndex = 25;
            this.labelId2.Text = "ID : ";
            // 
            // buttonLogin
            // 
            this.buttonLogin.BackColor = System.Drawing.Color.Transparent;
            this.buttonLogin.FlatAppearance.BorderSize = 0;
            this.buttonLogin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonLogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLogin.Location = new System.Drawing.Point(603, 452);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(93, 38);
            this.buttonLogin.TabIndex = 41;
            this.buttonLogin.UseVisualStyleBackColor = false;
            this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click);
            // 
            // buttonBatal
            // 
            this.buttonBatal.Enabled = false;
            this.buttonBatal.Location = new System.Drawing.Point(135, 407);
            this.buttonBatal.Name = "buttonBatal";
            this.buttonBatal.Size = new System.Drawing.Size(82, 31);
            this.buttonBatal.TabIndex = 42;
            this.buttonBatal.Text = "BATAL";
            this.buttonBatal.UseVisualStyleBackColor = true;
            this.buttonBatal.Click += new System.EventHandler(this.buttonBatal_Click);
            // 
            // FormKeuanganSupp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ProjectBesarSCM.Properties.Resources.Keuangan_Supp;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(714, 502);
            this.Controls.Add(this.buttonBatal);
            this.Controls.Add(this.buttonLogin);
            this.Controls.Add(this.labelId2);
            this.Controls.Add(this.buttonHapus);
            this.Controls.Add(this.buttonUbahSupplier);
            this.Controls.Add(this.buttonSimpan);
            this.Controls.Add(this.buttonHapusBahan);
            this.Controls.Add(this.buttonTambahBahan);
            this.Controls.Add(this.comboBoxId);
            this.Controls.Add(this.textBoxID);
            this.Controls.Add(this.textBoxAlamat);
            this.Controls.Add(this.textBoxSupplier);
            this.Controls.Add(this.labelID);
            this.Controls.Add(this.labelAlamat);
            this.Controls.Add(this.labelSupplier);
            this.Controls.Add(this.buttonHapusSupplier);
            this.Controls.Add(this.buttonUpdateSupplier);
            this.Controls.Add(this.buttonTambahSupplier);
            this.Controls.Add(this.labelHello);
            this.Name = "FormKeuanganSupp";
            this.Text = "FormKeuanganSupp";
            this.Load += new System.EventHandler(this.FormKeuanganSupp_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelHello;
        private System.Windows.Forms.Button buttonTambahSupplier;
        private System.Windows.Forms.Button buttonUpdateSupplier;
        private System.Windows.Forms.Button buttonHapusSupplier;
        private System.Windows.Forms.Button buttonUbahSupplier;
        private System.Windows.Forms.Button buttonHapusBahan;
        private System.Windows.Forms.Button buttonTambahBahan;
        private System.Windows.Forms.Button buttonSimpan;
        private System.Windows.Forms.ComboBox comboBoxId;
        private System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.TextBox textBoxAlamat;
        private System.Windows.Forms.TextBox textBoxSupplier;
        private System.Windows.Forms.Label labelID;
        private System.Windows.Forms.Label labelAlamat;
        private System.Windows.Forms.Label labelSupplier;
        private System.Windows.Forms.Button buttonHapus;
        private System.Windows.Forms.Label labelId2;
        private System.Windows.Forms.Button buttonLogin;
        private System.Windows.Forms.Button buttonBatal;
    }
}