﻿namespace Admin
{
    partial class Admin_FormJadwalSubmit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblUser_A_FJS = new System.Windows.Forms.Label();
            this.lblDateTime_A_FJS = new System.Windows.Forms.Label();
            this.btnKembali_A_FJS = new System.Windows.Forms.Button();
            this.cboVerifikasi_A_FJS = new System.Windows.Forms.CheckBox();
            this.btnSubmit_A_FJS = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.timer1_A_FJS = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // lblUser_A_FJS
            // 
            this.lblUser_A_FJS.AutoSize = true;
            this.lblUser_A_FJS.Location = new System.Drawing.Point(618, 19);
            this.lblUser_A_FJS.Name = "lblUser_A_FJS";
            this.lblUser_A_FJS.Size = new System.Drawing.Size(29, 13);
            this.lblUser_A_FJS.TabIndex = 3;
            this.lblUser_A_FJS.Text = "User";
            // 
            // lblDateTime_A_FJS
            // 
            this.lblDateTime_A_FJS.AutoSize = true;
            this.lblDateTime_A_FJS.Location = new System.Drawing.Point(23, 19);
            this.lblDateTime_A_FJS.Name = "lblDateTime_A_FJS";
            this.lblDateTime_A_FJS.Size = new System.Drawing.Size(58, 13);
            this.lblDateTime_A_FJS.TabIndex = 2;
            this.lblDateTime_A_FJS.Text = "Date/Time";
            // 
            // btnKembali_A_FJS
            // 
            this.btnKembali_A_FJS.Location = new System.Drawing.Point(494, 326);
            this.btnKembali_A_FJS.Name = "btnKembali_A_FJS";
            this.btnKembali_A_FJS.Size = new System.Drawing.Size(153, 43);
            this.btnKembali_A_FJS.TabIndex = 4;
            this.btnKembali_A_FJS.Text = "KEMBALI";
            this.btnKembali_A_FJS.UseVisualStyleBackColor = true;
            this.btnKembali_A_FJS.Click += new System.EventHandler(this.btnKembali_A_FJS_Click);
            // 
            // cboVerifikasi_A_FJS
            // 
            this.cboVerifikasi_A_FJS.AutoSize = true;
            this.cboVerifikasi_A_FJS.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboVerifikasi_A_FJS.Location = new System.Drawing.Point(225, 341);
            this.cboVerifikasi_A_FJS.Name = "cboVerifikasi_A_FJS";
            this.cboVerifikasi_A_FJS.Size = new System.Drawing.Size(199, 28);
            this.cboVerifikasi_A_FJS.TabIndex = 5;
            this.cboVerifikasi_A_FJS.Text = "VERIFIKASI BENAR";
            this.cboVerifikasi_A_FJS.UseVisualStyleBackColor = true;
            this.cboVerifikasi_A_FJS.CheckedChanged += new System.EventHandler(this.cboVerifikasi_A_FJS_CheckedChanged);
            // 
            // btnSubmit_A_FJS
            // 
            this.btnSubmit_A_FJS.Enabled = false;
            this.btnSubmit_A_FJS.Location = new System.Drawing.Point(26, 326);
            this.btnSubmit_A_FJS.Name = "btnSubmit_A_FJS";
            this.btnSubmit_A_FJS.Size = new System.Drawing.Size(149, 43);
            this.btnSubmit_A_FJS.TabIndex = 6;
            this.btnSubmit_A_FJS.Text = "SUBMIT";
            this.btnSubmit_A_FJS.UseVisualStyleBackColor = true;
            this.btnSubmit_A_FJS.Click += new System.EventHandler(this.btnSubmit_A_FJS_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(26, 57);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(621, 251);
            this.listBox1.TabIndex = 7;
            // 
            // timer1_A_FJS
            // 
            this.timer1_A_FJS.Tick += new System.EventHandler(this.timer1_A_FJS_Tick);
            // 
            // Admin_FormJadwalSubmit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(671, 391);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.btnSubmit_A_FJS);
            this.Controls.Add(this.cboVerifikasi_A_FJS);
            this.Controls.Add(this.btnKembali_A_FJS);
            this.Controls.Add(this.lblUser_A_FJS);
            this.Controls.Add(this.lblDateTime_A_FJS);
            this.Name = "Admin_FormJadwalSubmit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormJadwalSubmit";
            this.Load += new System.EventHandler(this.FormJadwalSubmit_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUser_A_FJS;
        private System.Windows.Forms.Label lblDateTime_A_FJS;
        private System.Windows.Forms.Button btnKembali_A_FJS;
        private System.Windows.Forms.CheckBox cboVerifikasi_A_FJS;
        private System.Windows.Forms.Button btnSubmit_A_FJS;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Timer timer1_A_FJS;
    }
}