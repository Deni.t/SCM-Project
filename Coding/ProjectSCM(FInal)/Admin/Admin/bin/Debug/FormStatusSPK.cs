﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Admin
{
    public partial class FormStatusSPK : Form
    {
        String[] tampung;
        public FormStatusSPK()
        {
            InitializeComponent();
        }

        private void FormStatusSPK_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            var loadstring = @"server=localhost;database=scm;userid=root;password=;";
            using (var connection = new MySqlConnection(loadstring))
            {
                connection.Open();

                var queryNoSPK = "SELECT noSPK from spk";
                using (var command1 = new MySqlCommand(queryNoSPK, connection))
                {
                    using (var reader1 = command1.ExecuteReader())
                    {
                        string a = "";
                        while (reader1.Read())
                        {
                            a = reader1.GetString("noSPK");
                            cboSPK.Items.Add(a);
                            cboSPK.SelectedIndex = -1;
                        }
                    }
                }
            }
            int status = 0;
            Admin_FormSPK fSPK = (Admin_FormSPK)this.Owner;
            Admin_FormHome FH = (Admin_FormHome)this.Owner;
            int stat = FH.status;
            if (stat == 1)
            {
                status = 1;
                label1.Text = fSPK.labelUser.Text;
            }

        }

        private void btnKembali_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cboSPK_SelectedIndexChanged(object sender, EventArgs e)
        {
            var loadstring = @"server=localhost;database=scm;userid=root;password=;";
            using (var connection = new MySqlConnection(loadstring))
            {
                connection.Open();
                string combo = cboSPK.Text;
                //var queryStatSPK = "Select * from spk s left join customer c on s.kodeCustomer= c.kodeCustomer left join userti u on s.idUser=u.idUser where s.noSPK = '" + combo + "'";
                var queryStatSPK = "Select s.*,c.nama as namaPem ,c.alamat as alamPem,u.nama,u.alamat,u.telepon from spk s left join customer c on s.kodeCustomer= c.kodeCustomer left join userti u on s.idUser=u.idUser where s.noSPK = '" + combo + "'";
                //MessageBox.Show("bisa");
                using (var command2 = new MySqlCommand(queryStatSPK, connection))
                {
                    using (var reader2 = command2.ExecuteReader())
                    {
                        lboStatusSPK.Items.Clear();
                        int status = 0;
                        while (reader2.Read())
                        {
                            tampung = new string[8];
                            status = reader2.GetInt32("status");
                            if (status == 0)
                            {
                                lboStatusSPK.Items.Add("Status : BELUM DIVERIFIKASI");
                            }
                            else
                            {
                                lboStatusSPK.Items.Add("Status : SUDAH DIVERIFIKASI");
                            }
                            tampung[0] = "Nama Pembeli : " + reader2.GetString("namaPem");
                            tampung[1] = "Alamat Pembeli : " + reader2.GetString("alamPem");
                            tampung[2] = "Nama Penanggung Jawab : " + reader2.GetString("nama");
                            tampung[3] = "Alamat Penanggung Jawab : " + reader2.GetString("alamat");
                            tampung[4] = "Telp : " + reader2.GetString("telepon");
                            tampung[5] = "Pekerjaan : " + reader2.GetString("pekerjaan");
                            tampung[6] = "Biaya : " + reader2.GetString("biaya");
                            tampung[7] = "Lama : " + reader2.GetString("lama") + "hari kerja - Dimulai tanggal " + reader2.GetMySqlDateTime("tanggalMulai");
                            lboStatusSPK.Items.Add("Nama Pembeli : " + reader2.GetString("namaPem"));
                            lboStatusSPK.Items.Add("Alamat Pembeli : " + reader2.GetString("alamPem"));
                            lboStatusSPK.Items.Add("Nama Penanggung Jawab : " + reader2.GetString("nama"));
                            lboStatusSPK.Items.Add("Alamat Penanggung Jawab : " + reader2.GetString("alamat"));
                            lboStatusSPK.Items.Add("Telp : " + reader2.GetString("telepon"));
                            lboStatusSPK.Items.Add("Pekerjaan : " + reader2.GetString("pekerjaan"));
                            lboStatusSPK.Items.Add("Biaya : " + reader2.GetString("biaya"));
                            lboStatusSPK.Items.Add("Lama : " + reader2.GetString("lama") + "hari kerja - Dimulai tanggal " + reader2.GetMySqlDateTime("tanggalMulai"));
                        }
                    }
                }
            }
        }

        private void btnKomentar_Click(object sender, EventArgs e)
        {
            var loadstring = @"server=localhost;database=scm;userid=root;password=;";
            using (var connection = new MySqlConnection(loadstring))
            {
                connection.Open();
                string combo = cboSPK.Text;
                var queryNoSPK = "SELECT komentar from spk where noSPK = '" + combo + "'";
                using (var command1 = new MySqlCommand(queryNoSPK, connection))
                {
                    using (var reader3 = command1.ExecuteReader())
                    {
                        //Iterate through the rows and add it to the combobox's items
                        string a = "";
                        while (reader3.Read())
                        {
                            a = reader3.GetString("komentar");
                            MessageBox.Show(a);
                        }
                    }
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            this.lblDateTime_A_FTM.Text = dt.ToString();
        }

        private void btnCetak_Click(object sender, EventArgs e)
        {
            Document doc = new Document(iTextSharp.text.PageSize.LETTER, 10, 10, 42, 35);
            PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream(cboSPK.Text + ".pdf", FileMode.Create));
            doc.Open();
            for (int i = 0; i < tampung.Length; i++)
            {
                Paragraph para = new Paragraph(tampung[i]);
                doc.Add(para);
            }
            MessageBox.Show("Lihat di Debug untuk lihat PDF");
            doc.Close();
        }
    }
}
