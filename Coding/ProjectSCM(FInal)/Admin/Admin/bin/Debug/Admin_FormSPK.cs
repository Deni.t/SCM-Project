﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1;
using ProjectBesarSCM;
namespace Admin
{
    public partial class Admin_FormSPK : Form
    {
        public int Status = 0;
        public Admin_FormSPK()
        {
            InitializeComponent();
            //timer1.Start();
        }
      
        private void btnStatusSPK_Click(object sender, EventArgs e)
        {
            FormStatusSPK formStatusSPK = new FormStatusSPK();
            formStatusSPK.ShowDialog();
            this.Hide();
        }

        private void btnHapusSPK_Click(object sender, EventArgs e)
        {
            FormHapusSPK formHapusSPK = new FormHapusSPK();
            formHapusSPK.ShowDialog();
            this.Hide();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            this.labelDate.Text = dt.ToString();
        }

        private void Admin_FormSPK_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            Admin_FormHome aFH = (Admin_FormHome)this.Owner;
            if(aFH.status == 1)
            {
                buttonBuatSPK.Enabled = false;
                btnHapusSPK.Enabled = false;
                labelUser.Text = aFH.labelUser.Text;
            }
           // FormKeuanganHome FH = (FormKeuanganHome)this.Owner;

            //int stat = FH.Status2;
            //if (stat == 1)
            //{
            //    buttonBuatSPK.Enabled = false;
            //    btnHapusSPK.Enabled = false;
            //}
        }

        private void buttonBuatSPK_Click(object sender, EventArgs e)
        {
            FormBuatSPK.FormSPK f1 = new FormBuatSPK.FormSPK();
            f1.Owner = this;
            f1.ShowDialog();
        }
    }
}
