﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1;
namespace Admin
{
    public partial class Admin_FormHome : Form
    {
        public int status = 0;
        public Admin_FormHome()
        {
            InitializeComponent();
        }

        private void FormAdmin_Home_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            if(status == 1)
            {
                buttonJadwalProduksi.Enabled = false;
                button1.Enabled = false;
                button2.Enabled = false;
            }

                
            
           // MessageBox.Show(status.ToString());
            
            
        }

        //private void buttonJadwalProduksi_Click(object sender, EventArgs e)
        //{
        //    FormJadwalProduksi formJadwal = new FormJadwalProduksi(admin_username, admin_kategori);
        //    formJadwal.ShowDialog();
        //    this.Hide();
        //}
        private void buttonJadwalProduksi_Click(object sender, EventArgs e)
        {
            this.Hide();
            Admin_FormJadwalProduksi formJadwal = new Admin_FormJadwalProduksi();
            formJadwal.Owner = this;
            formJadwal.ShowDialog(); this.Show();
            //this.Hide();
        }

        private void FormAdmin_Home_FormClosing(object sender, FormClosingEventArgs e)
        {
            //this.Close();
            
        }

        private void buttonSPK_Click(object sender, EventArgs e)
        {
            this.Hide();
            Admin_FormSPK formSPK = new Admin_FormSPK();
            formSPK.Owner = this;
            formSPK.ShowDialog(); this.Show();
            //this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            TambahMesin mesin = new TambahMesin();
            mesin.ShowDialog(); this.Show();
            //this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            TambahBahanBaku tbb = new TambahBahanBaku();
            tbb.Owner = this;
            tbb.ShowDialog();
            this.Show();
        }

        private void buttonProgress_Click(object sender, EventArgs e)
        {
            this.Hide();
            ProgresProduksi pro = new ProgresProduksi();
            pro.Owner = this;
            pro.ShowDialog();
            this.Show();
        }

        private void buttonPesanBahanBaku_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormPesan fp = new FormPesan();
            fp.Owner = this;
            fp.ShowDialog();
            this.Show();
        }

        private void buttonPenggunaan_Click(object sender, EventArgs e)
        {
            this.Hide();
            Admin_FormPenggunaanBahanBaku a = new Admin_FormPenggunaanBahanBaku();
            a.Owner = this;
            a.ShowDialog();
            this.Show();
        }

        private void buttonPenerimaan_Click(object sender, EventArgs e)
        {
            this.Hide();
            Admin_FormPenerimaanBarangJadi b = new Admin_FormPenerimaanBarangJadi();
            b.Owner = this;
            b.ShowDialog();
            this.Show();
        }

        private void buttonPengiriman_Click(object sender, EventArgs e)
        {
            this.Hide();
            Admin_FormPengiriman p = new Admin_FormPengiriman();
            p.Owner = this;
            p.ShowDialog();
            this.Show();
        }
    }
}
