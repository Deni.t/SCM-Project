﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormBuatSPK
{
    public class User
    {
        private string id;
        private string nama;
        private string alamat;
        private string telepon;
        private string username;
        private string password;
        private Jabatan jabatan;

        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Nama
        {
            get { return nama; }
            set { nama = value; }
        }

        public string Alamat
        {
            get { return alamat; }
            set { alamat = value; }
        }

        public string Telepon
        {
            get { return telepon; }
            set { telepon = value; }
        }

        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public Jabatan Jabatan
        {
            get { return jabatan; }
        }

        public User(string pId, string pNama)
        {
            id = pId;
            nama = pNama;
        }
        public User(string pId, string pNama, string pAlamat, string pTelp, string pUser, string pPass, Jabatan pJab)
        {
            id = pId;
            nama = pNama;
            alamat = pAlamat;
            telepon = pTelp;
            username = pUser;
            password = pPass;
            jabatan = pJab;
        }
    }
}
