﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormBuatSPK
{
    public partial class FormSPK : Form
    {
        public FormSPK()
        {
            InitializeComponent();
        }

        private void buttonBuatSPK_Click(object sender, EventArgs e)
        {
            
        }

        private void FormSPK_Load(object sender, EventArgs e)
        {
            CenterToScreen();
            labelJam.Text = DateTime.Now.ToString("hh:mm");
            labelTgl.Text = DateTime.Now.ToString("dd-MM-yy");
            DaftarUser daftar = new DaftarUser();
            string hasil = daftar.BacaUser();
            if (hasil == "sukses")
            {
                comboBoxPJ.Items.Clear();
                for (int i = 0; i < daftar.JumlahUser; i++)
                {
                    comboBoxPJ.Items.Add(daftar.DaftarListUser[i].Id + " - " + daftar.DaftarListUser[i].Nama);

                }
                comboBoxPJ.SelectedIndex = 0;
            }
            else
            {
                MessageBox.Show("Penanggung jawab gagal load");
            }
            DaftarKustomer daftarCus = new DaftarKustomer();
            hasil = daftarCus.BacaCustomer();
            if (hasil == "sukses")
            {
                comboBoxKodPem.Items.Clear();
                for (int i = 0; i < daftarCus.JumlahCustomer; i++)
                {
                    comboBoxKodPem.Items.Add(daftarCus.ListCustomer[i].Kode);
                }
                comboBoxKodPem.SelectedIndex = 0;
            }
        }

        private void comboBoxPJ_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxKodPem_SelectedIndexChanged(object sender, EventArgs e)
        {
            

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxKodPem_SelectedValueChanged(object sender, EventArgs e)
        {
           
        }

        private void buttonBuatSPK_Click_1(object sender, EventArgs e)
        {
            Customer cus = new Customer(comboBoxKodPem.Text, textBoxNamPem.Text, textBoxAlamPem.Text);
            string kode = comboBoxPJ.Text.Substring(0, 4);
            string nama = "";
            User us = new User(kode, nama);
            SPK spk = new SPK(textBoxNoSpk.Text, DateTime.Now, textBoxPeker.Text,
                              int.Parse(textBoxBiaya.Text), int.Parse(textBoxLama.Text), textBoxSyarat.Text, 0, "-", cus, us, textBoxKodeProd.Text, dateTimePickerTgl.Value);
            DaftarSpk daftar = new DaftarSpk();

            string hasil = daftar.TambahData(spk);
            if (hasil == "sukses")
            {
                MessageBox.Show("berhasil ditambahkan");
            }
            else
            {
                MessageBox.Show("gagal" + hasil);
            }
        }

        private void comboBoxKodPem_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            string kode = comboBoxKodPem.Text;
            DaftarKustomer daftar = new DaftarKustomer();
            daftar.Cari("kodeCustomer", kode);
            textBoxNamPem.Text = daftar.ListCustomer[0].Nama;
            textBoxAlamPem.Text = daftar.ListCustomer[0].Alamat;
        }

        private void buttonKembali1_Click(object sender, EventArgs e)
        {
            
        }

        private void FormSPK_FormClosing(object sender, FormClosingEventArgs e)
        {
            

        }
    }
}
