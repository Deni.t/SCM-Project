﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormBuatSPK
{
    public class SPK
    {
        #region datamember
        private string noSpk;
        private DateTime tgl;
        private string pekerjaan;
        private int biaya;
        private int lama;
        private string syarat;
        private int status;
        private string komentar;
        private Customer customer;
        private User user;
        private string kodeProduk;
        private DateTime tglMulai;
        #endregion

        #region properties
        public string NoSpk
        {
            get { return noSpk; }
            set { noSpk = value; }
        }

        public DateTime Tgl
        {
            get { return tgl; }
            set { tgl = value; }
        }

        public string Pekerjaan
        {
            get { return pekerjaan; }
            set { pekerjaan = value; }
        }

        public int Biaya
        {
            get { return biaya; }
            set { biaya = value; }
        }

        public int Lama
        {
            get { return lama; }
            set { lama = value; }
        }

        public string Syarat
        {
            get { return syarat; }
            set { syarat = value; }
        }

        public int Status
        {
            get { return status; }
            set { status = value; }
        }

        public string Komentar
        {
            get { return komentar; }
            set { komentar = value; }
        }

        public Customer Customer
        {
            get { return customer; }
        }

        public User User
        {
            get { return user; }
        }

        public string KodeProduk
        {
            get { return kodeProduk; }
            set { kodeProduk = value; }
        }

        public DateTime TglMulai
        {
            get { return tglMulai; }
            set { tglMulai = value; }
        }
        #endregion

        public SPK(string pNospk, DateTime ptgl, string pPker, int pBiaya, int pLama, string pSyarat, int pStatus, string pKomen, Customer cus, User us, string kodeProd, DateTime pMulai)
        {
            noSpk = pNospk;
            tgl = ptgl;
            pekerjaan = pPker;
            biaya = pBiaya;
            lama = pLama;
            syarat = pSyarat;
            status = pStatus;
            komentar = pKomen;
            customer = cus;
            user = us;
            kodeProduk = kodeProd;
            tglMulai = pMulai;
        }
    }
}
