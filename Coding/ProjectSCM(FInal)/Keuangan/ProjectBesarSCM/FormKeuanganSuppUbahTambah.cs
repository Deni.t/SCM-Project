﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace ProjectBesarSCM
{
    public partial class FormKeuanganSuppUbahTambah : Form
    {
        Koneksi k;
        public FormKeuanganSuppUbahTambah()
        {
            InitializeComponent();
            loadComboBox();
            loadComboBox2();
        }
        public void loadComboBox()
        {
            k = new Koneksi();
            k.Connect();

            MySqlCommand cmd = new MySqlCommand("SELECT * FROM supplier", k.KoneksiDB);
            try
            {
                MySqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    cmbSupplier.Items.Add(rdr["kodeSupplier"] + " - " + rdr["namaSupplier"]);
                }
                //MessageBox.Show("Koneksi berhasil");
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void loadComboBox2()
        {
            k = new Koneksi();
            k.Connect();

            MySqlCommand cmd = new MySqlCommand("SELECT * FROM bahanbaku", k.KoneksiDB);
            try
            {
                MySqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    cmbBahanBaku.Items.Add(rdr["kodeBahanBaku"] + " - " + rdr["nama"]);
                }
                //MessageBox.Show("Koneksi berhasil");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FormKeuanganSuppUbahTambah_Load(object sender, EventArgs e)
        {
            string Jam = DateTime.Now.ToString("dd-MM-yyyy");
            string Waktu = DateTime.Now.ToString("HH:mm");
            labelWaktu.Text = Waktu;
            labelJam.Text = Jam;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            k = new Koneksi();
            k.Connect();

            string bahanBaku = cmbBahanBaku.Text;
            string supplier = cmbSupplier.Text;
            int harga = int.Parse(txtHargaSatuan.Text);

            try
            {
                string insert = "insert into detilbahanbakusupplier(kodeBahanBaku,kodeSupplier,hargaSatuan) " +
                    "values('" + bahanBaku.ToString().Substring(0, 5) + "','" + supplier.ToString().Substring(0, 2) + "','" + harga + "')";

                MySqlCommand cmd = new MySqlCommand(insert, k.KoneksiDB);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Data berhasil ditambahkan");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Data sudah ada dengan kode sebagai berikut: "+"\nkode bahan baku = " + bahanBaku.ToString().Substring(0, 5) + "\nkode supplier = " + supplier.ToString().Substring(0, 2));
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormKeuanganSupp formSP = new FormKeuanganSupp();
            formSP.Show(this);
            this.Hide();
        }
    }
}
