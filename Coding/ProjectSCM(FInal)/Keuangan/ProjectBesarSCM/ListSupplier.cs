﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace ProjectBesarSCM
{
    public class ListSupplier
    {
        private List<Supplier> listSupplier;
         private int kode;
        
        public List<Supplier> LSupplier
        {
            get { return listSupplier; }
        }
        public int Kode
        {
            get { return kode; }
        }
        public int JumlahList
        {
            get { return listSupplier.Count; }
        }

        public ListSupplier()
        {
            listSupplier = new List<Supplier>();
            kode = 1;
        }

        #region Method
        public string BacaSemuaData()
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "SELECT * FROM Supplier";
            MySqlCommand C = new MySqlCommand(sql, K.KoneksiDB);
            try
            {
                MySqlDataReader Data = C.ExecuteReader();
                while (Data.Read() == true)
                {
                    string kode = Data.GetValue(0).ToString();
                    string nama = Data.GetValue(1).ToString();
                    string Alamat = Data.GetValue(2).ToString();
                    Supplier Sup = new Supplier(kode, nama, Alamat);
                    listSupplier.Add(Sup);
                }
                C.Dispose();
                Data.Dispose();
                return "sukses";
            }
            catch(Exception E)
            {
                return E.Message;
            }
        }
        // INI CONTEKAN KU
        public string CariData(string kriteria, string nilaiKriteria)
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "SELECT * FROM supplier WHERE " + kriteria + " LIKE '%" + nilaiKriteria + "%'";
            MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
            try
            {
                MySqlDataReader data = MSC.ExecuteReader(); //gunakan MySqlReader dan ExecuteReader untuk menjalankan perintah SELECT

                while (data.Read() == true) //Selama data reader masih bisa membaca (selama masih ada data)
                {
                    string kode = data.GetValue(0).ToString();
                    string nama = data.GetValue(1).ToString();
                    string Alamat = data.GetValue(2).ToString();
                    Supplier Sup = new Supplier(kode, nama, Alamat);
                    listSupplier.Add(Sup);
                }
                MSC.Dispose(); //hapus MySqlCommand setelah selesai
                data.Dispose(); //hapus data reader setelah selesai

                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        //public string GenerateKode()
        //{
        //    Koneksi k = new Koneksi();
        //    k.Connect();
        //    string sql = "SELECT KodeSupplier FROM supplier ORDER BY KodeSupplier DESC LIMIT 1";
        //    MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB); //membuat mysqlcommandnya
        //    try
        //    {
        //        MySqlDataReader data = c.ExecuteReader(); //gunakan MySqlReader dan ExecuteReader untuk menjalankan perintah SELECT

        //        if (data.Read() == true)//jika data reader masih bisa membaca(masih ada data)
        //        {
        //            int kdTerbaru = int.Parse(data.GetValue(0).ToString()) + 1; //mendapatkan kode kategori dari hasil data reader
                   
        //            kode = kdTerbaru;

        //        }
        //        c.Dispose(); //hapus MySqlCommand setelah selesai
        //        data.Dispose(); //hapus data reader setelah selesai

        //        return "sukses";
        //    }
        //    catch (Exception e)
        //    {
        //        return e.Message;
        //    }
        //}
        public string TambahData(Supplier S)
        {
            Koneksi k = new Koneksi();
            k.Connect();
            string sql = "INSERT INTO supplier(KodeSupplier, Nama, Alamat) VALUES (" + S.KodeSup + ",'" + S.NamaSup + "','" + S.Alamat + "')";
            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB);

            try
            {
                c.ExecuteNonQuery();
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        public string UbahData(Supplier S)
        {
            Koneksi k = new Koneksi();
            k.Connect();
            string sql = "UPDATE supplier SET Nama = '" + S.NamaSup + "', Alamat = '" + S.Alamat + "' WHERE KodeSupplier = " + S.KodeSup;
            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB);

            try
            {
                c.ExecuteNonQuery();
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        public string HapusData(Supplier S)
        {
            Koneksi k = new Koneksi();
            k.Connect();
            string sql = "DELETE FROM supplier WHERE KodeSupplier = " + S.KodeSup;
            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB);

            try
            {
                c.ExecuteNonQuery();
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }


        #endregion
    }
}
