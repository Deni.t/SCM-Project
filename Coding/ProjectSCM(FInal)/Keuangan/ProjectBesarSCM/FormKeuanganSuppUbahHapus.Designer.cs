﻿namespace ProjectBesarSCM
{
    partial class FormKeuanganSuppUbahHapus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelHello = new System.Windows.Forms.Label();
            this.labelSupplier = new System.Windows.Forms.Label();
            this.cmbSupplier = new System.Windows.Forms.ComboBox();
            this.cmbBahanBaku = new System.Windows.Forms.ComboBox();
            this.labelBahanBaku = new System.Windows.Forms.Label();
            this.labelRp = new System.Windows.Forms.Label();
            this.labelHarga = new System.Windows.Forms.Label();
            this.labelHargaSatuan = new System.Windows.Forms.Label();
            this.labelJam = new System.Windows.Forms.Label();
            this.labelWaktu = new System.Windows.Forms.Label();
            this.btnHapus = new System.Windows.Forms.Button();
            this.btnKembali = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelHello
            // 
            this.labelHello.AutoSize = true;
            this.labelHello.Location = new System.Drawing.Point(556, 16);
            this.labelHello.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelHello.Name = "labelHello";
            this.labelHello.Size = new System.Drawing.Size(56, 17);
            this.labelHello.TabIndex = 4;
            this.labelHello.Text = "Hello  , ";
            // 
            // labelSupplier
            // 
            this.labelSupplier.AutoSize = true;
            this.labelSupplier.Location = new System.Drawing.Point(325, 144);
            this.labelSupplier.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelSupplier.Name = "labelSupplier";
            this.labelSupplier.Size = new System.Drawing.Size(87, 17);
            this.labelSupplier.TabIndex = 8;
            this.labelSupplier.Text = "SUPPLIER : ";
            // 
            // cmbSupplier
            // 
            this.cmbSupplier.FormattingEnabled = true;
            this.cmbSupplier.Location = new System.Drawing.Point(416, 140);
            this.cmbSupplier.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbSupplier.Name = "cmbSupplier";
            this.cmbSupplier.Size = new System.Drawing.Size(192, 24);
            this.cmbSupplier.TabIndex = 9;
            // 
            // cmbBahanBaku
            // 
            this.cmbBahanBaku.FormattingEnabled = true;
            this.cmbBahanBaku.Location = new System.Drawing.Point(416, 174);
            this.cmbBahanBaku.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbBahanBaku.Name = "cmbBahanBaku";
            this.cmbBahanBaku.Size = new System.Drawing.Size(192, 24);
            this.cmbBahanBaku.TabIndex = 11;
            this.cmbBahanBaku.SelectedIndexChanged += new System.EventHandler(this.cmbBahanBaku_SelectedIndexChanged);
            // 
            // labelBahanBaku
            // 
            this.labelBahanBaku.AutoSize = true;
            this.labelBahanBaku.Location = new System.Drawing.Point(305, 177);
            this.labelBahanBaku.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelBahanBaku.Name = "labelBahanBaku";
            this.labelBahanBaku.Size = new System.Drawing.Size(108, 17);
            this.labelBahanBaku.TabIndex = 10;
            this.labelBahanBaku.Text = "BAHAN BAKU : ";
            // 
            // labelRp
            // 
            this.labelRp.AutoSize = true;
            this.labelRp.Location = new System.Drawing.Point(416, 215);
            this.labelRp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelRp.Name = "labelRp";
            this.labelRp.Size = new System.Drawing.Size(30, 17);
            this.labelRp.TabIndex = 18;
            this.labelRp.Text = "Rp.";
            // 
            // labelHarga
            // 
            this.labelHarga.AutoSize = true;
            this.labelHarga.Location = new System.Drawing.Point(456, 215);
            this.labelHarga.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelHarga.Name = "labelHarga";
            this.labelHarga.Size = new System.Drawing.Size(16, 17);
            this.labelHarga.TabIndex = 17;
            this.labelHarga.Text = "0";
            // 
            // labelHargaSatuan
            // 
            this.labelHargaSatuan.AutoSize = true;
            this.labelHargaSatuan.Location = new System.Drawing.Point(284, 215);
            this.labelHargaSatuan.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelHargaSatuan.Name = "labelHargaSatuan";
            this.labelHargaSatuan.Size = new System.Drawing.Size(129, 17);
            this.labelHargaSatuan.TabIndex = 16;
            this.labelHargaSatuan.Text = "HARGA SATUAN : ";
            // 
            // labelJam
            // 
            this.labelJam.AutoSize = true;
            this.labelJam.Location = new System.Drawing.Point(16, 512);
            this.labelJam.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelJam.Name = "labelJam";
            this.labelJam.Size = new System.Drawing.Size(34, 17);
            this.labelJam.TabIndex = 20;
            this.labelJam.Text = "Jam";
            // 
            // labelWaktu
            // 
            this.labelWaktu.AutoSize = true;
            this.labelWaktu.Location = new System.Drawing.Point(16, 530);
            this.labelWaktu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelWaktu.Name = "labelWaktu";
            this.labelWaktu.Size = new System.Drawing.Size(48, 17);
            this.labelWaktu.TabIndex = 19;
            this.labelWaktu.Text = "Waktu";
            // 
            // btnHapus
            // 
            this.btnHapus.BackColor = System.Drawing.Color.DarkGray;
            this.btnHapus.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHapus.Location = new System.Drawing.Point(268, 357);
            this.btnHapus.Name = "btnHapus";
            this.btnHapus.Size = new System.Drawing.Size(293, 56);
            this.btnHapus.TabIndex = 29;
            this.btnHapus.Text = "HAPUS";
            this.btnHapus.UseVisualStyleBackColor = false;
            this.btnHapus.Click += new System.EventHandler(this.btnHapus_Click);
            // 
            // btnKembali
            // 
            this.btnKembali.BackColor = System.Drawing.Color.DarkGray;
            this.btnKembali.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKembali.Location = new System.Drawing.Point(683, 494);
            this.btnKembali.Name = "btnKembali";
            this.btnKembali.Size = new System.Drawing.Size(112, 48);
            this.btnKembali.TabIndex = 30;
            this.btnKembali.Text = "KEMBALI";
            this.btnKembali.UseVisualStyleBackColor = false;
            this.btnKembali.Click += new System.EventHandler(this.btnKembali_Click);
            // 
            // FormKeuanganSuppUbahHapus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ProjectBesarSCM.Properties.Resources.Keuangan_Supp_Ubah_Hapus;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(797, 558);
            this.Controls.Add(this.btnKembali);
            this.Controls.Add(this.btnHapus);
            this.Controls.Add(this.labelJam);
            this.Controls.Add(this.labelWaktu);
            this.Controls.Add(this.labelRp);
            this.Controls.Add(this.labelHarga);
            this.Controls.Add(this.labelHargaSatuan);
            this.Controls.Add(this.cmbBahanBaku);
            this.Controls.Add(this.labelBahanBaku);
            this.Controls.Add(this.cmbSupplier);
            this.Controls.Add(this.labelSupplier);
            this.Controls.Add(this.labelHello);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FormKeuanganSuppUbahHapus";
            this.Text = "FormKeuanganSuppUbahHapus";
            this.Load += new System.EventHandler(this.FormKeuanganSuppUbahHapus_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelHello;
        private System.Windows.Forms.Label labelSupplier;
        private System.Windows.Forms.ComboBox cmbSupplier;
        private System.Windows.Forms.ComboBox cmbBahanBaku;
        private System.Windows.Forms.Label labelBahanBaku;
        private System.Windows.Forms.Label labelRp;
        private System.Windows.Forms.Label labelHarga;
        private System.Windows.Forms.Label labelHargaSatuan;
        private System.Windows.Forms.Label labelJam;
        private System.Windows.Forms.Label labelWaktu;
        private System.Windows.Forms.Button btnHapus;
        private System.Windows.Forms.Button btnKembali;
    }
}