﻿namespace ProjectBesarSCM
{
    partial class FormKeuanganHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormKeuanganHome));
            this.labelWaktu = new System.Windows.Forms.Label();
            this.labelJam = new System.Windows.Forms.Label();
            this.labelHello = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonPembelian = new System.Windows.Forms.Button();
            this.buttonTransaksiCustomer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelWaktu
            // 
            this.labelWaktu.AutoSize = true;
            this.labelWaktu.Location = new System.Drawing.Point(5, 340);
            this.labelWaktu.Name = "labelWaktu";
            this.labelWaktu.Size = new System.Drawing.Size(39, 13);
            this.labelWaktu.TabIndex = 0;
            this.labelWaktu.Text = "Waktu";
            // 
            // labelJam
            // 
            this.labelJam.AutoSize = true;
            this.labelJam.Location = new System.Drawing.Point(5, 325);
            this.labelJam.Name = "labelJam";
            this.labelJam.Size = new System.Drawing.Size(26, 13);
            this.labelJam.TabIndex = 1;
            this.labelJam.Text = "Jam";
            // 
            // labelHello
            // 
            this.labelHello.AutoSize = true;
            this.labelHello.Location = new System.Drawing.Point(584, 9);
            this.labelHello.Name = "labelHello";
            this.labelHello.Size = new System.Drawing.Size(43, 13);
            this.labelHello.TabIndex = 4;
            this.labelHello.Text = "Hello  , ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(113, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 46);
            this.label1.TabIndex = 5;
            this.label1.Text = "Pembelian\r\nBahan Baku";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(375, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 46);
            this.label2.TabIndex = 6;
            this.label2.Text = "Transaksi \r\nCustomer";
            // 
            // buttonPembelian
            // 
            this.buttonPembelian.BackColor = System.Drawing.Color.Transparent;
            this.buttonPembelian.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonPembelian.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonPembelian.FlatAppearance.BorderSize = 0;
            this.buttonPembelian.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonPembelian.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonPembelian.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonPembelian.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPembelian.Location = new System.Drawing.Point(71, 63);
            this.buttonPembelian.Name = "buttonPembelian";
            this.buttonPembelian.Size = new System.Drawing.Size(205, 102);
            this.buttonPembelian.TabIndex = 7;
            this.buttonPembelian.Text = "Pembelian\r\nBahan Baku";
            this.buttonPembelian.UseVisualStyleBackColor = false;
            this.buttonPembelian.Click += new System.EventHandler(this.buttonPembelian_Click);
            // 
            // buttonTransaksiCustomer
            // 
            this.buttonTransaksiCustomer.BackColor = System.Drawing.Color.Transparent;
            this.buttonTransaksiCustomer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonTransaksiCustomer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonTransaksiCustomer.FlatAppearance.BorderSize = 0;
            this.buttonTransaksiCustomer.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonTransaksiCustomer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonTransaksiCustomer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTransaksiCustomer.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTransaksiCustomer.Location = new System.Drawing.Point(317, 62);
            this.buttonTransaksiCustomer.Name = "buttonTransaksiCustomer";
            this.buttonTransaksiCustomer.Size = new System.Drawing.Size(205, 102);
            this.buttonTransaksiCustomer.TabIndex = 8;
            this.buttonTransaksiCustomer.Text = "Transaksi\r\nCustomer";
            this.buttonTransaksiCustomer.UseVisualStyleBackColor = false;
            // 
            // FormKeuanganHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(838, 358);
            this.Controls.Add(this.buttonTransaksiCustomer);
            this.Controls.Add(this.buttonPembelian);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelHello);
            this.Controls.Add(this.labelJam);
            this.Controls.Add(this.labelWaktu);
            this.Name = "FormKeuanganHome";
            this.Text = "FormKeuanganHome";
            this.Load += new System.EventHandler(this.FormKeuanganHome_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelWaktu;
        private System.Windows.Forms.Label labelJam;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonPembelian;
        private System.Windows.Forms.Button buttonTransaksiCustomer;
        public System.Windows.Forms.Label labelHello;
    }
}