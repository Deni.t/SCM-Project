﻿namespace ProjectBesarSCM
{
    partial class FormKeuanganSupp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelHello = new System.Windows.Forms.Label();
            this.labelJam = new System.Windows.Forms.Label();
            this.labelWaktu = new System.Windows.Forms.Label();
            this.buttonTambahSupplier = new System.Windows.Forms.Button();
            this.buttonUpdateSupplier = new System.Windows.Forms.Button();
            this.buttonHapusSupplier = new System.Windows.Forms.Button();
            this.panelTambah = new System.Windows.Forms.Panel();
            this.panelHapus = new System.Windows.Forms.Panel();
            this.panelUpdate = new System.Windows.Forms.Panel();
            this.buttonUbahSupplier = new System.Windows.Forms.Button();
            this.buttonHapusBahan = new System.Windows.Forms.Button();
            this.buttonTambahBahan = new System.Windows.Forms.Button();
            this.buttonHapus = new System.Windows.Forms.Button();
            this.comboBoxSupplier = new System.Windows.Forms.ComboBox();
            this.textBoxAlamat2 = new System.Windows.Forms.TextBox();
            this.labelAlamat2 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonSimpan = new System.Windows.Forms.Button();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.textBoxAlamat = new System.Windows.Forms.TextBox();
            this.textBoxSupplier = new System.Windows.Forms.TextBox();
            this.labelID = new System.Windows.Forms.Label();
            this.labelAlamat = new System.Windows.Forms.Label();
            this.labelSupplier = new System.Windows.Forms.Label();
            this.panelTambah.SuspendLayout();
            this.panelHapus.SuspendLayout();
            this.panelUpdate.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelHello
            // 
            this.labelHello.AutoSize = true;
            this.labelHello.Location = new System.Drawing.Point(504, 14);
            this.labelHello.Name = "labelHello";
            this.labelHello.Size = new System.Drawing.Size(43, 13);
            this.labelHello.TabIndex = 0;
            this.labelHello.Text = "Hello  , ";
            // 
            // labelJam
            // 
            this.labelJam.AutoSize = true;
            this.labelJam.Location = new System.Drawing.Point(12, 465);
            this.labelJam.Name = "labelJam";
            this.labelJam.Size = new System.Drawing.Size(26, 13);
            this.labelJam.TabIndex = 5;
            this.labelJam.Text = "Jam";
            // 
            // labelWaktu
            // 
            this.labelWaktu.AutoSize = true;
            this.labelWaktu.Location = new System.Drawing.Point(12, 480);
            this.labelWaktu.Name = "labelWaktu";
            this.labelWaktu.Size = new System.Drawing.Size(39, 13);
            this.labelWaktu.TabIndex = 4;
            this.labelWaktu.Text = "Waktu";
            // 
            // buttonTambahSupplier
            // 
            this.buttonTambahSupplier.Location = new System.Drawing.Point(116, 85);
            this.buttonTambahSupplier.Name = "buttonTambahSupplier";
            this.buttonTambahSupplier.Size = new System.Drawing.Size(126, 102);
            this.buttonTambahSupplier.TabIndex = 6;
            this.buttonTambahSupplier.Text = "TAMBAH SUPPLIER";
            this.buttonTambahSupplier.UseVisualStyleBackColor = true;
            this.buttonTambahSupplier.Visible = false;
            // 
            // buttonUpdateSupplier
            // 
            this.buttonUpdateSupplier.Location = new System.Drawing.Point(288, 86);
            this.buttonUpdateSupplier.Name = "buttonUpdateSupplier";
            this.buttonUpdateSupplier.Size = new System.Drawing.Size(126, 102);
            this.buttonUpdateSupplier.TabIndex = 7;
            this.buttonUpdateSupplier.Text = "UBAH DETIL SUPPLIER";
            this.buttonUpdateSupplier.UseVisualStyleBackColor = true;
            this.buttonUpdateSupplier.Visible = false;
            // 
            // buttonHapusSupplier
            // 
            this.buttonHapusSupplier.Location = new System.Drawing.Point(460, 85);
            this.buttonHapusSupplier.Name = "buttonHapusSupplier";
            this.buttonHapusSupplier.Size = new System.Drawing.Size(126, 102);
            this.buttonHapusSupplier.TabIndex = 8;
            this.buttonHapusSupplier.Text = "HAPUS SUPPLIER";
            this.buttonHapusSupplier.UseVisualStyleBackColor = true;
            this.buttonHapusSupplier.Visible = false;
            // 
            // panelTambah
            // 
            this.panelTambah.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelTambah.Controls.Add(this.panelHapus);
            this.panelTambah.Controls.Add(this.buttonSimpan);
            this.panelTambah.Controls.Add(this.textBoxID);
            this.panelTambah.Controls.Add(this.textBoxAlamat);
            this.panelTambah.Controls.Add(this.textBoxSupplier);
            this.panelTambah.Controls.Add(this.labelID);
            this.panelTambah.Controls.Add(this.labelAlamat);
            this.panelTambah.Controls.Add(this.labelSupplier);
            this.panelTambah.Location = new System.Drawing.Point(114, 217);
            this.panelTambah.Name = "panelTambah";
            this.panelTambah.Size = new System.Drawing.Size(470, 239);
            this.panelTambah.TabIndex = 9;
            this.panelTambah.Visible = false;
            // 
            // panelHapus
            // 
            this.panelHapus.Controls.Add(this.panelUpdate);
            this.panelHapus.Controls.Add(this.buttonHapus);
            this.panelHapus.Controls.Add(this.comboBoxSupplier);
            this.panelHapus.Controls.Add(this.textBoxAlamat2);
            this.panelHapus.Controls.Add(this.labelAlamat2);
            this.panelHapus.Controls.Add(this.label2);
            this.panelHapus.Location = new System.Drawing.Point(0, 0);
            this.panelHapus.Name = "panelHapus";
            this.panelHapus.Size = new System.Drawing.Size(472, 236);
            this.panelHapus.TabIndex = 7;
            this.panelHapus.Visible = false;
            // 
            // panelUpdate
            // 
            this.panelUpdate.Controls.Add(this.buttonUbahSupplier);
            this.panelUpdate.Controls.Add(this.buttonHapusBahan);
            this.panelUpdate.Controls.Add(this.buttonTambahBahan);
            this.panelUpdate.Location = new System.Drawing.Point(1, 1);
            this.panelUpdate.Name = "panelUpdate";
            this.panelUpdate.Size = new System.Drawing.Size(472, 233);
            this.panelUpdate.TabIndex = 11;
            this.panelUpdate.Visible = false;
            // 
            // buttonUbahSupplier
            // 
            this.buttonUbahSupplier.Location = new System.Drawing.Point(328, 16);
            this.buttonUbahSupplier.Name = "buttonUbahSupplier";
            this.buttonUbahSupplier.Size = new System.Drawing.Size(135, 39);
            this.buttonUbahSupplier.TabIndex = 2;
            this.buttonUbahSupplier.Text = "UBAH SUPPLIER";
            this.buttonUbahSupplier.UseVisualStyleBackColor = true;
            // 
            // buttonHapusBahan
            // 
            this.buttonHapusBahan.Location = new System.Drawing.Point(160, 164);
            this.buttonHapusBahan.Name = "buttonHapusBahan";
            this.buttonHapusBahan.Size = new System.Drawing.Size(154, 39);
            this.buttonHapusBahan.TabIndex = 1;
            this.buttonHapusBahan.Text = "HAPUS BAHAN";
            this.buttonHapusBahan.UseVisualStyleBackColor = true;
            // 
            // buttonTambahBahan
            // 
            this.buttonTambahBahan.Location = new System.Drawing.Point(21, 14);
            this.buttonTambahBahan.Name = "buttonTambahBahan";
            this.buttonTambahBahan.Size = new System.Drawing.Size(139, 41);
            this.buttonTambahBahan.TabIndex = 0;
            this.buttonTambahBahan.Text = "TAMBAH BAHAN";
            this.buttonTambahBahan.UseVisualStyleBackColor = true;
            // 
            // buttonHapus
            // 
            this.buttonHapus.Location = new System.Drawing.Point(295, 194);
            this.buttonHapus.Name = "buttonHapus";
            this.buttonHapus.Size = new System.Drawing.Size(75, 23);
            this.buttonHapus.TabIndex = 10;
            this.buttonHapus.Text = "HAPUS";
            this.buttonHapus.UseVisualStyleBackColor = true;
            // 
            // comboBoxSupplier
            // 
            this.comboBoxSupplier.FormattingEnabled = true;
            this.comboBoxSupplier.Location = new System.Drawing.Point(161, 17);
            this.comboBoxSupplier.Name = "comboBoxSupplier";
            this.comboBoxSupplier.Size = new System.Drawing.Size(209, 21);
            this.comboBoxSupplier.TabIndex = 9;
            // 
            // textBoxAlamat2
            // 
            this.textBoxAlamat2.Location = new System.Drawing.Point(161, 43);
            this.textBoxAlamat2.Name = "textBoxAlamat2";
            this.textBoxAlamat2.Size = new System.Drawing.Size(209, 20);
            this.textBoxAlamat2.TabIndex = 8;
            // 
            // labelAlamat2
            // 
            this.labelAlamat2.AutoSize = true;
            this.labelAlamat2.Location = new System.Drawing.Point(113, 46);
            this.labelAlamat2.Name = "labelAlamat2";
            this.labelAlamat2.Size = new System.Drawing.Size(48, 13);
            this.labelAlamat2.TabIndex = 6;
            this.labelAlamat2.Text = "Alamat : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(107, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Supplier : ";
            // 
            // buttonSimpan
            // 
            this.buttonSimpan.Location = new System.Drawing.Point(334, 194);
            this.buttonSimpan.Name = "buttonSimpan";
            this.buttonSimpan.Size = new System.Drawing.Size(85, 31);
            this.buttonSimpan.TabIndex = 6;
            this.buttonSimpan.Text = "SIMPAN";
            this.buttonSimpan.UseVisualStyleBackColor = true;
            // 
            // textBoxID
            // 
            this.textBoxID.Location = new System.Drawing.Point(283, 14);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.Size = new System.Drawing.Size(100, 20);
            this.textBoxID.TabIndex = 5;
            // 
            // textBoxAlamat
            // 
            this.textBoxAlamat.Location = new System.Drawing.Point(73, 40);
            this.textBoxAlamat.Name = "textBoxAlamat";
            this.textBoxAlamat.Size = new System.Drawing.Size(234, 20);
            this.textBoxAlamat.TabIndex = 4;
            // 
            // textBoxSupplier
            // 
            this.textBoxSupplier.Location = new System.Drawing.Point(73, 14);
            this.textBoxSupplier.Name = "textBoxSupplier";
            this.textBoxSupplier.Size = new System.Drawing.Size(161, 20);
            this.textBoxSupplier.TabIndex = 3;
            // 
            // labelID
            // 
            this.labelID.AutoSize = true;
            this.labelID.Location = new System.Drawing.Point(257, 17);
            this.labelID.Name = "labelID";
            this.labelID.Size = new System.Drawing.Size(27, 13);
            this.labelID.TabIndex = 2;
            this.labelID.Text = "ID : ";
            // 
            // labelAlamat
            // 
            this.labelAlamat.AutoSize = true;
            this.labelAlamat.Location = new System.Drawing.Point(25, 43);
            this.labelAlamat.Name = "labelAlamat";
            this.labelAlamat.Size = new System.Drawing.Size(48, 13);
            this.labelAlamat.TabIndex = 1;
            this.labelAlamat.Text = "Alamat : ";
            // 
            // labelSupplier
            // 
            this.labelSupplier.AutoSize = true;
            this.labelSupplier.Location = new System.Drawing.Point(19, 17);
            this.labelSupplier.Name = "labelSupplier";
            this.labelSupplier.Size = new System.Drawing.Size(54, 13);
            this.labelSupplier.TabIndex = 0;
            this.labelSupplier.Text = "Supplier : ";
            // 
            // FormKeuanganSupp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ProjectBesarSCM.Properties.Resources.Keuangan_Supp;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(714, 502);
            this.Controls.Add(this.panelTambah);
            this.Controls.Add(this.buttonHapusSupplier);
            this.Controls.Add(this.buttonUpdateSupplier);
            this.Controls.Add(this.buttonTambahSupplier);
            this.Controls.Add(this.labelJam);
            this.Controls.Add(this.labelWaktu);
            this.Controls.Add(this.labelHello);
            this.Name = "FormKeuanganSupp";
            this.Text = "FormKeuanganSupp";
            this.Load += new System.EventHandler(this.FormKeuanganSupp_Load);
            this.panelTambah.ResumeLayout(false);
            this.panelTambah.PerformLayout();
            this.panelHapus.ResumeLayout(false);
            this.panelHapus.PerformLayout();
            this.panelUpdate.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelHello;
        private System.Windows.Forms.Label labelJam;
        private System.Windows.Forms.Label labelWaktu;
        private System.Windows.Forms.Button buttonTambahSupplier;
        private System.Windows.Forms.Button buttonUpdateSupplier;
        private System.Windows.Forms.Button buttonHapusSupplier;
        private System.Windows.Forms.Panel panelTambah;
        private System.Windows.Forms.Label labelID;
        private System.Windows.Forms.Label labelAlamat;
        private System.Windows.Forms.Label labelSupplier;
        private System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.TextBox textBoxAlamat;
        private System.Windows.Forms.TextBox textBoxSupplier;
        private System.Windows.Forms.Button buttonSimpan;
        private System.Windows.Forms.Panel panelHapus;
        private System.Windows.Forms.ComboBox comboBoxSupplier;
        private System.Windows.Forms.TextBox textBoxAlamat2;
        private System.Windows.Forms.Label labelAlamat2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonHapus;
        private System.Windows.Forms.Panel panelUpdate;
        private System.Windows.Forms.Button buttonUbahSupplier;
        private System.Windows.Forms.Button buttonHapusBahan;
        private System.Windows.Forms.Button buttonTambahBahan;
    }
}