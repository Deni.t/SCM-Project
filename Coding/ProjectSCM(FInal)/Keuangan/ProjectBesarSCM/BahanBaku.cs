﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectBesarSCM
{
    public class BahanBaku
    {
         private string kodeBarang;
        private string namaBarang;
        private string satuan;
        private int stok;
      

        public string KodeBarang
        {
            get { return kodeBarang; }
            set { kodeBarang = value; }
        }
        public string NamaBarang
        {
            get { return namaBarang; }
            set { namaBarang = value; }
        }
        public string Satuan
        {
            get { return satuan; }
            set { satuan = value; }
        }
        public int Stok
        {
            get { return stok; }
            set { stok = value; }
        }
       
        #region Constructor
        public BahanBaku()
        {
            kodeBarang = "";
            namaBarang = "";
            satuan = "";
            stok = 0;
        }
        public BahanBaku(string kode, string nama, string sat, int jumlah)
        {
            kodeBarang = kode;
            namaBarang = nama;
            satuan = sat;
            stok = jumlah;
        }
        #endregion
    }
}
