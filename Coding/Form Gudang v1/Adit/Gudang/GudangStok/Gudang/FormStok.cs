﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SCM;
namespace Gudang
{
    public partial class FormStok : Form
    {
        public FormStok()
        {
            InitializeComponent();
        }

        private void FormStok_Load(object sender, EventArgs e)
        {
            Koneksi k = new Koneksi("localhost",
                                    "scm",
                                    "root",
                                    "");
            string hasil = k.Connect();
            if (hasil == "sukses")
            {
                labelJam.Text = DateTime.Now.ToString("hh:mm");
                labelTgl.Text = DateTime.Now.ToString("dd/MM/yy");
            }
        }

        private void buttonCari_Click(object sender, EventArgs e)
        {
            DaftarStok daftar = new DaftarStok();
            string hasil = daftar.CariData(textBoxNama.Text, textBoxKode.Text);
            if (hasil == "sukses")
            {
                dataGridViewStk.DataSource = daftar.ListStok;
            }
        }
    }
}
