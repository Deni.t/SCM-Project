﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace SCM
{
    public class DaftarStok
    {
        private List<Stok> listStok;

        public List<Stok> ListStok
        {
            get { return listStok; }
        }

        public int JumlahStok
        {
            get { return listStok.Count; }
        }

        public DaftarStok()
        {
            listStok = new List<Stok>();
        }

        public string CariData(string nama, string kode)
        {
            Koneksi k = new Koneksi();
            k.Connect();
            string sql = "select bb.kodebahanbaku, bb.nama, fpd.jumlah, s.namasupplier, dbbs.hargasatuan " +
                         "from bahanbaku bb inner join formpembeliandetil fpd on bb.kodebahanbaku = fpd.kodebahanbaku " +
                         "inner join detilbahanbakusupplier dbbs on dbbs.kodebahanbaku = bb.kodebahanbaku " +
                         "inner join supplier s on s.kodesupplier = dbbs.kodesupplier " +
                         "inner join formpembelian fp on fp.kodesupplier = s.kodesupplier " +
                         "where bb.nama = '" + nama +"' and bb.kodebahanbaku = '" + kode + "'";
            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB);
            try
            {
                MySqlDataReader data = c.ExecuteReader();

                while (data.Read() == true)
                {
                    string kd = data.GetValue(0).ToString();
                    string nm = data.GetValue(1).ToString();
                    int jmlh = int.Parse(data.GetValue(2).ToString());
                    string nmSp = data.GetValue(3).ToString();
                    int hrg = int.Parse(data.GetValue(4).ToString());
                    Stok stk = new Stok(kd, nm, jmlh, nmSp, hrg);
                    ListStok.Add(stk);
                }
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}
