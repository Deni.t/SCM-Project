﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gudang
{
    public partial class FormUtama : Form
    {
        public FormUtama()
        {
            InitializeComponent();
        }

        private void FormUtama_Load(object sender, EventArgs e)
        {
            this.IsMdiContainer = true;
            this.Enabled = false;
            Login frmLogin = new Login();
            frmLogin.Owner = this;
            frmLogin.Show();
            string time = DateTime.Now.ToString("HH:MM");
            string date = DateTime.Now.ToString("dd/MM/yyyy");
            lblTgl2.Text = date;
            lblTime.Text = time;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormDaftarBarang fdb = new FormDaftarBarang();
            fdb.Owner = this;
            fdb.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormKonfrimasi FK = new FormKonfrimasi();
            FK.Owner = this;
            FK.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Laporan laporan = new Laporan();
            laporan.Owner = this;
            laporan.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FormStock fs = new FormStock();
            fs.Owner = this;
            fs.Show();
        }
    }
}
