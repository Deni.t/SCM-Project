﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gudang
{
    public class Stock
    {
        private string kode;
        private string nama;
        private int jumlah;
        private string namaSup;
        private int hargaSat;

        public string KodeBahanBaku
        {
            get { return kode; }
            set { kode = value; }
        }

        public string NamaBahanBaku
        {
            get { return nama; }
            set { nama = value; }
        }

        public int Jumlah
        {
            get { return jumlah; }
            set { jumlah = value; }
        }

        public string NamaSupplier
        {
            get { return namaSup; }
            set { namaSup = value; }
        }

        public int HargaSatuan
        {
            get { return hargaSat; }
            set { hargaSat = value; }
        }

        public Stock()
        {
            kode = "";
            nama = "";
            jumlah = 0;
            namaSup = "";
            hargaSat = 0;
        }

        public Stock(string pKode, string pNama, int pJumlah, string pNamaSup, int pHrg)
        {
            kode = pKode;
            nama = pNama;
            jumlah = pJumlah;
            namaSup = pNamaSup;
            hargaSat = pHrg;
        }
    }
}
