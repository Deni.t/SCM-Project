﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Gudang
{
    public class DaftarBahan
    {
        private List<Bahan> listBahan;
        private string kodeTerbaru;

        #region Properties
        public List<Bahan> ListBahan
        {
            get { return listBahan; }
        }

        public string KodeTerbaru
        {
            get { return kodeTerbaru; }
        }
        public int JumlahBahan
        {
            get { return listBahan.Count; }
        }
        #endregion

        #region Contructor
        public DaftarBahan()
        {
            listBahan = new List<Bahan>();
            kodeTerbaru = "";
        }
        #endregion

        #region Method
        public string BacaSemuaData()
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "select bb.kodebahanbaku, bb.nama, fpd.jumlah, fpd.keterangan, fpd.statuskirim from " +
                "bahanbaku bb inner join formpemesanandetil fpd on bb.kodebahanbaku = fpd.kodebahanbaku " +
                "inner join formpemesanan fp on fpd.nospk = fp.nospk " + "inner join spk s on s.nospk = fp.nospk ";

            MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
            try
            {
                MySqlDataReader Data = MSC.ExecuteReader();
                while (Data.Read() == true)
                {
                    string kode = Data.GetValue(0).ToString();
                    string nama = Data.GetValue(1).ToString();
                    int jumlah = int.Parse(Data.GetValue(2).ToString());
                    string ket = Data.GetValue(3).ToString();
                    int stat = int.Parse(Data.GetValue(4).ToString());
                    Bahan bhn = new Bahan(kode, nama, jumlah, ket, stat);
                    listBahan.Add(bhn);
                }
                MSC.Dispose();
                Data.Dispose();
                return "sukses";
            }
            catch (Exception E)
            {
                return E.Message;
            }
        }

        public string CariData(string kode1)
        {
            Koneksi k = new Koneksi();
            k.Connect();
                string sql = "select bb.kodebahanbaku, bb.nama, fpd.jumlah, fpd.keterangan,fpd.statuskirim from " +
                    "bahanbaku bb inner join formpemesanandetil fpd on bb.kodebahanbaku = fpd.kodebahanbaku " +
                    "inner join formpemesanan fp on fpd.nospk = fp.nospk " + "inner join spk s on s.nospk = fp.nospk " + "where bb.kodebahanbaku = '" + kode1 + "'";
            MySqlCommand MSC = new MySqlCommand(sql, k.KoneksiDB);
            try
            {
                MySqlDataReader Data = MSC.ExecuteReader();
                while (Data.Read() == true)
                {
                    string kode = Data.GetValue(0).ToString();
                    string nama = Data.GetValue(1).ToString();
                    int jumlah = int.Parse(Data.GetValue(2).ToString());
                    string ket = Data.GetValue(3).ToString();
                    int stat = int.Parse(Data.GetValue(4).ToString());
                    Bahan bhn = new Bahan(kode, nama, jumlah, ket, stat);
                    listBahan.Add(bhn);
                }
                MSC.Dispose();
                Data.Dispose();
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }


        }
        public string status()
        {
            Koneksi k = new Koneksi();
            k.Connect();
            string sql = "select bb.kodebahanbaku, bb.nama, fpd.jumlah, fpd.keterangan, fpd.statusKirim from bahanbaku bb inner join formpemesanandetil fpd on bb.kodebahanbaku = fpd.kodebahanbaku inner join formpemesanan fp on fpd.nospk = fp.nospk inner join spk s on s.nospk = fp.nospk where fpd.statusKirim = 1";
            MySqlCommand MSC = new MySqlCommand(sql, k.KoneksiDB);
            try
            {
                MySqlDataReader Data = MSC.ExecuteReader();
                while (Data.Read() == true)
                {

                    string kode = Data.GetValue(0).ToString();
                    string nama = Data.GetValue(1).ToString();
                    int jumlah = int.Parse(Data.GetValue(2).ToString());
                    string ket = Data.GetValue(3).ToString();
                    int stat = int.Parse(Data.GetValue(4).ToString());
                    Bahan bhn = new Bahan(kode, nama, jumlah, ket, stat);
                    listBahan.Add(bhn);
                }
                MSC.Dispose();
                Data.Dispose();
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }


        }
        #endregion
    }
}
