﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gudang;


namespace Gudang
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            this.Height = 50 + panelLogin.Height;

            textBoxServer.Text = "localhost";
            textBoxDataBase.Text = "scm";
            textBoxUsername.Text = "root";
            textBoxUsername.Focus();
        }

        private void linkLabelPengaturan_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Height = 50 + panelLogin.Height + panelPengaturan.Height;
        }

        private void buttonSimpan_Click(object sender, EventArgs e)
        {
            this.Height = 50 + panelLogin.Height;
        }

        private void buttonKeluar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            if (textBoxUsername.Text != "")
            {
                Koneksi k = new Koneksi(textBoxServer.Text, textBoxDataBase.Text, textBoxUsername.Text, textBoxPass.Text);

                string hasilConnect = k.Connect();

                if (hasilConnect == "sukses")
                {

                    MessageBox.Show("Selamat Datang di sistem penjualan pembelian", "Info");
                    FormUtama Fu = (FormUtama)this.Owner;     
                    Fu.Enabled = true;
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Username tidak boleh dikosongi", "Kesalahan");
            }
        }
    }
}
