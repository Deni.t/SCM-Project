﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Gudang
{
    public class DaftarStock
    {
        private List<Stock> listStok;

        public List<Stock> ListStok
        {
            get { return listStok; }
        }

        public int JumlahStok
        {
            get { return listStok.Count; }
        }

        public DaftarStock()
        {
            listStok = new List<Stock>();
        }
        public string BacaSemuaData()
        {
            Koneksi k = new Koneksi();
            k.Connect();
            string sql = "select bb.kodebahanbaku, bb.nama, bb.stok, s.namasupplier, dbbs.hargasatuan " +
                         "from bahanbaku bb inner join formpembeliandetil fpd on bb.kodebahanbaku = fpd.kodebahanbaku " +
                         "inner join detilbahanbakusupplier dbbs on dbbs.kodebahanbaku = bb.kodebahanbaku " +
                         "inner join supplier s on s.kodesupplier = dbbs.kodesupplier " +
                         "inner join formpembelian fp on fp.kodesupplier = s.kodesupplier";
            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB);
            try
            {
                MySqlDataReader data = c.ExecuteReader();

                while (data.Read() == true)
                {
                    string kd = data.GetValue(0).ToString();
                    string nm = data.GetValue(1).ToString();
                    int jmlh = int.Parse(data.GetValue(2).ToString());
                    string nmSp = data.GetValue(3).ToString();
                    int hrg = int.Parse(data.GetValue(4).ToString());
                    Stock stk = new Stock(kd, nm, jmlh, nmSp, hrg);
                    ListStok.Add(stk);
                }
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string CariData(string nama, string kode)
        {
            Koneksi k = new Koneksi();
            k.Connect();
            string sql = "select bb.kodebahanbaku, bb.nama, bb.stok, s.namasupplier, dbbs.hargasatuan " +
                         "from bahanbaku bb inner join formpembeliandetil fpd on bb.kodebahanbaku = fpd.kodebahanbaku " +
                         "inner join detilbahanbakusupplier dbbs on dbbs.kodebahanbaku = bb.kodebahanbaku " +
                         "inner join supplier s on s.kodesupplier = dbbs.kodesupplier " +
                         "inner join formpembelian fp on fp.kodesupplier = s.kodesupplier " +
                         "where bb.nama = '" + nama +"' and bb.kodebahanbaku = '" + kode + "'";
            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB);
            try
            {
                MySqlDataReader data = c.ExecuteReader();

                while (data.Read() == true)
                {
                    string kd = data.GetValue(0).ToString();
                    string nm = data.GetValue(1).ToString();
                    int jmlh = int.Parse(data.GetValue(2).ToString());
                    string nmSp = data.GetValue(3).ToString();
                    int hrg = int.Parse(data.GetValue(4).ToString());
                    Stock stk = new Stock(kd, nm, jmlh, nmSp, hrg);
                    ListStok.Add(stk);
                }
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}
