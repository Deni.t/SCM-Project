﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gudang;

namespace Gudang
{
    public partial class FormKonfrimasi : Form
    {
        public FormKonfrimasi()
        {
            InitializeComponent();
        }
        List<DaftarKonfirm> listD = new List<DaftarKonfirm>();

        private void Konfrimasi_Load(object sender, EventArgs e)
        {
            labelDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            DaftarKonfirm DK = new DaftarKonfirm();
            int stat = 0;

            string hasil = DK.BacaSemuaData();
            if (hasil == "sukses")
            {
                for (int i = 0; i < DK.JumlahList; i++)
                {
                    comboBox1.Items.Add(DK.ListKonfirm[i].KodeB);
                    stat = DK.ListKonfirm[i].Status;
                    if (stat == 1)
                    {
                        string stats = "Diterima";
                        string kode = DK.ListKonfirm[i].KodeB;
                        string nama = DK.ListKonfirm[i].Nama;
                        string supplier = DK.ListKonfirm[i].Supplier;
                        int harga = DK.ListKonfirm[i].HargaSatuan;
                        int jumlah = DK.ListKonfirm[i].Jumlah;
                        DateTime tglB = DK.ListKonfirm[i].TanggalBeli;
                        int status = DK.ListKonfirm[i].Status;
                        DateTime tglD = DK.ListKonfirm[i].TanggalDatang;
                        dataGridViewInfo.Rows.Add(nama, supplier, harga, jumlah, tglB, stats, tglD);
                    }
                    else if (stat == 0)
                    {
                        string stats1 = "Proses";
                        string kode = DK.ListKonfirm[i].KodeB;
                        string nama = DK.ListKonfirm[i].Nama;
                        string supplier = DK.ListKonfirm[i].Supplier;
                        int harga = DK.ListKonfirm[i].HargaSatuan;
                        int jumlah = DK.ListKonfirm[i].Jumlah;
                        DateTime tglB = DK.ListKonfirm[i].TanggalBeli;
                        int status = DK.ListKonfirm[i].Status;
                        DateTime tglD = DK.ListKonfirm[i].TanggalDatang;
                        dataGridViewInfo.Rows.Add(nama, supplier, harga, jumlah, tglB, stats1, tglD);
                    }
                }
            }
            else
            {
                MessageBox.Show("Error : " + hasil);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            DaftarKonfirm DK = new DaftarKonfirm();
            string hasilB = DK.BacaSemuaData();

            foreach (DataGridViewRow roww in dataGridViewInfo.Rows)
            {
                DataGridViewCheckBoxCell chk = roww.Cells[7] as DataGridViewCheckBoxCell;
                if (Convert.ToBoolean(chk.Value) == true)
                {
                    for (int i = 0; i < dataGridViewInfo.Rows.Count; i++)
                    {
                        if(hasilB == "sukses")
                        {
                            if (dataGridViewInfo.Rows[i].Cells["Check"].Value != null)
                            {
                                string kodeP = DK.ListKonfirm[i].KodeB;
                                int stat = DK.ListKonfirm[i].Status;
                                string date2;
                                DateTime date = DateTime.Now;
                                date2 = date.ToString();
                                string hasil = DK.Update(kodeP, date);
                                if (hasil == "sukses")
                                {
                                    if(stat == 0)
                                    {
                                        DK.BacaSemuaData();
                                        MessageBox.Show("Data Terupdate");
                                    }
                                    else if(stat == 1)
                                    {
                                        MessageBox.Show("Barang Telah Diterima");
                                    }
                                }
                                else
                                {
                                    MessageBox.Show(hasil);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DaftarKonfirm DK = new DaftarKonfirm();
            string noSp = comboBox1.SelectedItem.ToString();
            string hasil = DK.CariData(noSp);

            if (hasil == "sukses")
            {
                dataGridViewInfo.Rows.Clear();
                int stat = DK.ListKonfirm[0].Status;
                if (stat == 1)
                {
                    string stats = "Diterima";
                    string kode = DK.ListKonfirm[0].KodeB;
                    string nama = DK.ListKonfirm[0].Nama;
                    string supplier = DK.ListKonfirm[0].Supplier;
                    int harga = DK.ListKonfirm[0].HargaSatuan;
                    int jumlah = DK.ListKonfirm[0].Jumlah;
                    DateTime tglB = DK.ListKonfirm[0].TanggalBeli;
                    int status = DK.ListKonfirm[0].Status;
                    DateTime tglD = DK.ListKonfirm[0].TanggalDatang;
                    dataGridViewInfo.Rows.Add(nama, supplier, harga, jumlah, tglB, stats, tglD);
                }
                else if (stat == 0)
                {
                    string stats1 = "Proses";
                    string kode = DK.ListKonfirm[0].KodeB;
                    string nama = DK.ListKonfirm[0].Nama;
                    string supplier = DK.ListKonfirm[0].Supplier;
                    int harga = DK.ListKonfirm[0].HargaSatuan;
                    int jumlah = DK.ListKonfirm[0].Jumlah;
                    DateTime tglB = DK.ListKonfirm[0].TanggalBeli;
                    int status = DK.ListKonfirm[0].Status;
                    DateTime tglD = DK.ListKonfirm[0].TanggalDatang;
                    dataGridViewInfo.Rows.Add(nama, supplier, harga, jumlah, tglB, stats1, tglD);

                }
            }

            else
            {
                MessageBox.Show(hasil);
            }

        }
    }
}
