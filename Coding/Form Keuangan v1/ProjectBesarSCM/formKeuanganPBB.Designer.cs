﻿namespace ProjectBesarSCM
{
    partial class formKeuanganPBB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelKodePembelian = new System.Windows.Forms.Label();
            this.labelInfoKodePembelian = new System.Windows.Forms.Label();
            this.labelInfoBahanBaku = new System.Windows.Forms.Label();
            this.comboBoxBahanBaku = new System.Windows.Forms.ComboBox();
            this.comboBoxSupplier = new System.Windows.Forms.ComboBox();
            this.labelSupplier = new System.Windows.Forms.Label();
            this.labelJumlah = new System.Windows.Forms.Label();
            this.textBoxJumlah = new System.Windows.Forms.TextBox();
            this.labelHarga = new System.Windows.Forms.Label();
            this.labelHargaSatuan = new System.Windows.Forms.Label();
            this.labelInfoTotalHarga = new System.Windows.Forms.Label();
            this.labelTotalHarga = new System.Windows.Forms.Label();
            this.dataGridViewInfo = new System.Windows.Forms.DataGridView();
            this.namaBahan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HargaSatuan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Jumlah = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TanggalDatang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelInfoTotal = new System.Windows.Forms.Label();
            this.labelTotal = new System.Windows.Forms.Label();
            this.labelRp = new System.Windows.Forms.Label();
            this.labelRp2 = new System.Windows.Forms.Label();
            this.labelJam = new System.Windows.Forms.Label();
            this.labelWaktu = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelHello = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // labelKodePembelian
            // 
            this.labelKodePembelian.AutoSize = true;
            this.labelKodePembelian.Location = new System.Drawing.Point(61, 85);
            this.labelKodePembelian.Name = "labelKodePembelian";
            this.labelKodePembelian.Size = new System.Drawing.Size(98, 13);
            this.labelKodePembelian.TabIndex = 0;
            this.labelKodePembelian.Text = "KODE Pembelian : ";
            // 
            // labelInfoKodePembelian
            // 
            this.labelInfoKodePembelian.AutoSize = true;
            this.labelInfoKodePembelian.Location = new System.Drawing.Point(165, 85);
            this.labelInfoKodePembelian.Name = "labelInfoKodePembelian";
            this.labelInfoKodePembelian.Size = new System.Drawing.Size(48, 13);
            this.labelInfoKodePembelian.TabIndex = 1;
            this.labelInfoKodePembelian.Text = "Ini KOde";
            // 
            // labelInfoBahanBaku
            // 
            this.labelInfoBahanBaku.AutoSize = true;
            this.labelInfoBahanBaku.Location = new System.Drawing.Point(61, 109);
            this.labelInfoBahanBaku.Name = "labelInfoBahanBaku";
            this.labelInfoBahanBaku.Size = new System.Drawing.Size(140, 13);
            this.labelInfoBahanBaku.TabIndex = 2;
            this.labelInfoBahanBaku.Text = "Kode - Nama Bahan Baku : ";
            // 
            // comboBoxBahanBaku
            // 
            this.comboBoxBahanBaku.FormattingEnabled = true;
            this.comboBoxBahanBaku.Location = new System.Drawing.Point(207, 106);
            this.comboBoxBahanBaku.Name = "comboBoxBahanBaku";
            this.comboBoxBahanBaku.Size = new System.Drawing.Size(200, 21);
            this.comboBoxBahanBaku.TabIndex = 3;
            // 
            // comboBoxSupplier
            // 
            this.comboBoxSupplier.FormattingEnabled = true;
            this.comboBoxSupplier.Location = new System.Drawing.Point(168, 136);
            this.comboBoxSupplier.Name = "comboBoxSupplier";
            this.comboBoxSupplier.Size = new System.Drawing.Size(196, 21);
            this.comboBoxSupplier.TabIndex = 5;
            // 
            // labelSupplier
            // 
            this.labelSupplier.AutoSize = true;
            this.labelSupplier.Location = new System.Drawing.Point(111, 139);
            this.labelSupplier.Name = "labelSupplier";
            this.labelSupplier.Size = new System.Drawing.Size(54, 13);
            this.labelSupplier.TabIndex = 4;
            this.labelSupplier.Text = "Supplier : ";
            // 
            // labelJumlah
            // 
            this.labelJumlah.AutoSize = true;
            this.labelJumlah.Location = new System.Drawing.Point(90, 166);
            this.labelJumlah.Name = "labelJumlah";
            this.labelJumlah.Size = new System.Drawing.Size(69, 13);
            this.labelJumlah.TabIndex = 6;
            this.labelJumlah.Text = "Jumlah Beli : ";
            // 
            // textBoxJumlah
            // 
            this.textBoxJumlah.Location = new System.Drawing.Point(168, 163);
            this.textBoxJumlah.Name = "textBoxJumlah";
            this.textBoxJumlah.Size = new System.Drawing.Size(116, 20);
            this.textBoxJumlah.TabIndex = 7;
            // 
            // labelHarga
            // 
            this.labelHarga.AutoSize = true;
            this.labelHarga.Location = new System.Drawing.Point(503, 139);
            this.labelHarga.Name = "labelHarga";
            this.labelHarga.Size = new System.Drawing.Size(13, 13);
            this.labelHarga.TabIndex = 9;
            this.labelHarga.Text = "0";
            // 
            // labelHargaSatuan
            // 
            this.labelHargaSatuan.AutoSize = true;
            this.labelHargaSatuan.Location = new System.Drawing.Point(385, 139);
            this.labelHargaSatuan.Name = "labelHargaSatuan";
            this.labelHargaSatuan.Size = new System.Drawing.Size(82, 13);
            this.labelHargaSatuan.TabIndex = 8;
            this.labelHargaSatuan.Text = "Harga Satuan : ";
            // 
            // labelInfoTotalHarga
            // 
            this.labelInfoTotalHarga.AutoSize = true;
            this.labelInfoTotalHarga.Location = new System.Drawing.Point(195, 190);
            this.labelInfoTotalHarga.Name = "labelInfoTotalHarga";
            this.labelInfoTotalHarga.Size = new System.Drawing.Size(13, 13);
            this.labelInfoTotalHarga.TabIndex = 11;
            this.labelInfoTotalHarga.Text = "0";
            // 
            // labelTotalHarga
            // 
            this.labelTotalHarga.AutoSize = true;
            this.labelTotalHarga.Location = new System.Drawing.Point(90, 190);
            this.labelTotalHarga.Name = "labelTotalHarga";
            this.labelTotalHarga.Size = new System.Drawing.Size(72, 13);
            this.labelTotalHarga.TabIndex = 10;
            this.labelTotalHarga.Text = "Total Harga : ";
            // 
            // dataGridViewInfo
            // 
            this.dataGridViewInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.namaBahan,
            this.Supplier,
            this.HargaSatuan,
            this.Jumlah,
            this.Total,
            this.Status,
            this.TanggalDatang});
            this.dataGridViewInfo.Location = new System.Drawing.Point(47, 256);
            this.dataGridViewInfo.Name = "dataGridViewInfo";
            this.dataGridViewInfo.Size = new System.Drawing.Size(563, 89);
            this.dataGridViewInfo.TabIndex = 12;
            // 
            // namaBahan
            // 
            this.namaBahan.HeaderText = "Nama Bahan";
            this.namaBahan.Name = "namaBahan";
            this.namaBahan.Width = 75;
            // 
            // Supplier
            // 
            this.Supplier.HeaderText = "Supplier";
            this.Supplier.Name = "Supplier";
            this.Supplier.Width = 75;
            // 
            // HargaSatuan
            // 
            this.HargaSatuan.HeaderText = "Harga Satuan";
            this.HargaSatuan.Name = "HargaSatuan";
            this.HargaSatuan.Width = 75;
            // 
            // Jumlah
            // 
            this.Jumlah.HeaderText = "Jumlah";
            this.Jumlah.Name = "Jumlah";
            this.Jumlah.Width = 75;
            // 
            // Total
            // 
            this.Total.HeaderText = "Total";
            this.Total.Name = "Total";
            this.Total.Width = 75;
            // 
            // Status
            // 
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.Width = 75;
            // 
            // TanggalDatang
            // 
            this.TanggalDatang.HeaderText = "Tanggal Datang";
            this.TanggalDatang.Name = "TanggalDatang";
            this.TanggalDatang.Width = 75;
            // 
            // labelInfoTotal
            // 
            this.labelInfoTotal.ForeColor = System.Drawing.Color.Red;
            this.labelInfoTotal.Location = new System.Drawing.Point(562, 358);
            this.labelInfoTotal.Name = "labelInfoTotal";
            this.labelInfoTotal.Size = new System.Drawing.Size(35, 13);
            this.labelInfoTotal.TabIndex = 14;
            this.labelInfoTotal.Text = "0";
            // 
            // labelTotal
            // 
            this.labelTotal.ForeColor = System.Drawing.Color.Red;
            this.labelTotal.Location = new System.Drawing.Point(502, 358);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(35, 13);
            this.labelTotal.TabIndex = 13;
            this.labelTotal.Text = "Total:";
            // 
            // labelRp
            // 
            this.labelRp.AutoSize = true;
            this.labelRp.Location = new System.Drawing.Point(473, 139);
            this.labelRp.Name = "labelRp";
            this.labelRp.Size = new System.Drawing.Size(24, 13);
            this.labelRp.TabIndex = 15;
            this.labelRp.Text = "Rp.";
            // 
            // labelRp2
            // 
            this.labelRp2.AutoSize = true;
            this.labelRp2.Location = new System.Drawing.Point(165, 190);
            this.labelRp2.Name = "labelRp2";
            this.labelRp2.Size = new System.Drawing.Size(24, 13);
            this.labelRp2.TabIndex = 16;
            this.labelRp2.Text = "Rp.";
            // 
            // labelJam
            // 
            this.labelJam.AutoSize = true;
            this.labelJam.Location = new System.Drawing.Point(1, 396);
            this.labelJam.Name = "labelJam";
            this.labelJam.Size = new System.Drawing.Size(26, 13);
            this.labelJam.TabIndex = 18;
            this.labelJam.Text = "Jam";
            // 
            // labelWaktu
            // 
            this.labelWaktu.AutoSize = true;
            this.labelWaktu.Location = new System.Drawing.Point(1, 411);
            this.labelWaktu.Name = "labelWaktu";
            this.labelWaktu.Size = new System.Drawing.Size(39, 13);
            this.labelWaktu.TabIndex = 17;
            this.labelWaktu.Text = "Waktu";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(531, 358);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Rp.";
            // 
            // labelHello
            // 
            this.labelHello.AutoSize = true;
            this.labelHello.Location = new System.Drawing.Point(466, 13);
            this.labelHello.Name = "labelHello";
            this.labelHello.Size = new System.Drawing.Size(43, 13);
            this.labelHello.TabIndex = 20;
            this.labelHello.Text = "Hello  , ";
            // 
            // formKeuanganPBB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ProjectBesarSCM.Properties.Resources.Keuangan_PBB;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(667, 433);
            this.Controls.Add(this.labelHello);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelJam);
            this.Controls.Add(this.labelWaktu);
            this.Controls.Add(this.labelRp2);
            this.Controls.Add(this.labelRp);
            this.Controls.Add(this.labelInfoTotal);
            this.Controls.Add(this.labelTotal);
            this.Controls.Add(this.dataGridViewInfo);
            this.Controls.Add(this.labelInfoTotalHarga);
            this.Controls.Add(this.labelTotalHarga);
            this.Controls.Add(this.labelHarga);
            this.Controls.Add(this.labelHargaSatuan);
            this.Controls.Add(this.textBoxJumlah);
            this.Controls.Add(this.labelJumlah);
            this.Controls.Add(this.comboBoxSupplier);
            this.Controls.Add(this.labelSupplier);
            this.Controls.Add(this.comboBoxBahanBaku);
            this.Controls.Add(this.labelInfoBahanBaku);
            this.Controls.Add(this.labelInfoKodePembelian);
            this.Controls.Add(this.labelKodePembelian);
            this.Name = "formKeuanganPBB";
            this.Text = "formKeuanganPBB";
            this.Load += new System.EventHandler(this.formKeuanganPBB_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewInfo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelKodePembelian;
        private System.Windows.Forms.Label labelInfoKodePembelian;
        private System.Windows.Forms.Label labelInfoBahanBaku;
        private System.Windows.Forms.ComboBox comboBoxBahanBaku;
        private System.Windows.Forms.ComboBox comboBoxSupplier;
        private System.Windows.Forms.Label labelSupplier;
        private System.Windows.Forms.Label labelJumlah;
        private System.Windows.Forms.TextBox textBoxJumlah;
        private System.Windows.Forms.Label labelHarga;
        private System.Windows.Forms.Label labelHargaSatuan;
        private System.Windows.Forms.Label labelInfoTotalHarga;
        private System.Windows.Forms.Label labelTotalHarga;
        private System.Windows.Forms.DataGridView dataGridViewInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn namaBahan;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier;
        private System.Windows.Forms.DataGridViewTextBoxColumn HargaSatuan;
        private System.Windows.Forms.DataGridViewTextBoxColumn Jumlah;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn TanggalDatang;
        private System.Windows.Forms.Label labelInfoTotal;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label labelRp;
        private System.Windows.Forms.Label labelRp2;
        private System.Windows.Forms.Label labelJam;
        private System.Windows.Forms.Label labelWaktu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelHello;
    }
}