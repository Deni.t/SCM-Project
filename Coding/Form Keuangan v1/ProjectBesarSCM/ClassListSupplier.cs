﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.IO;
using System.Drawing;
using System.Drawing.Printing;

namespace ProjectBesarSCM
{
    class ClassListSupplier
    {
        private List<ClassKeuanganSupp> listSupp;



        #region properties
        public List<ClassKeuanganSupp> ListSupp
        {
            get { return listSupp; }
            set { listSupp = value; }
        }

        public int JumlahSupp
        {
            get { return listSupp.Count; }
        }
        #endregion

        #region Constructor
        public ClassListSupplier()
        {
            listSupp = new List<ClassKeuanganSupp>();
        }
        #endregion

        #region Method
        public string BacaSemuaData()
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "select * from supplier";
            MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
            try
            {
                MySqlDataReader data = MSC.ExecuteReader();
                while (data.Read() == true)
                {
                    string kodeSupplier = data.GetValue(0).ToString();
                    string namaSupplier = data.GetValue(1).ToString();
                    string alamat = data.GetValue(2).ToString();
                    ClassKeuanganSupp supp = new ClassKeuanganSupp(kodeSupplier, namaSupplier, alamat);
                    listSupp.Add(supp);
                }
                MSC.Dispose();
                data.Dispose();
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string TambahSupplier(ClassKeuanganSupp supp)
        {
            Koneksi k = new Koneksi();
            k.Connect();

            string sql = "INSERT INTO supplier(kodeSupplier, namaSupplier, alamat) VALUES ('" + supp.Id + "','" + supp.Nama + "','" + supp.Alamat + "')";

            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB);

            try
            {
                c.ExecuteNonQuery();
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string CariData(string id)
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "select * from supplier" + " WHERE kodeSupplier LIKE '%" + id + "%'";
            MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
            try
            {
                MySqlDataReader data = MSC.ExecuteReader();
                while (data.Read() == true)
                {
                    string kodeSupplier = data.GetValue(0).ToString();
                    string namaSupplier = data.GetValue(1).ToString();
                    string alamat = data.GetValue(2).ToString();
                    ClassKeuanganSupp supp = new ClassKeuanganSupp(kodeSupplier, namaSupplier, alamat);
                    listSupp.Add(supp);
                }
                MSC.Dispose();
                data.Dispose();
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string HapusSupplier(ClassKeuanganSupp supp)
        {
            Koneksi k = new Koneksi();
            k.Connect();

            string sql = "Delete From supplier WHERE kodeSupplier = '" + supp.Id + "'";
            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB);

            try
            {
                c.ExecuteNonQuery();
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        #endregion
    }
}
