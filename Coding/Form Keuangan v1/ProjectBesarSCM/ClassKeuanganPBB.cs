﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.IO;
using System.Drawing;
using System.Drawing.Printing;
namespace ProjectBesarSCM
{
    public class ClassKeuanganPBB
    {
        private string kodePembelian;
        private DateTime tanggal;
        private string supplier;
      

        public string KodePembelian
        {
            get { return kodePembelian; }
            set { kodePembelian = value; }
        }
     

        public DateTime Tanggal
        {
            get { return tanggal; }
            set { tanggal = value; }
        }
       

        public string Supplier
        {
            get { return supplier; }
            set { supplier = value; }
        }
        public ClassKeuanganPBB()
        {
            kodePembelian = "pembelian02";
            tanggal = new DateTime();
            supplier = "";
        }
        public ClassKeuanganPBB(string Kode, DateTime Date, string sup)
        {
            kodePembelian = Kode;
            tanggal = Date;
            supplier = sup;
        }
        
    }
}
