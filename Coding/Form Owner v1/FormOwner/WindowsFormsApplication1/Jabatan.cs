﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    public class Jabatan
    {
        private string kodeJabatan;
        private string namaJabatan;

        #region Properties
        public string KodeJabatan
        {
            get { return kodeJabatan; }
            set { kodeJabatan = value; }
        }
        public string NamaJabatan
        {
            get { return namaJabatan; }
            set { namaJabatan = value; }
        }
        #endregion
        #region Constructor
        public Jabatan()
        {
            kodeJabatan = "";
            namaJabatan = "";
        }
        public Jabatan(string kode, string nama)
        {
            kodeJabatan = kode;
            namaJabatan = nama;
        }
        #endregion
    }
}
