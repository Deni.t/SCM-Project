﻿namespace WindowsFormsApplication1
{
    partial class FormHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormHome));
            this.buttonVerifikasi = new System.Windows.Forms.Button();
            this.buttonGudang = new System.Windows.Forms.Button();
            this.buttonKeuangan = new System.Windows.Forms.Button();
            this.buttonAdmin = new System.Windows.Forms.Button();
            this.listBoxInfo = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonVerifikasi
            // 
            this.buttonVerifikasi.BackColor = System.Drawing.Color.Transparent;
            this.buttonVerifikasi.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonVerifikasi.Location = new System.Drawing.Point(72, 85);
            this.buttonVerifikasi.Name = "buttonVerifikasi";
            this.buttonVerifikasi.Size = new System.Drawing.Size(169, 88);
            this.buttonVerifikasi.TabIndex = 0;
            this.buttonVerifikasi.Text = "VERIFIKASI \r\nSPK";
            this.buttonVerifikasi.UseVisualStyleBackColor = false;
            this.buttonVerifikasi.Click += new System.EventHandler(this.buttonVerifikasi_Click);
            this.buttonVerifikasi.MouseLeave += new System.EventHandler(this.button1_MouseLeave);
            this.buttonVerifikasi.MouseHover += new System.EventHandler(this.button1_MouseHover);
            // 
            // buttonGudang
            // 
            this.buttonGudang.BackColor = System.Drawing.Color.Transparent;
            this.buttonGudang.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGudang.Location = new System.Drawing.Point(256, 85);
            this.buttonGudang.Name = "buttonGudang";
            this.buttonGudang.Size = new System.Drawing.Size(166, 88);
            this.buttonGudang.TabIndex = 1;
            this.buttonGudang.Text = "CEK \r\nGUDANG";
            this.buttonGudang.UseVisualStyleBackColor = false;
            this.buttonGudang.Click += new System.EventHandler(this.buttonGudang_Click);
            // 
            // buttonKeuangan
            // 
            this.buttonKeuangan.BackColor = System.Drawing.Color.Transparent;
            this.buttonKeuangan.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonKeuangan.Location = new System.Drawing.Point(441, 86);
            this.buttonKeuangan.Name = "buttonKeuangan";
            this.buttonKeuangan.Size = new System.Drawing.Size(166, 88);
            this.buttonKeuangan.TabIndex = 2;
            this.buttonKeuangan.Text = "LAPORAN\r\nKEUANGAN";
            this.buttonKeuangan.UseVisualStyleBackColor = false;
            // 
            // buttonAdmin
            // 
            this.buttonAdmin.BackColor = System.Drawing.Color.Transparent;
            this.buttonAdmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdmin.Location = new System.Drawing.Point(628, 86);
            this.buttonAdmin.Name = "buttonAdmin";
            this.buttonAdmin.Size = new System.Drawing.Size(166, 88);
            this.buttonAdmin.TabIndex = 3;
            this.buttonAdmin.Text = "CEK \r\nADMIN";
            this.buttonAdmin.UseVisualStyleBackColor = false;
            // 
            // listBoxInfo
            // 
            this.listBoxInfo.FormattingEnabled = true;
            this.listBoxInfo.Location = new System.Drawing.Point(167, 204);
            this.listBoxInfo.Name = "listBoxInfo";
            this.listBoxInfo.Size = new System.Drawing.Size(512, 134);
            this.listBoxInfo.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 356);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 369);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "label2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(608, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 20);
            this.label3.TabIndex = 8;
            this.label3.Text = "label3";
            // 
            // FormHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(866, 391);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBoxInfo);
            this.Controls.Add(this.buttonAdmin);
            this.Controls.Add(this.buttonKeuangan);
            this.Controls.Add(this.buttonGudang);
            this.Controls.Add(this.buttonVerifikasi);
            this.Name = "FormHome";
            this.Text = "Owner";
            this.Load += new System.EventHandler(this.FormHome_Load_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonVerifikasi;
        private System.Windows.Forms.Button buttonGudang;
        private System.Windows.Forms.Button buttonKeuangan;
        private System.Windows.Forms.Button buttonAdmin;
        private System.Windows.Forms.ListBox listBoxInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label3;

    }
}

