﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1;

namespace WindowsFormsApplication1
{
    public partial class FormHome : Form
    {
        public FormHome()
        {
            InitializeComponent();
        }

        private void button1_MouseHover(object sender, EventArgs e)
        {
            buttonVerifikasi.BackColor = Color.WhiteSmoke;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            buttonVerifikasi.BackColor = Color.Transparent;
        }

        private void buttonVerifikasi_Click(object sender, EventArgs e)
        {
            FormVerifikasi FV = new FormVerifikasi();
            FV.Owner = this;
            FV.Show();

        }

        private void buttonGudang_Click(object sender, EventArgs e)
        {

        }

        private void FormHome_Load_1(object sender, EventArgs e)
        {
            label1.Text = DateTime.Now.ToString("HH:MM");
            label2.Text = DateTime.Now.ToString("dd/MM/yyyy");

            this.IsMdiContainer = true;
            this.Enabled = true;
            //Login frmLogin = new Login();
            //frmLogin.Owner = this;
            //frmLogin.ShowDialog();
        }

    }
}
