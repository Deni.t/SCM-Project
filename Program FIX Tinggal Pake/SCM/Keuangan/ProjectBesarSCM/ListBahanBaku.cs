﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace ProjectBesarSCM
{
    public class ListBahanBaku
    {
        private List<BahanBaku> listBahanBaku;
        private string kodeTerbaru;

        #region Properties
        public List<BahanBaku> LBahanBaku
        {
            get { return listBahanBaku; }
        }

        public int JumlahBarang
        {
            get { return listBahanBaku.Count; }
        }
        public string KodeTerbaru
        {
            get { return kodeTerbaru; }
        }
        #endregion

        #region Constructor
        public ListBahanBaku()
        {
            listBahanBaku = new List<BahanBaku>();
            kodeTerbaru = "B0001";
        }
        #endregion

        #region Method
        public string BacaSemuaData()
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "SELECT * from bahanbaku ORDER BY kodeBahanBaku ASC";
            MySqlCommand MSC = new MySqlCommand(sql,K.KoneksiDB);
            try
            {
                MySqlDataReader Data = MSC.ExecuteReader();
                while(Data.Read() == true)
                {
                    string kode = Data.GetValue(0).ToString();
                    string nama = Data.GetValue(1).ToString();
                    string satuan = Data.GetValue(2).ToString();
                    int stok = int.Parse(Data.GetValue(3).ToString());
                   
                
                    BahanBaku brg = new BahanBaku(kode, nama, satuan, stok);
                    listBahanBaku.Add(brg);
                }
                MSC.Dispose();
                Data.Dispose();
                return "sukses";
            }
            catch(Exception E)
            {
                return E.Message;
            }
        }
        public string CariData(string kriteria, string nilaiKriteria)
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "SELECT * from bahanbaku ORDER BY kodeBahanBaku ASC" + " WHERE " + kriteria + " LIKE '%" + nilaiKriteria + "%'";
            MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
            try
            {
                MySqlDataReader Data = MSC.ExecuteReader();
                while (Data.Read() == true)
                {
                    string kode = Data.GetValue(0).ToString();
                    string nama = Data.GetValue(1).ToString();
                    string satuan = Data.GetValue(2).ToString();
                    int stok = int.Parse(Data.GetValue(3).ToString());


                    BahanBaku brg = new BahanBaku(kode, nama, satuan, stok);
                    listBahanBaku.Add(brg);
                }
                MSC.Dispose();
                Data.Dispose();
                return "sukses";
            }
            catch (Exception E)
            {
                return E.Message;
            }
        }
        public string GenerateKode()
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "SELECT SUBSTRING(KodeBarang,2,4) FROM Barang ORDER BY KodeBarang DESC LIMIT 1";
            MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
            try
            {
                MySqlDataReader Data = MSC.ExecuteReader();
                if(Data.Read() == true)
                {
                    int kdTerbaru = int.Parse(Data.GetValue(0).ToString()) + 1;
                    kodeTerbaru = kdTerbaru.ToString();
                    if(kodeTerbaru.Length == 1)
                    {
                        kodeTerbaru = "B000" + kodeTerbaru;
                    }
                    else if (kodeTerbaru.Length == 2)
                    {
                        kodeTerbaru = "B00" + kodeTerbaru;
                    }
                    else if (kodeTerbaru.Length == 3)
                    {
                        kodeTerbaru = "B0" + kodeTerbaru;
                    }
                    else if (kodeTerbaru.Length == 4)
                    {
                        kodeTerbaru = "B" + kodeTerbaru;
                    }
                }
                Data.Dispose();
                MSC.Dispose();
                return "sukses";
            }
            catch(Exception E)
            {
                return E.Message;
            }
        }
        public string TambahData(BahanBaku brg)
        {
            Koneksi k = new Koneksi();
            k.Connect();

            string sql = "INSERT INTO Barang(KodeBarang, Nama, satuan, Stok) VALUES ('" + brg.KodeBarang + "','" + brg.NamaBarang+ "','"+ brg.Satuan+"','"+brg.Stok+ "')";

            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB);

            try
            {
                c.ExecuteNonQuery();
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        public string UbahData(BahanBaku brg)
        {
            Koneksi k = new Koneksi();
            k.Connect();

            string sql = "UPDATE Barang Set Nama = '" + brg.NamaBarang + "', Satuan = '" + brg.Satuan + "', Stok = '" + brg.Stok+"' WHERE KodeBahanBaku = '"+ brg.KodeBarang + "'";
            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB);

            try
            {
                c.ExecuteNonQuery();
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        public string HapusData(BahanBaku brg)
        {
            Koneksi k = new Koneksi();
            k.Connect();

            string sql = "Delete From BahanBaku WHERE KodeBahanBaku = '" + brg.KodeBarang + "'";
            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB);

            try
            {
                c.ExecuteNonQuery();
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        #endregion
    }
}
