﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace ProjectBesarSCM
{
    public partial class FormKeuanganSuppUbahHapus : Form
    {
        Koneksi k;
        public FormKeuanganSuppUbahHapus()
        {
            InitializeComponent();
            loadComboBox();
            loadComboBox2();
        }
        public void loadComboBox()
        {
            k = new Koneksi();
            k.Connect();

            MySqlCommand cmd = new MySqlCommand("SELECT * FROM supplier", k.KoneksiDB);
            try
            {
                MySqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    cmbSupplier.Items.Add(rdr["kodeSupplier"] + " - " + rdr["namaSupplier"]);
                }
                //MessageBox.Show("Koneksi berhasil");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void loadComboBox2()
        {
            k = new Koneksi();
            k.Connect();

            MySqlCommand cmd = new MySqlCommand("SELECT * FROM bahanbaku", k.KoneksiDB);
            try
            {
                MySqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    cmbBahanBaku.Items.Add(rdr["kodeBahanBaku"] + " - " + rdr["nama"]);
                }
                //MessageBox.Show("Koneksi berhasil");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void cmbBahanBaku_SelectedIndexChanged(object sender, EventArgs e)
        {
            string supplier = cmbSupplier.Text;
            string bahanBaku = cmbBahanBaku.Text;

            k = new Koneksi();
            k.Connect();

            MySqlCommand cmd = new MySqlCommand("SELECT hargaSatuan FROM detilbahanbakusupplier WHERE kodeBahanBaku = '" + bahanBaku.ToString().Substring(0, 5) + "' AND kodeSupplier = '" + supplier.ToString().Substring(0, 2) + "'", k.KoneksiDB);
            try
            {
                MySqlDataReader rdr = cmd.ExecuteReader();
                string cek = "";
                while (rdr.Read())
                {
                    cek = rdr["hargaSatuan"].ToString();
                }
                if (cek != null)
                {
                    labelHarga.Text = rdr["hargaSatuan"].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Belum ada data untuk kode bahan baku = " + bahanBaku.ToString().Substring(0, 5) + " dan kode supplier = " + supplier.ToString().Substring(0, 2));
            }
        }
        private void btnHapus_Click(object sender, EventArgs e)
        {
            string supplier = cmbSupplier.Text;
            string bahanBaku = cmbBahanBaku.Text;

            k = new Koneksi();
            k.Connect();
            MySqlCommand cmd = new MySqlCommand("delete from detilbahanbakusupplier WHERE kodeBahanBaku = '" + bahanBaku.ToString().Substring(0, 5) + "' AND kodeSupplier = '" + supplier.ToString().Substring(0, 2) + "'", k.KoneksiDB);
            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Data berhasil dihapus");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void FormKeuanganSuppUbahHapus_Load(object sender, EventArgs e)
        {
            string Jam = DateTime.Now.ToString("dd-MM-yyyy");
            string Waktu = DateTime.Now.ToString("HH:mm");
            labelWaktu.Text = Waktu;
            labelJam.Text = Jam;
        }

        private void btnKembali_Click(object sender, EventArgs e)
        {
            FormKeuanganSupp formSP = new FormKeuanganSupp();
            formSP.Show(this);
            this.Hide();
        }

        

        
    }
}
