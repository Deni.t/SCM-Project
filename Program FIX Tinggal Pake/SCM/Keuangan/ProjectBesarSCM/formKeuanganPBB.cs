﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1;

namespace ProjectBesarSCM
{
    public partial class formKeuanganPBB : Form
    {
        public formKeuanganPBB()
        {
            InitializeComponent();
        }

        private void formKeuanganPBB_Load(object sender, EventArgs e)
        {
            //FormHome FH = (FormHome)this.Owner;
            //FormKeuanganHome FKH = (FormKeuanganHome)this.Owner;
            //labelHello.Text = FH.Nama;
            string Jam = DateTime.Now.ToString("dd-MM-yyyy");
            string Waktu = DateTime.Now.ToString("HH:mm");
            labelWaktu.Text = Waktu;
            labelJam.Text = Jam;

            ListBahanBaku LB = new ListBahanBaku();
            string hasil = LB.BacaSemuaData();
            if(hasil == "sukses")
            {
                comboBoxBahanBaku.Items.Clear();
                for(int i = 0 ; i < LB.JumlahBarang ; i++)
                {
                    comboBoxBahanBaku.Items.Add(LB.LBahanBaku[i].KodeBarang + " - " +LB.LBahanBaku[i].NamaBarang);
                }
                comboBoxBahanBaku.SelectedIndex = 0;
            }
            ListSupplier LS = new ListSupplier();
            string hasil1 = LS.BacaSemuaData();
            if(hasil1 == "sukses")
            {
                comboBoxSupplier.Items.Clear();
                for (int i = 0; i < LS.JumlahList; i++)
                {
                    comboBoxSupplier.Items.Add(LS.LSupplier[i].KodeSup + " - " + LS.LSupplier[i].NamaSup);
                }
                
            }
            else
            {
                MessageBox.Show(hasil1);
            }
         
        }
    }
}
