﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gudang;

namespace Gudang
{
    public partial class FormDaftarBarang : Form
    {
        public FormDaftarBarang()
        {
            InitializeComponent();
        }

        DaftarBahan db = new DaftarBahan();
        spk spk = new Gudang.spk();
        DaftarKonfirm dk = new DaftarKonfirm();
        string time = DateTime.Now.ToString("HH:MM");
        string date = DateTime.Now.ToString("dd/MM/yyyy");
        int stat = 0;
        
        private void FormDaftarBarang_Load(object sender, EventArgs e)
        {
            this.CenterToScreen(); timer1.Start();
            DaftarUser listUser = new DaftarUser();

            string hasilBaca = listUser.CariData("username", "deni7");
            if (hasilBaca == "sukses")
            {
                labelUser.Text = "Hallo, " + listUser.ListPegawai[0].Nama + " - " + listUser.ListPegawai[0].JabatanUser.NamaJabatan;
            }
            //lblTgl.Text = date;
            //lblTgl2.Text = date;
            //lblTime.Text = time;
            string hasil = db.BacaSemuaData();
            if (hasil == "sukses")
            {
                for (int i = 0; i < db.JumlahBahan; i++)
                {
                    comboBox1.Items.Add(db.ListBahan[i].KodeBahan);
                    string id = db.ListBahan[i].KodeBahan;
                    string nama = db.ListBahan[i].NamaBahan;
                    int jumlah = db.ListBahan[i].Jumlah;
                    string ket = db.ListBahan[i].Keterangan;
                    bool stat = db.ListBahan[i].Status;
                    dataGridViewInfo.Rows.Add(id, nama, jumlah, ket, stat);
                }
            }
            else
            {
                MessageBox.Show(hasil);
            }
            for (int i = 0; i < db.JumlahBahan; i++)
            {
                bool st = db.ListBahan[i].Status;
                if(st == true)
                {
                    labelStat.Text = "Terkirim";
                }
                else
                {
                    labelStat.Text = "Proses";
                }

            }
            


        }

       
        private void button1_Click(object sender, EventArgs e)
        {
            string id = "";
            string nama = "";
            string jumlah = "";
            string ket = "";
            foreach (DataGridViewRow roww in dataGridViewInfo.Rows)
            {
                DataGridViewCheckBoxCell chk = roww.Cells[4] as DataGridViewCheckBoxCell;
                if (Convert.ToBoolean(chk.Value) == true)
                {
                    for (int i = 0; i < dataGridViewInfo.Rows.Count; i++)
                    {
                        if (dataGridViewInfo.Rows[i].Cells["Check"].Value != null)
                        {
                            id = dataGridViewInfo.Rows[i].Cells["ID"].Value.ToString();
                            nama = dataGridViewInfo.Rows[i].Cells["Nama"].Value.ToString();
                            jumlah = dataGridViewInfo.Rows[i].Cells["Jumlah"].Value.ToString();
                            ket = dataGridViewInfo.Rows[i].Cells["Keterangan"].Value.ToString();
                        }
                    }
                }
            }
            string hasil = db.Update(id);
            if (hasil == "sukses")
            {
                MessageBox.Show("update suskes");
            }
      
           

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DaftarBahan db = new DaftarBahan();
            string noSp = comboBox1.SelectedItem.ToString();
            string hasil = db.CariData(noSp);

            if (hasil == "sukses")
            {
                dataGridViewInfo.Rows.Clear();
                string id = db.ListBahan[0].KodeBahan;
                string nama = db.ListBahan[0].NamaBahan;
                int jumlah = db.ListBahan[0].Jumlah;
                string ket = db.ListBahan[0].Keterangan;
                dataGridViewInfo.Rows.Add(id, nama, jumlah, ket);
                string stats = db.ListBahan[0].Keterangan;
                if (stats == null)
                {
                    labelStat.Text = "Proses";
                }
            }


        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            labelDate.Text = DateTime.Now.ToString("hh:mm:ss"); 
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
