﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1;

namespace Gudang
{
    public partial class Laporan : Form
    {
        public Laporan()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            dgvKonfirm.Visible = false;
            dataGridViewInfo.Visible = true;
            DaftarBahan db = new DaftarBahan();
            string hasil = db.BacaSemuaData();
            if (hasil == "sukses")
            {
                dataGridViewInfo.Rows.Clear();
                for (int i = 0; i < db.JumlahBahan; i++)
                {
                    string kode = db.ListBahan[i].KodeBahan;
                    string id = db.ListBahan[i].KodeBahan;
                    string nama = db.ListBahan[i].NamaBahan;
                    int jumlah = db.ListBahan[i].Jumlah;
                    string ket = db.ListBahan[i].Keterangan;
                    dataGridViewInfo.Rows.Add(id, nama, jumlah, ket);
                }
            }
            else
            {
                MessageBox.Show(hasil);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridViewInfo.Visible = false;
            dgvKonfirm.Visible = true;
            DaftarKonfirm dk = new DaftarKonfirm();
            string hasil = dk.BacaSemuaData();
            if (hasil == "sukses")
            {
                dgvKonfirm.Rows.Clear();
                for (int i = 0; i < dk.JumlahList; i++)
                {
                    int stat = dk.ListKonfirm[i].Status;
                    if (stat == 1)
                    {
                        string stats = "Diterima";
                        string kode = dk.ListKonfirm[i].KodeB;
                        string nama = dk.ListKonfirm[i].Nama;
                        string supplier = dk.ListKonfirm[i].Supplier;
                        int harga = dk.ListKonfirm[i].HargaSatuan;
                        int jumlah = dk.ListKonfirm[i].Jumlah;
                        DateTime tglB = dk.ListKonfirm[i].TanggalBeli;
                        int status = dk.ListKonfirm[i].Status;
                        DateTime tglD = dk.ListKonfirm[i].TanggalDatang;
                        dgvKonfirm.Rows.Add(nama, supplier, harga, jumlah, tglB, stats, tglD);
                    }
                    else if (stat == 0)
                    {
                        string stats1 = "Proses";
                        string kode = dk.ListKonfirm[i].KodeB;
                        string nama = dk.ListKonfirm[i].Nama;
                        string supplier = dk.ListKonfirm[i].Supplier;
                        int harga = dk.ListKonfirm[i].HargaSatuan;
                        int jumlah = dk.ListKonfirm[i].Jumlah;
                        DateTime tglB = dk.ListKonfirm[i].TanggalBeli;
                        int status = dk.ListKonfirm[i].Status;
                        DateTime tglD = dk.ListKonfirm[i].TanggalDatang;
                        dgvKonfirm.Rows.Add(nama, supplier, harga, jumlah, tglB, stats1, tglD);
                    }
                }
            }
            else
            {
                MessageBox.Show(hasil);
            }
        }

        private void Laporan_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            timer1.Start();
            DaftarUser listUser = new DaftarUser();
            FormUtama fu = (FormUtama)this.Owner;
            string hasilBaca = listUser.CariData("username", "deni7");
            if (hasilBaca == "sukses")
            {
                labelUser.Text = fu.labelJabat.Text;
            }
             DaftarKonfirm dk = new DaftarKonfirm();
            string hasil = dk.BacaSemuaData();
            if (hasil == "sukses")
            {
                dgvKonfirm.Rows.Clear();
                for (int i = 0; i < dk.JumlahList; i++)
                {
                    int stat = dk.ListKonfirm[i].Status;
                    if (stat == 1)
                    {
                        string stats = "Diterima";
                        string kode = dk.ListKonfirm[i].KodeB;
                        string nama = dk.ListKonfirm[i].Nama;
                        string supplier = dk.ListKonfirm[i].Supplier;
                        int harga = dk.ListKonfirm[i].HargaSatuan;
                        int jumlah = dk.ListKonfirm[i].Jumlah;
                        DateTime tglB = dk.ListKonfirm[i].TanggalBeli;
                        int status = dk.ListKonfirm[i].Status;
                        DateTime tglD = dk.ListKonfirm[i].TanggalDatang;
                        dgvKonfirm.Rows.Add(nama, supplier, harga, jumlah, tglB, stats, tglD);
                    }
                    else if (stat == 0)
                    {
                        string stats1 = "Proses";
                        string kode = dk.ListKonfirm[i].KodeB;
                        string nama = dk.ListKonfirm[i].Nama;
                        string supplier = dk.ListKonfirm[i].Supplier;
                        int harga = dk.ListKonfirm[i].HargaSatuan;
                        int jumlah = dk.ListKonfirm[i].Jumlah;
                        DateTime tglB = dk.ListKonfirm[i].TanggalBeli;
                        int status = dk.ListKonfirm[i].Status;
                        DateTime tglD = dk.ListKonfirm[i].TanggalDatang;
                        dgvKonfirm.Rows.Add(nama, supplier, harga, jumlah, tglB, stats1, tglD);
                    }
                }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            labelDate.Text = DateTime.Now.ToString("hh:mm:ss");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
