﻿namespace WindowsFormsApplication1
{
    partial class FormHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormHome));
            this.buttonGudang = new System.Windows.Forms.Button();
            this.buttonKeuangan = new System.Windows.Forms.Button();
            this.buttonAdmin = new System.Windows.Forms.Button();
            this.listBoxInfo = new System.Windows.Forms.ListBox();
            this.labelDate = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.buttonVerifikasi = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonGudang
            // 
            this.buttonGudang.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonGudang.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonGudang.FlatAppearance.BorderSize = 0;
            this.buttonGudang.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.buttonGudang.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGudang.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGudang.Location = new System.Drawing.Point(256, 85);
            this.buttonGudang.Name = "buttonGudang";
            this.buttonGudang.Size = new System.Drawing.Size(166, 88);
            this.buttonGudang.TabIndex = 1;
            this.buttonGudang.Text = "CEK \r\nGUDANG";
            this.buttonGudang.UseVisualStyleBackColor = false;
            this.buttonGudang.Click += new System.EventHandler(this.buttonGudang_Click);
            // 
            // buttonKeuangan
            // 
            this.buttonKeuangan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonKeuangan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonKeuangan.FlatAppearance.BorderSize = 0;
            this.buttonKeuangan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.buttonKeuangan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonKeuangan.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonKeuangan.Location = new System.Drawing.Point(441, 86);
            this.buttonKeuangan.Name = "buttonKeuangan";
            this.buttonKeuangan.Size = new System.Drawing.Size(166, 88);
            this.buttonKeuangan.TabIndex = 2;
            this.buttonKeuangan.Text = "LAPORAN\r\nKEUANGAN";
            this.buttonKeuangan.UseVisualStyleBackColor = false;
            this.buttonKeuangan.Click += new System.EventHandler(this.buttonKeuangan_Click);
            // 
            // buttonAdmin
            // 
            this.buttonAdmin.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonAdmin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonAdmin.FlatAppearance.BorderSize = 0;
            this.buttonAdmin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.buttonAdmin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdmin.Location = new System.Drawing.Point(628, 86);
            this.buttonAdmin.Name = "buttonAdmin";
            this.buttonAdmin.Size = new System.Drawing.Size(166, 88);
            this.buttonAdmin.TabIndex = 3;
            this.buttonAdmin.Text = "CEK \r\nADMIN";
            this.buttonAdmin.UseVisualStyleBackColor = false;
            this.buttonAdmin.Click += new System.EventHandler(this.buttonAdmin_Click);
            // 
            // listBoxInfo
            // 
            this.listBoxInfo.FormattingEnabled = true;
            this.listBoxInfo.Location = new System.Drawing.Point(167, 204);
            this.listBoxInfo.Name = "listBoxInfo";
            this.listBoxInfo.Size = new System.Drawing.Size(512, 134);
            this.listBoxInfo.TabIndex = 4;
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelDate.Location = new System.Drawing.Point(15, 366);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(35, 13);
            this.labelDate.TabIndex = 6;
            this.labelDate.Text = "label1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(608, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 20);
            this.label3.TabIndex = 8;
            this.label3.Text = "label3";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // buttonVerifikasi
            // 
            this.buttonVerifikasi.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonVerifikasi.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonVerifikasi.FlatAppearance.BorderSize = 0;
            this.buttonVerifikasi.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.buttonVerifikasi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonVerifikasi.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonVerifikasi.Location = new System.Drawing.Point(71, 85);
            this.buttonVerifikasi.Name = "buttonVerifikasi";
            this.buttonVerifikasi.Size = new System.Drawing.Size(169, 88);
            this.buttonVerifikasi.TabIndex = 9;
            this.buttonVerifikasi.Text = "VERIFIKASI \r\nSPK";
            this.buttonVerifikasi.UseVisualStyleBackColor = false;
            this.buttonVerifikasi.Click += new System.EventHandler(this.button1_Click);
            this.buttonVerifikasi.MouseHover += new System.EventHandler(this.button1_MouseHover_1);
            // 
            // FormHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(866, 391);
            this.Controls.Add(this.buttonVerifikasi);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labelDate);
            this.Controls.Add(this.listBoxInfo);
            this.Controls.Add(this.buttonAdmin);
            this.Controls.Add(this.buttonKeuangan);
            this.Controls.Add(this.buttonGudang);
            this.Name = "FormHome";
            this.Text = "Owner";
            this.Load += new System.EventHandler(this.FormHome_Load_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonGudang;
        private System.Windows.Forms.Button buttonKeuangan;
        private System.Windows.Forms.Button buttonAdmin;
        private System.Windows.Forms.ListBox listBoxInfo;
        private System.Windows.Forms.Label labelDate;
        public System.Windows.Forms.Label label3;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button buttonVerifikasi;

    }
}

