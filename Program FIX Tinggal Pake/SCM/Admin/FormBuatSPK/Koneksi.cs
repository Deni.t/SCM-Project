﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace FormBuatSPK
{
    public class Koneksi
    {
        #region Datamember
        private MySqlConnection koneksi;
        private string namaServer;
        private string namaDatabase;
        private string userName;
        private string password;
        #endregion

        #region Properties
        public MySqlConnection KoneksiDB
        {
            get { return koneksi; }
        }
        public string NamaServer
        {
            get { return namaServer; }
            set { namaServer = value; }
        }
        public string NamaDatabase
        {
            get { return namaDatabase; }
            set { namaDatabase = value; }
        }
        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }
        public string Password
        {
            set { password = value; }
        }
        #endregion

        #region Constractor
        public Koneksi()
        {
            koneksi = new MySqlConnection();
            koneksi.ConnectionString = ConfigurationManager.ConnectionStrings["KonfigurasiKoneksi"].ConnectionString;

            string hasilConnect = Connect();
        }
        public Koneksi(string server, string database, string user, string pwd)
        {
            namaServer = server;
            namaDatabase = database;
            userName = user;
            password = pwd;

            string strCon = "server=" + namaServer + "; database=" + namaDatabase + "; userid=" + userName + "; password=" + password;

            koneksi = new MySqlConnection();

            koneksi.ConnectionString = strCon;

            string hasilConnect = Connect();

            if (hasilConnect == "sukses")
            {
                UpdateAppConfig(strCon);
            }
        }
        #endregion

        #region Method
        public string Connect()
        {
            try
            {
                if (koneksi.State == System.Data.ConnectionState.Open)
                {
                    koneksi.Close();
                }
                koneksi.Open();
                return "sukses"; //artinya sukses connect
            }
            catch (Exception e)
            {
                return e.Message; //artinya gagal connect
            }
        }
        public void UpdateAppConfig(string connectionString)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.ConnectionStrings.ConnectionStrings["KonfigurasiKoneksi"].ConnectionString = connectionString;
            config.Save(ConfigurationSaveMode.Modified, true);
            ConfigurationManager.RefreshSection("connectionStrings");

        }
        #endregion
    }
}
