﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace FormBuatSPK
{
    public class DaftarKustomer
    {
        private List<Customer> listCustomer;

        public List<Customer> ListCustomer
        {
            get { return listCustomer; }
        }

        public int JumlahCustomer
        {
            get { return listCustomer.Count; }
        }


        public DaftarKustomer()
        {
            listCustomer = new List<Customer>();
        }

        public string BacaCustomer()
        {
            Koneksi k = new Koneksi();
            k.Connect();

            string sql = "SELECT * FROM customer";
            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB);

            try
            {
                MySqlDataReader data = c.ExecuteReader();

                while (data.Read() == true)
                {
                    string kode = data.GetValue(0).ToString();
                    string nama = data.GetValue(1).ToString();
                    string alamat = data.GetValue(2).ToString();
                    Customer cus = new Customer(kode, nama, alamat);
                    ListCustomer.Add(cus);
                }
                c.Dispose();
                data.Dispose();
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string Cari(string kriteria, string nilaikriteria)
        {
            Koneksi k = new Koneksi();
            k.Connect();

            string sql = "SELECT c.kodeCustomer, c.nama, c.alamat FROM customer c WHERE " + kriteria + " = '" + nilaikriteria + "'";
            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB);

            try
            {
                MySqlDataReader data = c.ExecuteReader();

                while (data.Read() == true)
                {
                    string kode = data.GetValue(0).ToString();
                    string nama = data.GetValue(1).ToString();
                    string alamat = data.GetValue(2).ToString();
                    Customer cus = new Customer(kode, nama, alamat);
                    ListCustomer.Add(cus);
                }
                c.Dispose();
                data.Dispose();
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

    }
}
