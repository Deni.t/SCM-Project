﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace FormBuatSPK
{
    public class DaftarSpk
    {
        private List<SPK> listSpk;

        public List<SPK> ListSpk
        {
            get { return listSpk; }
        }

        public int JumlahSpk
        {
            get { return listSpk.Count; }
        }

        public DaftarSpk()
        {
            listSpk = new List<SPK>();
        }

        public string TambahData(SPK pSPK)
        {
            Koneksi k = new Koneksi();
            k.Connect();

            string sql = "INSERT INTO spk(noSPK, tanggal, pekerjaan, biaya, lama, syarat, status, komentar, kodeCustomer, idUser, kodeProduk, tanggalMulai) VALUES ('" + pSPK.NoSpk + "','" +
                         pSPK.Tgl + "','" + pSPK.Pekerjaan + "','" + pSPK.Biaya + "','" + pSPK.Lama + "','" + pSPK.Syarat + "','" + pSPK.Status + "','" + pSPK.Komentar + "','" + pSPK.Customer.Kode + "','" + pSPK.User.Id + "','" + pSPK.KodeProduk + "','" + pSPK.TglMulai + "')";
            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB);

            try
            {
                c.ExecuteNonQuery();
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}
