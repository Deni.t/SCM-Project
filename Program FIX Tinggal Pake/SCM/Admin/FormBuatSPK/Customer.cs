﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormBuatSPK
{
    public class Customer
    {
        private string kode;
        private string nama;
        private string alamat;

        public string Kode
        {
            get { return kode; }
            set { kode = value; }
        }

        public string Nama
        {
            get { return nama; }
            set { nama = value; }
        }

        public string Alamat
        {
            get { return alamat; }
            set { alamat = value; }
        }

        public Customer()
        {
            kode = "";
            nama = "";
            alamat = "";
        }
        public Customer(string pKode, string pNama, string pAlamat)
        {
            kode = pKode;
            nama = pNama;
            alamat = pAlamat;
        }
    }
}
