﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Admin
{
    public partial class FormHapusSPK : Form
    {
        public FormHapusSPK()
        {
            InitializeComponent();
            timer1.Start();
        }

      
        private void btnHapus_Click(object sender, EventArgs e)
        {
            var loadstring = @"server=localhost;database=scm;userid=root;password=;";
            using (var connection = new MySqlConnection(loadstring))
            {
                connection.Open();
                string combo = cboSPK.Text;
                var queryStatSPK = "Select * from spk where noSPK = '" + combo + "'";
                //MessageBox.Show("bisa");
                using (var command2 = new MySqlCommand(queryStatSPK, connection))
                {
                    using (var reader2 = command2.ExecuteReader())
                    {
                        int status = 0;
                        while (reader2.Read())
                        {
                            status = reader2.GetInt32("status");
                            if (status == 1)
                            {
                                MessageBox.Show("Status sudah diverivikasi, tidak bisa di hapus");
                            }
                            else
                            {
                                using (var connection2 = new MySqlConnection(loadstring))
                                {
                                    connection2.Open();
                                    string hapus = "DELETE FROM `spk` WHERE noSPK = '" + combo + "'";
                                    MySqlCommand sqlcom = new MySqlCommand(hapus, connection2);
                                    sqlcom.ExecuteNonQuery();
                                    MessageBox.Show("delete successful");
                                    cboSPK.Items.Clear();
                                    FormHapusSPK_Load(sender, e);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void FormHapusSPK_Load(object sender, EventArgs e)
        {
            timer1.Start();
            Admin_FormHome AFH = (Admin_FormHome)this.Owner;
            labelUser.Text = AFH.labelUser.Text;
            this.CenterToScreen();
            var loadstring = @"server=localhost;database=scm;userid=root;password=;";
            using (var connection = new MySqlConnection(loadstring))
            {
                connection.Open();

                var queryNoSPK = "SELECT noSPK from spk";
                using (var command1 = new MySqlCommand(queryNoSPK, connection))
                {
                    using (var reader1 = command1.ExecuteReader())
                    {
                        //Iterate through the rows and add it to the combobox's items
                        string a = "";
                        while (reader1.Read())
                        {
                            a = reader1.GetString("noSPK");
                            cboSPK.Items.Add(a);
                            cboSPK.SelectedIndex = 0;
                        }
                    }
                }
            }
        }

        private void btnKembali_Click(object sender, EventArgs e)
        {
            this.Hide();
            Admin_FormSPK spk = new Admin_FormSPK();
            spk.ShowDialog();
        }

        private void cboSPK_SelectedIndexChanged(object sender, EventArgs e)
        {
            var loadstring = @"server=localhost;database=scm;userid=root;password=;";
            using (var connection = new MySqlConnection(loadstring))
            {
                connection.Open();
                string combo = cboSPK.Text;
                var queryStatSPK = "Select status from spk where noSPK = '" + combo + "'";
                //MessageBox.Show("bisa");
                using (var command2 = new MySqlCommand(queryStatSPK, connection))
                {
                    using (var reader2 = command2.ExecuteReader())
                    {
                        int status = 0;
                        while (reader2.Read())
                        {
                            status = reader2.GetInt32("status");
                            if (status == 0)
                            {
                                txtStatusHapus.Text = "Status : BELUM DIVERIFIKASI";
                                //btnHapus.Visible = true;
                            }
                            else
                            {
                                txtStatusHapus.Text = "Status : SUDAH DIVERIFIKASI";
                                //btnHapus.Visible = false;
                            }
                        }
                    }
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            //dt.ToString("hh:MM:ss");
            this.lblDateTime_A_FTM.Text = dt.ToString("hh:MM:ss");
        }
    }
}
