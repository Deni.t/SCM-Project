﻿namespace Admin
{
    partial class FormHapusSPK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.cboSPK = new System.Windows.Forms.ComboBox();
            this.txtStatusHapus = new System.Windows.Forms.TextBox();
            this.btnHapus = new System.Windows.Forms.Button();
            this.lblDateTime_A_FTM = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnKembali = new System.Windows.Forms.Button();
            this.labelUser = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(53, 66);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cari No SPK : ";
            // 
            // cboSPK
            // 
            this.cboSPK.FormattingEnabled = true;
            this.cboSPK.Location = new System.Drawing.Point(140, 63);
            this.cboSPK.Margin = new System.Windows.Forms.Padding(2);
            this.cboSPK.Name = "cboSPK";
            this.cboSPK.Size = new System.Drawing.Size(165, 21);
            this.cboSPK.TabIndex = 1;
            this.cboSPK.SelectedIndexChanged += new System.EventHandler(this.cboSPK_SelectedIndexChanged);
            // 
            // txtStatusHapus
            // 
            this.txtStatusHapus.Location = new System.Drawing.Point(56, 109);
            this.txtStatusHapus.Margin = new System.Windows.Forms.Padding(2);
            this.txtStatusHapus.Multiline = true;
            this.txtStatusHapus.Name = "txtStatusHapus";
            this.txtStatusHapus.Size = new System.Drawing.Size(326, 59);
            this.txtStatusHapus.TabIndex = 2;
            // 
            // btnHapus
            // 
            this.btnHapus.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHapus.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHapus.FlatAppearance.BorderSize = 0;
            this.btnHapus.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.btnHapus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHapus.Location = new System.Drawing.Point(386, 109);
            this.btnHapus.Margin = new System.Windows.Forms.Padding(2);
            this.btnHapus.Name = "btnHapus";
            this.btnHapus.Size = new System.Drawing.Size(113, 59);
            this.btnHapus.TabIndex = 3;
            this.btnHapus.Text = "HAPUS";
            this.btnHapus.UseVisualStyleBackColor = false;
            this.btnHapus.Click += new System.EventHandler(this.btnHapus_Click);
            // 
            // lblDateTime_A_FTM
            // 
            this.lblDateTime_A_FTM.AutoSize = true;
            this.lblDateTime_A_FTM.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblDateTime_A_FTM.Location = new System.Drawing.Point(2, 329);
            this.lblDateTime_A_FTM.Name = "lblDateTime_A_FTM";
            this.lblDateTime_A_FTM.Size = new System.Drawing.Size(58, 13);
            this.lblDateTime_A_FTM.TabIndex = 12;
            this.lblDateTime_A_FTM.Text = "Date/Time";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnKembali
            // 
            this.btnKembali.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnKembali.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnKembali.FlatAppearance.BorderSize = 0;
            this.btnKembali.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.btnKembali.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKembali.Location = new System.Drawing.Point(479, 299);
            this.btnKembali.Margin = new System.Windows.Forms.Padding(2);
            this.btnKembali.Name = "btnKembali";
            this.btnKembali.Size = new System.Drawing.Size(75, 48);
            this.btnKembali.TabIndex = 14;
            this.btnKembali.Text = "KEMBALI";
            this.btnKembali.UseVisualStyleBackColor = false;
            this.btnKembali.Click += new System.EventHandler(this.btnKembali_Click);
            // 
            // labelUser
            // 
            this.labelUser.AutoSize = true;
            this.labelUser.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelUser.Font = new System.Drawing.Font("Schadow BT", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUser.Location = new System.Drawing.Point(398, 6);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(49, 19);
            this.labelUser.TabIndex = 15;
            this.labelUser.Text = "label2";
            // 
            // FormHapusSPK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Admin.Properties.Resources.Admin_SPK_Hapus;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(569, 353);
            this.Controls.Add(this.labelUser);
            this.Controls.Add(this.btnKembali);
            this.Controls.Add(this.lblDateTime_A_FTM);
            this.Controls.Add(this.btnHapus);
            this.Controls.Add(this.txtStatusHapus);
            this.Controls.Add(this.cboSPK);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FormHapusSPK";
            this.Text = "FormHapusSPK";
            this.Load += new System.EventHandler(this.FormHapusSPK_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboSPK;
        private System.Windows.Forms.TextBox txtStatusHapus;
        private System.Windows.Forms.Button btnHapus;
        private System.Windows.Forms.Label lblDateTime_A_FTM;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnKembali;
        private System.Windows.Forms.Label labelUser;
    }
}