﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin
{
    public partial class FormPesan : Form
    {
        string admin_username;
        string admin_kategori;

        public FormPesan()
        {
            InitializeComponent();
        }

        private void buttonPesan_Click(object sender, EventArgs e)
        {

            FormPesanTambah frm = new FormPesanTambah();
            frm.Owner = this;
            frm.Show();
        }

        private void FormPesan_Load(object sender, EventArgs e)
        {
         //   Admin_FormHome AFH = (Admin_FormHome)this.Owner;
           
        }

        private void buttonKembali_Click(object sender, EventArgs e)
        {

        }

        private void buttonTambahBahan_Click(object sender, EventArgs e)
        {
            textBoxJumlah.Enabled = true;
            textBoxKeterangan.Enabled = true;
            DPesan daftar = new DPesan();

            string hasilk = daftar.GenerateKode();
            if (hasilk == "sukses")
            {
                labelkode.Text = daftar.KodeTerbaru;

            }
            else
            {
                MessageBox.Show("Generate kode gagal dilakukan. Pesan kesalahan = " + hasilk);
            }

            Pemesanan pemesanan = new Pemesanan(labelkode.Text, comboBoxNoSpk.Text.Split('-')[0], DateTime.Now);
            string hasil = daftar.TambahData(pemesanan);

            if (hasil == "sukses")
            {
                MessageBox.Show("Data bahan baku telah tersimpan", "Info");
            }
            else
            {
                MessageBox.Show("Data kategori gagal tersimpan, Pesan kesalahan : " + hasil, "Kesalahan");
            }


            buttonTambahBahan.Enabled = false;
            comboBoxID.Enabled = false;
            comboBoxNoSpk.Enabled = false;
            buttonSubmit.Enabled = true;
        }

        private void buttonSubmit_Click(object sender, EventArgs e)
        {

            PesanBahan tb = new PesanBahan(comboBoxID.Text.Split('-')[0], labelkode.Text, int.Parse(comboBoxNoSpk.Text.Split('-')[0]), int.Parse(textBoxJumlah.Text), textBoxKeterangan.Text);

            string hasil = tb.TambahData(tb);
            if (hasil == "sukses")
            {
                MessageBox.Show("Data pesan bahan telah tersimpan", "Info");
            }
            else
            {
                MessageBox.Show("Data pesan gagal tersimpan, Pesan kesalahan : " + hasil, "Kesalahan");
            }
            textBoxJumlah.Enabled = false;
            textBoxKeterangan.Enabled = false;
            buttonTambahBahan.Enabled = false;
            buttonSubmit.Enabled = false;





            DPesan dp = new DPesan();

            string hasildp = dp.BacaSemuaData();

            if (hasildp == "sukses")
            {
                comboBoxNoSpk.Items.Clear();
                for (int i = 0; i < dp.JumlahBarang; i++)
                {
                    comboBoxNoSpk.Items.Add(dp.ListPesan[i].NoSPK + "-" + dp.ListPesan[i].KodeBB);
                }
                comboBoxNoSpk.SelectedIndex = 0;
            }
            DTambah dt = new DTambah();
            string hasildt = dt.BacaSemuaData();
            if (hasildp == "sukses")
            {

                comboBoxID.Items.Clear();
                for (int i = 0; i < dt.JumlahBarang; i++)
                {
                    comboBoxID.Items.Add(dt.ListTambah[i].Kode + "-" + dt.ListTambah[i].Nama);
                }
                comboBoxNoSpk.SelectedIndex = 0;
            }
            DPesan daftar = new DPesan();
            hasil = daftar.BacaSemuaData();

            if (hasil == "sukses")
            {
                dataGridView1.DataSource = daftar.ListPesan;
            }
            else
            {
                dataGridView1.Rows.Clear();
            }
            buttonSubmit.Enabled = false;
        }

        private void buttonHapus_Click(object sender, EventArgs e)
        {
            labelkode.Text = "";
            labelkode.Visible = true;
            comboBoxID.Text = "";
            comboBoxID.Enabled = true;
            comboBoxNoSpk.Text = "";
            comboBoxNoSpk.Enabled = true;
            textBoxJumlah.Text = "";
            textBoxKeterangan.Text = "";
            buttonTambahBahan.Enabled = true;

        }

        private void FormPesan_FormClosing(object sender, FormClosingEventArgs e)
        {
            //this.Close();
        }

        private void buttonKembali_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonPesan_Click_1(object sender, EventArgs e)
        {

        }

        private void FormPesan_Load_1(object sender, EventArgs e)
        {
            this.CenterToScreen();
            timer1_A_FJS.Start();
            textBoxJumlah.Enabled = false;
            textBoxKeterangan.Enabled = false;
            DPesan dp = new DPesan();

            string hasildp = dp.BacaSemuaData();

            if (hasildp == "sukses")
            {
                comboBoxNoSpk.Items.Clear();
                for (int i = 0; i < dp.JumlahBarang; i++)
                {
                    comboBoxNoSpk.Items.Add(dp.ListPesan[i].NoSPK + "-" + dp.ListPesan[i].KodeBB);
                }
                comboBoxNoSpk.SelectedIndex = 0;
            }
            DTambah dt = new DTambah();
            string hasildt = dt.BacaSemuaData();
            if (hasildp == "sukses")
            {

                comboBoxID.Items.Clear();
                for (int i = 0; i < dt.JumlahBarang; i++)
                {
                    comboBoxID.Items.Add(dt.ListTambah[i].Kode + "-" + dt.ListTambah[i].Nama);
                }
                comboBoxNoSpk.SelectedIndex = 0;
            }
            DPesan daftar = new DPesan();
            string hasil = daftar.BacaSemuaData();

            if (hasil == "sukses")
            {
                dataGridView1.DataSource = daftar.ListPesan;
            }
            else
            {
                dataGridView1.Rows.Clear();
            }

            Admin_FormHome FH = (Admin_FormHome)this.Owner;

            if (FH.status == 1)
            {
                comboBoxID.Enabled = false;
                comboBoxNoSpk.Enabled = false;
                textBoxJumlah.Enabled = false;
                textBoxKeterangan.Enabled = false;
                buttonTambahBahan.Enabled = false;
                buttonSubmit.Enabled = false;
                buttonHapus.Enabled = false;
                buttonPesan.Enabled = false;
                label3.Text = FH.labelUser.Text;
            }
            label3.Text = FH.labelUser.Text;
            buttonSubmit.Enabled = false;
        }

        private void timer1_A_FJS_Tick(object sender, EventArgs e)
        {
            DateTime dta = DateTime.Now;
            labelJam.Text = dta.ToString("hh:MM:ss");
        }
    }
}
