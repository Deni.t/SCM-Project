﻿namespace Admin
{
    partial class FormStatusSPK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cboSPK = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lboStatusSPK = new System.Windows.Forms.ListBox();
            this.btnCetak = new System.Windows.Forms.Button();
            this.btnKembali = new System.Windows.Forms.Button();
            this.lblDateTime_A_FTM = new System.Windows.Forms.Label();
            this.btnKomentar = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.labeluser = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cboSPK
            // 
            this.cboSPK.FormattingEnabled = true;
            this.cboSPK.Location = new System.Drawing.Point(124, 102);
            this.cboSPK.Margin = new System.Windows.Forms.Padding(2);
            this.cboSPK.Name = "cboSPK";
            this.cboSPK.Size = new System.Drawing.Size(253, 21);
            this.cboSPK.TabIndex = 3;
            this.cboSPK.SelectedIndexChanged += new System.EventHandler(this.cboSPK_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(33, 105);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Cari No SPK : ";
            // 
            // lboStatusSPK
            // 
            this.lboStatusSPK.FormattingEnabled = true;
            this.lboStatusSPK.Location = new System.Drawing.Point(41, 141);
            this.lboStatusSPK.Margin = new System.Windows.Forms.Padding(2);
            this.lboStatusSPK.Name = "lboStatusSPK";
            this.lboStatusSPK.Size = new System.Drawing.Size(528, 225);
            this.lboStatusSPK.TabIndex = 4;
            // 
            // btnCetak
            // 
            this.btnCetak.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCetak.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCetak.FlatAppearance.BorderSize = 0;
            this.btnCetak.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.btnCetak.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCetak.Location = new System.Drawing.Point(228, 373);
            this.btnCetak.Margin = new System.Windows.Forms.Padding(2);
            this.btnCetak.Name = "btnCetak";
            this.btnCetak.Size = new System.Drawing.Size(224, 42);
            this.btnCetak.TabIndex = 5;
            this.btnCetak.Text = "CETAK";
            this.btnCetak.UseVisualStyleBackColor = false;
            this.btnCetak.Click += new System.EventHandler(this.btnCetak_Click);
            // 
            // btnKembali
            // 
            this.btnKembali.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnKembali.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnKembali.FlatAppearance.BorderSize = 0;
            this.btnKembali.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.btnKembali.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKembali.Location = new System.Drawing.Point(484, 376);
            this.btnKembali.Margin = new System.Windows.Forms.Padding(2);
            this.btnKembali.Name = "btnKembali";
            this.btnKembali.Size = new System.Drawing.Size(92, 39);
            this.btnKembali.TabIndex = 6;
            this.btnKembali.Text = "KEMBALI";
            this.btnKembali.UseVisualStyleBackColor = false;
            this.btnKembali.Click += new System.EventHandler(this.btnKembali_Click);
            // 
            // lblDateTime_A_FTM
            // 
            this.lblDateTime_A_FTM.AutoSize = true;
            this.lblDateTime_A_FTM.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblDateTime_A_FTM.Location = new System.Drawing.Point(3, 387);
            this.lblDateTime_A_FTM.Name = "lblDateTime_A_FTM";
            this.lblDateTime_A_FTM.Size = new System.Drawing.Size(58, 13);
            this.lblDateTime_A_FTM.TabIndex = 10;
            this.lblDateTime_A_FTM.Text = "Date/Time";
            // 
            // btnKomentar
            // 
            this.btnKomentar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnKomentar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnKomentar.FlatAppearance.BorderSize = 0;
            this.btnKomentar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.btnKomentar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKomentar.Location = new System.Drawing.Point(63, 373);
            this.btnKomentar.Margin = new System.Windows.Forms.Padding(2);
            this.btnKomentar.Name = "btnKomentar";
            this.btnKomentar.Size = new System.Drawing.Size(145, 42);
            this.btnKomentar.TabIndex = 12;
            this.btnKomentar.Text = "KOMENTAR";
            this.btnKomentar.UseVisualStyleBackColor = false;
            this.btnKomentar.Click += new System.EventHandler(this.btnKomentar_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // labeluser
            // 
            this.labeluser.AutoSize = true;
            this.labeluser.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labeluser.Font = new System.Drawing.Font("Schadow BT", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labeluser.Location = new System.Drawing.Point(413, 8);
            this.labeluser.Name = "labeluser";
            this.labeluser.Size = new System.Drawing.Size(49, 19);
            this.labeluser.TabIndex = 13;
            this.labeluser.Text = "label2";
            // 
            // FormStatusSPK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Admin.Properties.Resources.Admin_SPK_Status;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(587, 420);
            this.Controls.Add(this.labeluser);
            this.Controls.Add(this.btnKomentar);
            this.Controls.Add(this.lblDateTime_A_FTM);
            this.Controls.Add(this.btnKembali);
            this.Controls.Add(this.btnCetak);
            this.Controls.Add(this.lboStatusSPK);
            this.Controls.Add(this.cboSPK);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FormStatusSPK";
            this.Text = "FormStatusSPK";
            this.Load += new System.EventHandler(this.FormStatusSPK_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboSPK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lboStatusSPK;
        private System.Windows.Forms.Button btnCetak;
        private System.Windows.Forms.Button btnKembali;
        private System.Windows.Forms.Label lblDateTime_A_FTM;
        private System.Windows.Forms.Button btnKomentar;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label labeluser;
    }
}