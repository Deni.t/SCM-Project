﻿namespace Admin
{
    partial class ProgresProduksi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonPenerimaan = new System.Windows.Forms.Button();
            this.buttonPengiriman = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.labelDate = new System.Windows.Forms.Label();
            this.timer1_A_FJS = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // buttonPenerimaan
            // 
            this.buttonPenerimaan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonPenerimaan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonPenerimaan.FlatAppearance.BorderSize = 0;
            this.buttonPenerimaan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.buttonPenerimaan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPenerimaan.Location = new System.Drawing.Point(254, 237);
            this.buttonPenerimaan.Name = "buttonPenerimaan";
            this.buttonPenerimaan.Size = new System.Drawing.Size(113, 105);
            this.buttonPenerimaan.TabIndex = 20;
            this.buttonPenerimaan.Text = "RIWAYAT PROGRES";
            this.buttonPenerimaan.UseVisualStyleBackColor = false;
            this.buttonPenerimaan.Click += new System.EventHandler(this.buttonPenerimaan_Click);
            // 
            // buttonPengiriman
            // 
            this.buttonPengiriman.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonPengiriman.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonPengiriman.FlatAppearance.BorderSize = 0;
            this.buttonPengiriman.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.buttonPengiriman.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPengiriman.Location = new System.Drawing.Point(81, 237);
            this.buttonPengiriman.Name = "buttonPengiriman";
            this.buttonPengiriman.Size = new System.Drawing.Size(134, 105);
            this.buttonPengiriman.TabIndex = 19;
            this.buttonPengiriman.Text = "TAMBAH PROGRES";
            this.buttonPengiriman.UseVisualStyleBackColor = false;
            this.buttonPengiriman.Click += new System.EventHandler(this.buttonPengiriman_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(399, 344);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(107, 43);
            this.button1.TabIndex = 18;
            this.button1.Text = "Kembali";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Font = new System.Drawing.Font("Schadow BT", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(359, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 19);
            this.label2.TabIndex = 21;
            this.label2.Text = "label2";
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelDate.Location = new System.Drawing.Point(1, 362);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(35, 13);
            this.labelDate.TabIndex = 22;
            this.labelDate.Text = "label1";
            // 
            // timer1_A_FJS
            // 
            this.timer1_A_FJS.Tick += new System.EventHandler(this.timer1_A_FJS_Tick);
            // 
            // ProgresProduksi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Admin.Properties.Resources.Admin_Progress;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(509, 390);
            this.Controls.Add(this.labelDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonPenerimaan);
            this.Controls.Add(this.buttonPengiriman);
            this.Controls.Add(this.button1);
            this.Name = "ProgresProduksi";
            this.Text = "ProgresProduksi";
            this.Load += new System.EventHandler(this.ProgresProduksi_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonPenerimaan;
        private System.Windows.Forms.Button buttonPengiriman;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.Timer timer1_A_FJS;
    }
}