﻿namespace Admin
{
    partial class Admin_FormSPK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonBuatSPK = new System.Windows.Forms.Button();
            this.btnStatusSPK = new System.Windows.Forms.Button();
            this.btnHapusSPK = new System.Windows.Forms.Button();
            this.labelDate = new System.Windows.Forms.Label();
            this.labelUser = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // buttonBuatSPK
            // 
            this.buttonBuatSPK.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonBuatSPK.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonBuatSPK.FlatAppearance.BorderSize = 0;
            this.buttonBuatSPK.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.buttonBuatSPK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBuatSPK.Location = new System.Drawing.Point(73, 81);
            this.buttonBuatSPK.Name = "buttonBuatSPK";
            this.buttonBuatSPK.Size = new System.Drawing.Size(143, 137);
            this.buttonBuatSPK.TabIndex = 3;
            this.buttonBuatSPK.Text = "BUAT SPK";
            this.buttonBuatSPK.UseVisualStyleBackColor = false;
            this.buttonBuatSPK.Click += new System.EventHandler(this.buttonBuatSPK_Click);
            // 
            // btnStatusSPK
            // 
            this.btnStatusSPK.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnStatusSPK.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStatusSPK.FlatAppearance.BorderSize = 0;
            this.btnStatusSPK.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.btnStatusSPK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStatusSPK.Location = new System.Drawing.Point(245, 81);
            this.btnStatusSPK.Name = "btnStatusSPK";
            this.btnStatusSPK.Size = new System.Drawing.Size(138, 137);
            this.btnStatusSPK.TabIndex = 4;
            this.btnStatusSPK.Text = "STATUS SPK";
            this.btnStatusSPK.UseVisualStyleBackColor = false;
            this.btnStatusSPK.Click += new System.EventHandler(this.btnStatusSPK_Click);
            // 
            // btnHapusSPK
            // 
            this.btnHapusSPK.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHapusSPK.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHapusSPK.FlatAppearance.BorderSize = 0;
            this.btnHapusSPK.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.btnHapusSPK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHapusSPK.Location = new System.Drawing.Point(414, 81);
            this.btnHapusSPK.Name = "btnHapusSPK";
            this.btnHapusSPK.Size = new System.Drawing.Size(149, 137);
            this.btnHapusSPK.TabIndex = 5;
            this.btnHapusSPK.Text = "HAPUS SPK";
            this.btnHapusSPK.UseVisualStyleBackColor = false;
            this.btnHapusSPK.Click += new System.EventHandler(this.btnHapusSPK_Click);
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelDate.Location = new System.Drawing.Point(3, 413);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(58, 13);
            this.labelDate.TabIndex = 9;
            this.labelDate.Text = "Date/Time";
            // 
            // labelUser
            // 
            this.labelUser.AutoSize = true;
            this.labelUser.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelUser.Font = new System.Drawing.Font("Schadow BT", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUser.Location = new System.Drawing.Point(441, 10);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(41, 19);
            this.labelUser.TabIndex = 8;
            this.labelUser.Text = "User";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Admin_FormSPK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Admin.Properties.Resources.Admin_SPK;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(626, 445);
            this.Controls.Add(this.labelDate);
            this.Controls.Add(this.labelUser);
            this.Controls.Add(this.btnHapusSPK);
            this.Controls.Add(this.btnStatusSPK);
            this.Controls.Add(this.buttonBuatSPK);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Admin_FormSPK";
            this.Text = "Admin_FormSPK";
            this.Load += new System.EventHandler(this.Admin_FormSPK_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonBuatSPK;
        private System.Windows.Forms.Button btnStatusSPK;
        private System.Windows.Forms.Button btnHapusSPK;
        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.Timer timer1;
        public System.Windows.Forms.Label labelUser;
    }
}