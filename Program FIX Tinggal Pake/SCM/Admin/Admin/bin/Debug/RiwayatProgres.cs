﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.OleDb;

namespace Admin
{
    public partial class RiwayatProgres : Form
    {
        string loadstring = @"server=localhost;database=scm;userid=root;password=;";
        public RiwayatProgres()
        {
            InitializeComponent();
        }

        private void RiwayatProgres_Load(object sender, EventArgs e)
        {
            this.CenterToScreen(); timer1_A_FJS.Start(); DateTime dt = DateTime.Now;
            labelDate.Text = dt.ToString("hh:MM:ss"); 
            MySqlConnection con = new MySqlConnection(loadstring);
            con.Open();
            try
            {
                MySqlCommand cmd = con.CreateCommand();
                cmd.CommandText = "SELECT * FROM progressproduksi";
                MySqlDataAdapter adap = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adap.Fill(ds);
                dataGridView1.DataSource = ds.Tables[0].DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void timer1_A_FJS_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            labelDate.Text = dt.ToString("hh:MM:ss"); 
        }
    }
}
