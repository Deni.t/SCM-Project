﻿namespace Admin
{
    partial class FormPesanTambah
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelJam = new System.Windows.Forms.Label();
            this.buttonKembali = new System.Windows.Forms.Button();
            this.labelUser = new System.Windows.Forms.Label();
            this.buttonSimpan = new System.Windows.Forms.Button();
            this.textBoxSatuan = new System.Windows.Forms.TextBox();
            this.textBoxNama = new System.Windows.Forms.TextBox();
            this.textBoxKode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1_A_FJS = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // labelJam
            // 
            this.labelJam.AutoSize = true;
            this.labelJam.BackColor = System.Drawing.Color.Transparent;
            this.labelJam.Location = new System.Drawing.Point(6, 317);
            this.labelJam.Name = "labelJam";
            this.labelJam.Size = new System.Drawing.Size(26, 13);
            this.labelJam.TabIndex = 47;
            this.labelJam.Text = "Jam";
            // 
            // buttonKembali
            // 
            this.buttonKembali.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonKembali.FlatAppearance.BorderSize = 0;
            this.buttonKembali.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.buttonKembali.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonKembali.Location = new System.Drawing.Point(453, 299);
            this.buttonKembali.Name = "buttonKembali";
            this.buttonKembali.Size = new System.Drawing.Size(79, 31);
            this.buttonKembali.TabIndex = 45;
            this.buttonKembali.Text = "KEMBALI";
            this.buttonKembali.UseVisualStyleBackColor = false;
            // 
            // labelUser
            // 
            this.labelUser.AutoSize = true;
            this.labelUser.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labelUser.Font = new System.Drawing.Font("Schadow BT", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUser.Location = new System.Drawing.Point(382, 5);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(41, 19);
            this.labelUser.TabIndex = 44;
            this.labelUser.Text = "User";
            // 
            // buttonSimpan
            // 
            this.buttonSimpan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonSimpan.FlatAppearance.BorderSize = 0;
            this.buttonSimpan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.buttonSimpan.Location = new System.Drawing.Point(173, 219);
            this.buttonSimpan.Name = "buttonSimpan";
            this.buttonSimpan.Size = new System.Drawing.Size(200, 39);
            this.buttonSimpan.TabIndex = 43;
            this.buttonSimpan.Text = "SIMPAN";
            this.buttonSimpan.UseVisualStyleBackColor = false;
            // 
            // textBoxSatuan
            // 
            this.textBoxSatuan.Location = new System.Drawing.Point(291, 184);
            this.textBoxSatuan.Name = "textBoxSatuan";
            this.textBoxSatuan.Size = new System.Drawing.Size(100, 20);
            this.textBoxSatuan.TabIndex = 42;
            // 
            // textBoxNama
            // 
            this.textBoxNama.Location = new System.Drawing.Point(291, 155);
            this.textBoxNama.Name = "textBoxNama";
            this.textBoxNama.Size = new System.Drawing.Size(100, 20);
            this.textBoxNama.TabIndex = 41;
            // 
            // textBoxKode
            // 
            this.textBoxKode.Location = new System.Drawing.Point(291, 129);
            this.textBoxKode.Name = "textBoxKode";
            this.textBoxKode.Size = new System.Drawing.Size(100, 20);
            this.textBoxKode.TabIndex = 40;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(172, 187);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 39;
            this.label3.Text = "SATUAN:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(172, 158);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 13);
            this.label2.TabIndex = 38;
            this.label2.Text = "NAMA BAHAN BAKU:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(172, 129);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 37;
            this.label1.Text = "KODE BAHAN BAKU:";
            // 
            // timer1_A_FJS
            // 
            this.timer1_A_FJS.Tick += new System.EventHandler(this.timer1_A_FJS_Tick);
            // 
            // FormPesanTambah
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Admin.Properties.Resources.Admin_Pesan_Tambah;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(545, 341);
            this.Controls.Add(this.labelJam);
            this.Controls.Add(this.buttonKembali);
            this.Controls.Add(this.labelUser);
            this.Controls.Add(this.buttonSimpan);
            this.Controls.Add(this.textBoxSatuan);
            this.Controls.Add(this.textBoxNama);
            this.Controls.Add(this.textBoxKode);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FormPesanTambah";
            this.Text = "FormPesanTambah";
            this.Load += new System.EventHandler(this.FormPesanTambah_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelJam;
        private System.Windows.Forms.Button buttonKembali;
        private System.Windows.Forms.Label labelUser;
        private System.Windows.Forms.Button buttonSimpan;
        private System.Windows.Forms.TextBox textBoxSatuan;
        private System.Windows.Forms.TextBox textBoxNama;
        private System.Windows.Forms.TextBox textBoxKode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1_A_FJS;
    }
}