﻿namespace ProjectBesarSCM
{
    partial class FormKeuanganHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelHello = new System.Windows.Forms.Label();
            this.buttonPembelian = new System.Windows.Forms.Button();
            this.buttonTransaksiCustomer = new System.Windows.Forms.Button();
            this.labelDate = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // labelHello
            // 
            this.labelHello.AutoSize = true;
            this.labelHello.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelHello.Font = new System.Drawing.Font("Schadow BT", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHello.Location = new System.Drawing.Point(587, 7);
            this.labelHello.Name = "labelHello";
            this.labelHello.Size = new System.Drawing.Size(60, 19);
            this.labelHello.TabIndex = 4;
            this.labelHello.Text = "Hello  , ";
            // 
            // buttonPembelian
            // 
            this.buttonPembelian.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonPembelian.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonPembelian.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonPembelian.FlatAppearance.BorderSize = 0;
            this.buttonPembelian.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.buttonPembelian.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPembelian.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPembelian.Location = new System.Drawing.Point(64, 57);
            this.buttonPembelian.Name = "buttonPembelian";
            this.buttonPembelian.Size = new System.Drawing.Size(217, 114);
            this.buttonPembelian.TabIndex = 7;
            this.buttonPembelian.Text = "Pembelian\r\nBahan Baku";
            this.buttonPembelian.UseVisualStyleBackColor = false;
            this.buttonPembelian.Click += new System.EventHandler(this.buttonPembelian_Click);
            // 
            // buttonTransaksiCustomer
            // 
            this.buttonTransaksiCustomer.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonTransaksiCustomer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonTransaksiCustomer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonTransaksiCustomer.FlatAppearance.BorderSize = 0;
            this.buttonTransaksiCustomer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.buttonTransaksiCustomer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTransaksiCustomer.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTransaksiCustomer.Location = new System.Drawing.Point(317, 57);
            this.buttonTransaksiCustomer.Name = "buttonTransaksiCustomer";
            this.buttonTransaksiCustomer.Size = new System.Drawing.Size(209, 114);
            this.buttonTransaksiCustomer.TabIndex = 8;
            this.buttonTransaksiCustomer.Text = "TRANSAKSI CUSTOMER";
            this.buttonTransaksiCustomer.UseVisualStyleBackColor = false;
            this.buttonTransaksiCustomer.Click += new System.EventHandler(this.buttonTransaksiCustomer_Click);
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelDate.Location = new System.Drawing.Point(7, 333);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(30, 13);
            this.labelDate.TabIndex = 9;
            this.labelDate.Text = "Date";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // FormKeuanganHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ProjectBesarSCM.Properties.Resources.Keuangan_Home;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(838, 358);
            this.Controls.Add(this.labelDate);
            this.Controls.Add(this.buttonTransaksiCustomer);
            this.Controls.Add(this.buttonPembelian);
            this.Controls.Add(this.labelHello);
            this.Name = "FormKeuanganHome";
            this.Text = "FormKeuanganHome";
            this.Load += new System.EventHandler(this.FormKeuanganHome_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonPembelian;
        private System.Windows.Forms.Button buttonTransaksiCustomer;
        public System.Windows.Forms.Label labelHello;
        public System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.Timer timer1;
    }
}