﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.IO;
using System.Drawing;
using System.Drawing.Printing;

namespace ProjectBesarSCM
{
    public class ClassKeuanganSupp
    {
        private string id;
        private string nama;
        private string alamat;

        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Nama
        {
            get { return nama; }
            set { nama = value; }
        }

        public string Alamat
        {
            get { return alamat; }
            set { alamat = value; }
        }

        #region Constructor
        public ClassKeuanganSupp()
        {
            id = "";
            nama = "";
            alamat = "";
        }

        public ClassKeuanganSupp(string ID, string name, string add)
        {
            id = ID;
            nama = name;
            alamat = add;
        }
        #endregion
    }
}
