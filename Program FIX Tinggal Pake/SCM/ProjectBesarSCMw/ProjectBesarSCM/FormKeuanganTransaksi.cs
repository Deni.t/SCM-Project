﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectBesarSCM
{
    public partial class FormKeuanganTransaksi : Form
    {
        public FormKeuanganTransaksi()
        {
            InitializeComponent();
        }
        //public string ambilSpk(FormKeuanganTransaksiInput txt)
        //{
        //    return abc;
        //}

        public string abc="";
        public int jmlah = 0;

        private void FormKeuanganTransaksi_Load(object sender, EventArgs e)
        {
            this.CenterToScreen(); timer1.Start();
            FormKeuanganHome FKH = (FormKeuanganHome)this.Owner;

            labelHello.Text = FKH.labelHello.Text;
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            ClassListKeuanganTransaksi clkt = new ClassListKeuanganTransaksi();
            string hasil = clkt.CariSpk(textBoxNoSpk.Text);
            if (hasil == "sukses")
            {
                if (clkt.jmlh > 0)
                {
                    labelInfoTanggal.Text = clkt.ListKT[0].Tanggal.ToString();
                    labelInfoPemesan.Text = clkt.ListKT[0].Nama;
                    labelInfoAlamat.Text = clkt.ListKT[0].Alamat;
                    labelInfoPekerjaan.Text = clkt.ListKT[0].Pekerjaan;
                    labelInfoTotal.Text = clkt.ListKT[0].Total.ToString();
                    abc = clkt.ListKT[0].Id;
                    jmlah = clkt.ListKT[0].Total;
                    
                }
                else
                {

                }
            }
            else
            {
                MessageBox.Show("perintah gagal" + hasil);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();

            Form form = Application.OpenForms["FormKeuanganHome"];

            if (form == null)
            {
                FormKeuanganHome formKeuanganHome = new FormKeuanganHome();
                formKeuanganHome.Owner = this;
                formKeuanganHome.Show();
            }
            else
            {
                form.Show();
                form.BringToFront();
            }
        }

        private void buttonInputPembayaran_Click(object sender, EventArgs e)
        {
            Form form = Application.OpenForms["FormKeuanganTransaksiInput"];

            if (form == null)
            {
                FormKeuanganTransaksiInput formKeuanganTransaksiInput = new FormKeuanganTransaksiInput();
                formKeuanganTransaksiInput.Owner = this;
                formKeuanganTransaksiInput.kode = int.Parse(textBoxNoSpk.Text);
                formKeuanganTransaksiInput.ttl = jmlah;
                formKeuanganTransaksiInput.Show();
            }
            else
            {
                form.Show();
                form.BringToFront();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            labelDate.Text = DateTime.Now.ToString("hh:mm:ss");
        }
    }
}
