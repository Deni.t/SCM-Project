﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectBesarSCM
{
    public class Pembelian
    {
        #region DM
        private string kodePembelian;
        private string tgl;
        private Supplier sup;
        private List<PembelianDetil> lPembelian;
        #endregion

        #region Properties
        public string KodePembelian
        {
            get { return kodePembelian; }
            set { kodePembelian = value; }
        }
        public string Tgl
        {
            get { return tgl; }
            set { tgl = value; }
        }
        public Supplier Sup
        {
            get { return sup; }
            set { sup = value; }
        }
        public List<PembelianDetil> LPembelian
        {
            get { return lPembelian; }
            set { lPembelian = value; }
        }
       
        #endregion

        #region Constructor
        public Pembelian()
        {
            kodePembelian = "";
            tgl = "";
            sup = new Supplier();
            lPembelian = new List<PembelianDetil>();
        }
        public Pembelian(string Kode,string Tgl, Supplier SS, List<PembelianDetil> LIST)
        {
            kodePembelian = Kode;
            Sup = SS;
            lPembelian = LIST;
            tgl = Tgl;
        }
        
        #endregion

        #region Method

        #endregion





    }
}
