﻿namespace ProjectBesarSCM
{
    partial class FormKeuanganSuppUbahTambah
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelHargaSatuan = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.labelBahanBaku = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.labelSupplier = new System.Windows.Forms.Label();
            this.labelHello = new System.Windows.Forms.Label();
            this.textBoxHargaSatuan = new System.Windows.Forms.TextBox();
            this.buttonSimpan = new System.Windows.Forms.Button();
            this.buttonLogin = new System.Windows.Forms.Button();
            this.labelDate = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // labelHargaSatuan
            // 
            this.labelHargaSatuan.AutoSize = true;
            this.labelHargaSatuan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelHargaSatuan.Location = new System.Drawing.Point(172, 212);
            this.labelHargaSatuan.Name = "labelHargaSatuan";
            this.labelHargaSatuan.Size = new System.Drawing.Size(101, 13);
            this.labelHargaSatuan.TabIndex = 24;
            this.labelHargaSatuan.Text = "HARGA SATUAN : ";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(272, 182);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(145, 21);
            this.comboBox2.TabIndex = 23;
            // 
            // labelBahanBaku
            // 
            this.labelBahanBaku.AutoSize = true;
            this.labelBahanBaku.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelBahanBaku.Location = new System.Drawing.Point(189, 185);
            this.labelBahanBaku.Name = "labelBahanBaku";
            this.labelBahanBaku.Size = new System.Drawing.Size(85, 13);
            this.labelBahanBaku.TabIndex = 22;
            this.labelBahanBaku.Text = "BAHAN BAKU : ";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(272, 155);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(145, 21);
            this.comboBox1.TabIndex = 21;
            // 
            // labelSupplier
            // 
            this.labelSupplier.AutoSize = true;
            this.labelSupplier.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelSupplier.Location = new System.Drawing.Point(204, 158);
            this.labelSupplier.Name = "labelSupplier";
            this.labelSupplier.Size = new System.Drawing.Size(69, 13);
            this.labelSupplier.TabIndex = 20;
            this.labelSupplier.Text = "SUPPLIER : ";
            // 
            // labelHello
            // 
            this.labelHello.AutoSize = true;
            this.labelHello.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelHello.Font = new System.Drawing.Font("Schadow BT", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHello.Location = new System.Drawing.Point(400, 9);
            this.labelHello.Name = "labelHello";
            this.labelHello.Size = new System.Drawing.Size(60, 19);
            this.labelHello.TabIndex = 19;
            this.labelHello.Text = "Hello  , ";
            // 
            // textBoxHargaSatuan
            // 
            this.textBoxHargaSatuan.Location = new System.Drawing.Point(272, 209);
            this.textBoxHargaSatuan.Name = "textBoxHargaSatuan";
            this.textBoxHargaSatuan.Size = new System.Drawing.Size(145, 20);
            this.textBoxHargaSatuan.TabIndex = 25;
            // 
            // buttonSimpan
            // 
            this.buttonSimpan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonSimpan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonSimpan.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonSimpan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.buttonSimpan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSimpan.Location = new System.Drawing.Point(191, 267);
            this.buttonSimpan.Name = "buttonSimpan";
            this.buttonSimpan.Size = new System.Drawing.Size(226, 51);
            this.buttonSimpan.TabIndex = 28;
            this.buttonSimpan.Text = "SIMPAN";
            this.buttonSimpan.UseVisualStyleBackColor = false;
            this.buttonSimpan.Click += new System.EventHandler(this.buttonSimpan_Click);
            // 
            // buttonLogin
            // 
            this.buttonLogin.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonLogin.FlatAppearance.BorderSize = 0;
            this.buttonLogin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonLogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLogin.Location = new System.Drawing.Point(486, 379);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(80, 40);
            this.buttonLogin.TabIndex = 29;
            this.buttonLogin.Text = "KEMBALI";
            this.buttonLogin.UseVisualStyleBackColor = false;
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelDate.Location = new System.Drawing.Point(7, 404);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(30, 13);
            this.labelDate.TabIndex = 30;
            this.labelDate.Text = "Date";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // FormKeuanganSuppUbahTambah
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ProjectBesarSCM.Properties.Resources.Keuangan_Supp_Ubah_Tambah;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(569, 431);
            this.Controls.Add(this.labelDate);
            this.Controls.Add(this.buttonLogin);
            this.Controls.Add(this.buttonSimpan);
            this.Controls.Add(this.textBoxHargaSatuan);
            this.Controls.Add(this.labelHargaSatuan);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.labelBahanBaku);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.labelSupplier);
            this.Controls.Add(this.labelHello);
            this.Name = "FormKeuanganSuppUbahTambah";
            this.Text = "FormKeuanganSuppUbahTambah";
            this.Load += new System.EventHandler(this.FormKeuanganSuppUbahTambah_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelHargaSatuan;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label labelBahanBaku;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label labelSupplier;
        private System.Windows.Forms.Label labelHello;
        private System.Windows.Forms.TextBox textBoxHargaSatuan;
        private System.Windows.Forms.Button buttonSimpan;
        private System.Windows.Forms.Button buttonLogin;
        public System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.Timer timer1;
    }
}