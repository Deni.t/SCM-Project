﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectBesarSCM
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public string nama;
        public string pass;
        
        private void buttonLogin_Click(object sender, EventArgs e)
        {
           
             nama = textBoxUser.Text;
             pass = textBoxPass.Text;

            if (nama != "null")
            {
                if (pass != "null")
                {
                   if(nama.ToLower() == "owner" || nama.ToLower() == "finance" || nama.ToLower() == "gudang" || nama.ToLower() == "admin" )
                   {
                       if(pass == nama.ToLower())
                       {
                           #region Connect
                               Koneksi k = new Koneksi("localhost", "SCM", "root", "");

                               string hasilConnect = k.Connect();  //panggil method Connect milik class Koneksi

                               if (hasilConnect == "sukses")
                               {
                                  
                                   MessageBox.Show("Welcome to SCM Project, " + nama, "Informasi");
                                   FormKeuanganHome formKeuanganHome = new FormKeuanganHome();
                                   formKeuanganHome.Owner = this;
                             
                                   formKeuanganHome.Show();
                                  
                               }
                               else
                               {
                                   MessageBox.Show("Koneksi gagal. Pesan Kesalahan : " + hasilConnect, "Kesalahan");  //tampilkan pesan kesalahan=
                               }
                           #endregion
                         
                         

                       }
                       else
                       {
                           MessageBox.Show("Wrong Password !");
                       }
                   }
                   else
                   {
                       MessageBox.Show("Please Enter a Valid Username");
                   }
                }
                else
                {
                    MessageBox.Show("Please Enter Password");
                    textBoxPass.Focus();
                }
            }
            else
            {
                MessageBox.Show("Please Enter Username");
            }

        }
       
        private void Form1_Load(object sender, EventArgs e)
        {
            
        }
    }
}
