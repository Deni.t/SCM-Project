﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace ProjectBesarSCM
{
    public partial class FormKeuanganSuppUbahTambah : Form
    {
        public FormKeuanganSuppUbahTambah()
        {
            InitializeComponent();
        }

        private void FormKeuanganSuppUbahTambah_Load(object sender, EventArgs e)
        {
            this.CenterToScreen(); timer1.Start();
        }

        private void buttonSimpan_Click(object sender, EventArgs e)
        {
           Koneksi k = new Koneksi();
            k.Connect();

            string bahanBaku = comboBox1.Text;
            string supplier = comboBox2.Text;
            int harga = int.Parse(textBoxHargaSatuan.Text);

            try
            {
                string insert = "insert into detilbahanbakusupplier(kodeBahanBaku,kodeSupplier,hargaSatuan) " +
                    "values('" + bahanBaku.ToString().Substring(0, 5) + "','" + supplier.ToString().Substring(0, 2) + "','" + harga + "')";

                MySqlCommand cmd = new MySqlCommand(insert, k.KoneksiDB);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Data berhasil ditambahkan");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Data sudah ada dengan kode sebagai berikut: " + "\nkode bahan baku = " + bahanBaku.ToString().Substring(0, 5) + "\nkode supplier = " + supplier.ToString().Substring(0, 2));
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            labelDate.Text = DateTime.Now.ToString("hh:mm:ss");
        }
    }
}
