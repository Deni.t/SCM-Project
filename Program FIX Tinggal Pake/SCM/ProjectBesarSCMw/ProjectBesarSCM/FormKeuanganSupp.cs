﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectBesarSCM
{
    public partial class FormKeuanganSupp : Form
    {
        public FormKeuanganSupp()
        {
            InitializeComponent();
        }

        public void Clean()
        {
            textBoxSupplier.Text = "";
            textBoxID.Text = "";
            textBoxAlamat.Text = "";
            comboBoxId.SelectedText = "";
            comboBoxId.SelectedIndex = -1;
        }

        public void matikan()
        {
            textBoxSupplier.Enabled = false;
            textBoxID.Enabled = false;
            textBoxAlamat.Enabled = false;
            buttonSimpan.Enabled = false;
            buttonUbahSupplier.Enabled = false;
            buttonTambahBahan.Enabled = false;
            buttonHapusBahan.Enabled = false;
            buttonHapus.Enabled = false;
            buttonSimpan.Enabled = false;
            buttonBatal.Enabled = false;
        }

        public void toogle(bool perintah)
        {
            buttonHapusSupplier.Enabled = perintah;
            buttonUpdateSupplier.Enabled = perintah;
            buttonTambahSupplier.Enabled = perintah;
        }

        private void FormKeuanganSupp_Load(object sender, EventArgs e)
        {
            this.CenterToScreen(); timer1.Start();

              isiComboBox();   
        }

        public void isiComboBox()
        {
            ClassListSupplier ls = new ClassListSupplier();

            string hasil = ls.BacaSemuaData();
            if (hasil == "sukses")
            {
                comboBoxId.Items.Clear();
                for (int i = 0; i < ls.JumlahSupp; i++)
                {
                    comboBoxId.Items.Add(ls.ListSupp[i].Id);
                }
            }
            else
            {
                MessageBox.Show("Kategori Gagal Ditampilkan di ComboBox, Pesan Kesalahan : " + hasil);
            }
        }

        private void buttonHapusSupplier_Click(object sender, EventArgs e)
        {
            toogle(false);

            comboBoxId.Enabled = true;
            buttonHapus.Enabled = true;
            buttonBatal.Enabled = true;
        }

        private void buttonSimpan_Click_1(object sender, EventArgs e)
        {
            string nama = textBoxSupplier.Text;
            string kode = textBoxID.Text;
            string alamat = textBoxAlamat.Text;

            ClassKeuanganSupp ks = new ClassKeuanganSupp(kode, nama, alamat);

            ClassListSupplier ls = new ClassListSupplier();
            
            string hasilTambah = ls.TambahSupplier(ks);
            if (hasilTambah == "sukses")
            {
                MessageBox.Show("berhasil", "Info");
            }
            else
            {
                MessageBox.Show("gagal. Pesan Kesalahan : " + hasilTambah, "kesalahan");
            }
            Clean();
            matikan();

            toogle(true);
        }

        private void buttonTambahSupplier_Click(object sender, EventArgs e)
        {
            textBoxSupplier.Enabled = true;
            textBoxID.Enabled = true;
            textBoxAlamat.Enabled = true;
            buttonSimpan.Enabled = true;

            toogle(false);
            buttonBatal.Enabled = true;
        }

        private void comboBoxSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClassListSupplier ls = new ClassListSupplier();
            string hasil = ls.CariData(comboBoxId.Text);
            if (hasil == "sukses")
            {
                if (ls.JumlahSupp > 0)
                {
                    textBoxAlamat.Text = ls.ListSupp[0].Alamat;
                    textBoxSupplier.Text = ls.ListSupp[0].Nama;
                }
                else
                {

                }
            }
            else
            {
                MessageBox.Show("Perintah Gagal" + hasil);
            }
        }

        private void buttonHapus_Click(object sender, EventArgs e)
        {
            string id = comboBoxId.Text;
            string nama = textBoxSupplier.Text;
            string alamat = textBoxAlamat.Text;

            ClassKeuanganSupp ks = new ClassKeuanganSupp(id, nama, alamat);
            ClassListSupplier ls = new ClassListSupplier();

            string hasil = ls.HapusSupplier(ks);
            if(hasil == "sukses")
            {
                MessageBox.Show("Supplier telah terhapus", "Info");
            }
            else
            {
                MessageBox.Show("gagal menghapus supplier. Pesan Kesalahan : " + hasil, "kesalahan");
            }
            isiComboBox();
            Clean();

            comboBoxId.Enabled = false;
            buttonHapus.Enabled = false;

            toogle(true);
        }

        private void buttonUpdateSupplier_Click(object sender, EventArgs e)
        {
            toogle(false);
            buttonUbahSupplier.Enabled = true;
            buttonTambahBahan.Enabled = true;
            buttonHapusBahan.Enabled = true;
            buttonBatal.Enabled = true;
        }

        private void buttonUbahSupplier_Click(object sender, EventArgs e)
        {
            FormKeuanganSuppUbahUbah form = new FormKeuanganSuppUbahUbah();
            form.Owner = this;
            form.Show();
        }

        private void buttonTambahBahan_Click(object sender, EventArgs e)
        {
            FormKeuanganSuppUbahTambah form = new FormKeuanganSuppUbahTambah();
            form.Owner = this;
            form.Show();
        }

        private void buttonHapusBahan_Click(object sender, EventArgs e)
        {
            FormKeuanganSuppUbahHapus form = new FormKeuanganSuppUbahHapus();
            form.Owner = this;
            form.Show();
        }

        private void buttonBatal_Click(object sender, EventArgs e)
        {
            Clean();

            matikan();

            toogle(true);
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            labelDate.Text = DateTime.Now.ToString("hh:mm:ss");
        }
    }
}
