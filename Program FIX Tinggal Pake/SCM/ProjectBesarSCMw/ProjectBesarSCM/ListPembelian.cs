﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
namespace ProjectBesarSCM
{
    public class ListPembelian
    {
        private List<Pembelian> listPembelian;
        private string kodePembelian;
        

        public List<Pembelian> LPembelian
        {
            get { return listPembelian; }
        }
        public int jumlahList
        {
            get { return listPembelian.Count; }
        }
        public string KodePembelian
        {
            get { return kodePembelian; }
        }
       
        public ListPembelian()
        {
            listPembelian = new List<Pembelian>();
            kodePembelian = "pembelian01";
        }

        #region BacaSemuaData
        public string BacaSemuaData()
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "SELECT FP.kodepembelian, FP.tanggal, S.kodesupplier, S.namasupplier, S.alamat from formpembelian FP INNER JOIN Supplier S ON FP.kodeSupplier = S.kodeSupplier order by FP.kodePembelian desc";
            MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
            try
            {
                MySqlDataReader data = MSC.ExecuteReader();
                while (data.Read() == true)
                {
                    string kodePembelian = data.GetValue(0).ToString();
                    string tanggal = data.GetValue(1).ToString();
                    string kodeSup = data.GetValue(2).ToString();
                    string namaSup = data.GetValue(3).ToString();
                    string alamatSup = data.GetValue(4).ToString();
                    Supplier S = new Supplier();
                    S.KodeSup = kodeSup;
                    S.NamaSup = namaSup;
                    S.Alamat = alamatSup;

                    List<PembelianDetil> listDetilPembelian = new List<PembelianDetil>();
                    Koneksi K2 = new Koneksi();
                    K2.Connect();

                    string sql2 = "select PD.KodeBahanBaku, BB.nama, PD.jumlah, PD.subtotal, PD.status, PD.total, PD.tanggalDatang from pembelian P INNER JOIN pembeliandetil PD ON P.kodepembelian = PD.kodePembelian INNER JOIN Barang BB ON PD.kodebahanbaku = BB.kodebahanbaku where P.kodePembelian = '" + kodePembelian + "'";
                    MySqlCommand MSC1 = new MySqlCommand(sql2, K2.KoneksiDB);

                    MySqlDataReader data2 = MSC.ExecuteReader();
                    while (data2.Read() == true)
                    {
                        string kodebahanbaku = data2.GetValue(0).ToString();
                        string namaBahanBaku = data2.GetValue(1).ToString();

                        BahanBaku BB = new BahanBaku();
                        BB.KodeBarang = kodebahanbaku;
                        BB.NamaBarang = namaBahanBaku;


                        int jumlah = int.Parse(data2.GetValue(2).ToString());
                        int Substotal = int.Parse(data2.GetValue(3).ToString());
                        int STATUS = int.Parse(data2.GetValue(4).ToString());
                        int TOTAL = int.Parse(data2.GetValue(5).ToString());
                        DateTime Tanggal = DateTime.Parse(data.GetValue(6).ToString());

                        PembelianDetil PD = new PembelianDetil(BB, Substotal, TOTAL, STATUS, Tanggal.ToString(), jumlah);
                        listDetilPembelian.Add(PD);
                    }
                    MSC1.Dispose();
                    data2.Dispose();

                    Pembelian P = new Pembelian(kodePembelian,tanggal, S, listDetilPembelian);
                    listPembelian.Add(P);

                }

                MSC.Dispose();
                data.Dispose();
                return "sukses";
            }
            catch (Exception E)
            {
                return E.Message;
            }
        }
        #endregion

        #region AutoGenerateCode
        public string AutoGenerateCode()
            {
                Koneksi K = new Koneksi();
                K.Connect();

                string sql = "select substring(kodePembelian,10,12) AS kodePembelian from formpembelian order by kodePembelian DESC limit 1";
                MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
                try
                {
                    MySqlDataReader Data = MSC.ExecuteReader();
                    string nomer = "";
                    if (Data.Read() == true)
                    {
                        int noPembelian = int.Parse(Data.GetValue(0).ToString()) + 1;
                        nomer = noPembelian.ToString();
                        if (nomer.Length == 1)
                        {
                            nomer = "0" + nomer;
                        }
                        else if (nomer.Length == 2)
                        {
                            nomer = "" + nomer;
                        }
                    }
                    else
                    {
                        nomer = "01";
                    }
                    kodePembelian = "Pembelian" + nomer;
                    Data.Dispose();
                    MSC.Dispose();
                    return "sukses";
                }
            catch(Exception E)
                {
                    return E.Message;
                }
            }
        #endregion

        #region AddData
        public string AddData(Pembelian P)
        {
            Koneksi K1 = new Koneksi();
            K1.Connect();
            string sql1 = "INSERT INTO formPembelian(KodePembelian, tanggal, kodeSupplier) VALUES ('" + P.KodePembelian + "','" +
                           P.Tgl.ToString() + "','" + P.Sup.KodeSup + "')";
            MySqlCommand MSC = new MySqlCommand(sql1, K1.KoneksiDB);
            try
            {
                MSC.ExecuteNonQuery();
                for (int i = 0; i < P.LPembelian.Count; i++)
                {
                    Koneksi K2 = new Koneksi();
                    K2.Connect();
                    string sql2 = "INSERT INTO formpembelianDetil(kodePembelian, KodebahanBaku, jumlah, subtotal, status, tanggaldatang, total) VALUES('" + P.KodePembelian + "','" +
                                  P.LPembelian[i].Bahan.KodeBarang + "','" + P.LPembelian[i].Jumlah + "','" + P.LPembelian[i].Sobtotal + "','" + P.LPembelian[i].Status + "','" + P.LPembelian[i].TanggalDatang + "','" + P.LPembelian[i].Total + "')";
                    MySqlCommand C2 = new MySqlCommand(sql2, K2.KoneksiDB);
                    C2.ExecuteNonQuery();
                }
                return "sukses";
            }
            catch (Exception E)
            {
                return E.Message;
            }
        }
        #endregion
       


    }
}
