﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectBesarSCM
{
    public class PembelianDetil
    {
        private BahanBaku bahan;        
        private int jumlah;
        private int subtotal;
        private int status;
        private string tanggalDatang;
        private int total;

        public BahanBaku Bahan
        {
            get { return bahan; }
            set { bahan = value; }
        }
       
        public int Sobtotal
        {
            get { return subtotal; }
            set { subtotal = value; }
        }

        public int Jumlah
        {
            get { return jumlah; }
            set { jumlah = value; }
        }
        public int Status
        {
            get { return status; }
            set { status = value; }
        }
        public string TanggalDatang
        {
            get { return tanggalDatang; }
            set { tanggalDatang = value; }
        }
        public int Total
        {
            get { return total; }
            set { total = value; }
        }

        public PembelianDetil()
        {
            bahan = new BahanBaku();
            tanggalDatang ="";
            status = 0;
            subtotal = 0;
            total = 0;
            jumlah = 0;
        }

        public PembelianDetil(BahanBaku BB, int Subtotal,int TOTAL,int STATUS, string TANGGAL, int quantity)
        {
            bahan = BB;
            tanggalDatang = TANGGAL;
            status = STATUS;
            subtotal = Subtotal;
            total = TOTAL;
         
            jumlah = quantity;
        }
    }
}
