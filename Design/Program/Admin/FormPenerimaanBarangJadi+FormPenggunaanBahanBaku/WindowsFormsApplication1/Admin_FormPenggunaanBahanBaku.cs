﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
    public partial class Admin_FormPenggunaanBahanBaku : Form
    {
        string admin_username;
        string admin_kategori;
        int stok = 0;
        public Admin_FormPenggunaanBahanBaku()
        {
            InitializeComponent();
        }

        private void buttonSimpan_Click(object sender, EventArgs e)
        {
                var loadString = @"server=localhost;database=scm;userid=root;password=;";
                using (var connection = new MySqlConnection(loadString))
                {
                    connection.Open();
                    var query = "SELECT stok from bahanbaku WHERE kode='"+comboBoxKode.Text+"'";
                    using (var command = new MySqlCommand(query, connection))
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                stok = reader.GetInt32("stok");
                            }
                        }
                    }

                    int sisa = stok- int.Parse(textBoxJumlah.Text);
                    string date = DateTime.Now.ToString("yyyy-MM-dd");

                    var query1 = "INSERT INTO formpenggunaandetil(noSPK,kodeBahanBaku,jumlahKeluar,sisaPenggunaan,tanggalKeluar) VALUES('"+comboBoxSPK.Text+"','"+comboBoxKode.Text+"','"+textBoxJumlah.Text+"','"+sisa+"','"+date+"')";
                    MySqlCommand cmd1 = new MySqlCommand(query1, connection);
                    cmd1.ExecuteNonQuery();

                    var query2 = "UPDATE bahanbaku SET stok='"+sisa+"'"+"WHERE kodeBahanBaku='"+comboBoxKode.Text+"'";
                    MySqlCommand cmd2 = new MySqlCommand(query2, connection);
                    cmd2.ExecuteNonQuery();
                }
        }

        private void Admin_FormPenggunaanBahanBaku_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            labelDateTime.Text = DateTime.Now.ToString("yyyy-MM-dd/hh:mm:ss");
            labelUser.Text = admin_username + " - " + admin_kategori;

            var loadString = @"server=localhost;database=scm;userid=root;password=;";
            using (var connection = new MySqlConnection(loadString))
            {
                connection.Open();
                var query1 = "SELECT kodeBahanBaku,nama from bahanbaku";
                using (var command1 = new MySqlCommand(query1, connection))
                {
                    using (var reader1 = command1.ExecuteReader())
                    {
                        string kode;
                        while (reader1.Read())
                        {
                            kode = reader1.GetString("kodeBahanBaku");
                            comboBoxKode.Items.Add(kode);
                            comboBoxKode.SelectedIndex = 0;
                        }
                    }
                }

                var query2 = "SELECT nama from bahanbaku where kodeBahanBaku='" + comboBoxKode.Text+"'";
                using (var command2 = new MySqlCommand(query2, connection))
                {
                    using (var reader2 = command2.ExecuteReader())
                    {
                        while (reader2.Read())
                        {
                            textBoxNamaBahan.Text = reader2.GetString("nama");
                        }
                    }
                }

                var query3 = "SELECT noSPK from spk";
                using (var command3 = new MySqlCommand(query3, connection))
                {
                    using (var reader3 = command3.ExecuteReader())
                    {
                        string kode;
                        while (reader3.Read())
                        {
                            kode = reader3.GetString("noSPK");
                            comboBoxSPK.Items.Add(kode);
                            comboBoxSPK.SelectedIndex = 0;
                        }
                    }
                }

                var query4 = "SELECT * from formpenggunaandetil";
                using (var command4 = new MySqlCommand(query4, connection))
                {
                    using (var reader4 = command4.ExecuteReader())
                    {
                        while (reader4.Read())
                        {
                            DataTable t = new DataTable();
                            MySqlDataAdapter da = new MySqlDataAdapter();
                            da.Fill(t);
                            dataGridView1.DataSource = t;
                        }
                    }
                }

            }
        }

        private void buttonKembali_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
