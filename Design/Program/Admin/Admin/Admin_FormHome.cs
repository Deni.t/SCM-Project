﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin
{
    public partial class Admin_FormHome : Form
    {
        string admin_username;
        string admin_kategori;
        public Admin_FormHome(string u, string k)
        {
            InitializeComponent();
            this.admin_username = u;
            this.admin_kategori = k;
        }

        private void FormAdmin_Home_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            labelUser.Text = admin_username + " - " +admin_kategori;
            labelDate.Text = DateTime.Now.ToString("yyyy-MM-dd/hh:mm:ss");
        }

        //private void buttonJadwalProduksi_Click(object sender, EventArgs e)
        //{
        //    FormJadwalProduksi formJadwal = new FormJadwalProduksi(admin_username, admin_kategori);
        //    formJadwal.ShowDialog();
        //    this.Hide();
        //}
        private void buttonJadwalProduksi_Click(object sender, EventArgs e)
        {
            Admin_FormJadwalProduksi formJadwal = new Admin_FormJadwalProduksi();
            formJadwal.ShowDialog();
            this.Hide();
        }

        private void FormAdmin_Home_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
        }

        private void buttonSPK_Click(object sender, EventArgs e)
        {
            Admin_FormSPK formSPK = new Admin_FormSPK(admin_username,admin_kategori);
            formSPK.ShowDialog();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            TambahMesin mesin = new TambahMesin();
            mesin.ShowDialog();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TambahBahanBaku tbb = new TambahBahanBaku();
            tbb.ShowDialog();
            this.Hide();
        }

        private void buttonProgress_Click(object sender, EventArgs e)
        {
            ProgresProduksi pro = new ProgresProduksi();
            pro.ShowDialog();
            this.Hide();
        }

        private void buttonPenggunaan_Click(object sender, EventArgs e)
        {
            FrmPenggunaanBhnBaku guna = new FrmPenggunaanBhnBaku();
            guna.ShowDialog();
            this.Hide();
        }
    }
}
