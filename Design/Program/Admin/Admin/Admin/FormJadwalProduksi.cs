﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin
{
    public partial class FormJadwalProduksi : Form
    {
        string admin_username;
        string admin_kategori;
        public FormJadwalProduksi(string u, string k)
        {
            InitializeComponent();
            this.admin_username = u;
            this.admin_kategori = k;
        }

        private void FormJadwalProduksi_Load(object sender, EventArgs e)
        {
            labelDateTime.Text = DateTime.Now.ToString("yyyy-MM-dd/hh:mm:ss");
            labelUser.Text = admin_username + " - " + admin_kategori;




        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            //comboBoxNo.SelectedIndex = 0;
            //textBoxJangkaWaktu.Text = "";
            this.Close();            
        }
    }
}
