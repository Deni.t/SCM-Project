﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin
{
    public partial class Admin_FormJadwalSubmit : Form
    {

        string data = "";
        DateTime dtBuat,dtMulai,dtSelesai;
        string jangkaWaktu, tenagaKerja, mesin;

        private void timer1_A_FJS_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            // this.labelDateTime.Text = dt.ToString("yyyy-MM-dd/hh:mm:ss");
            this.lblDateTime_A_FJS.Text = dt.ToString();
        }

        private void cboVerifikasi_A_FJS_CheckedChanged(object sender, EventArgs e)
        {
            if (cboVerifikasi_A_FJS.Checked)
            {
                btnSubmit_A_FJS.Enabled = true;
            }else
            {
                btnSubmit_A_FJS.Enabled = false;
            }
        }

        private void btnSubmit_A_FJS_Click(object sender, EventArgs e)
        {
            Koneksi k = new Koneksi();
            k.Connect();

            //string sql = "Insert into formjadwal(kodeJadwal,tanggalSelesai,status,noSPK,tenaga values ('" + noMesin + "','" + nama + "')";

            //MySqlCommand cmd = new MySqlCommand(sql, k.KoneksiDB);

            //try
            //{
            //    cmd.ExecuteNonQuery();
            //    return "sukses";
            //}
            //catch (Exception e)
            //{
            //    return e.Message;
            //}
        }

        private void btnKembali_A_FJS_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public Admin_FormJadwalSubmit(string data, DateTime dtBuat,DateTime dtMulai,DateTime dtSelesai,string jangkaWaktu, string tenagaKerja, string mesin)
        {
            InitializeComponent();
            timer1_A_FJS.Start();
            this.data = data;
            this.dtBuat = dtBuat;
            this.dtMulai = dtMulai;
            this.dtSelesai = dtSelesai;
            this.jangkaWaktu = jangkaWaktu;
            this.tenagaKerja = tenagaKerja;
            this.mesin = mesin;
        }

        private void FormJadwalSubmit_Load(object sender, EventArgs e)
        {
            //MessageBox.Show(data + " " + dtBuat + " " + dtMulai + " " + dtSelesai + " " + jangkaWaktu + " " + tenagaKerja + " " + mesin);
            string [] kodeSpk= data.Split('-');

            listBox1.Items.Add("Tanggal : " +  dtBuat);
            listBox1.Items.Add("SPK / PRAK  : " + kodeSpk[0]);
            listBox1.Items.Add("Kode Produk : " + kodeSpk[1]);
            listBox1.Items.Add("Tanggal Mulai: " + dtMulai);
            listBox1.Items.Add("Tanggal Selesai: " + dtSelesai);
            listBox1.Items.Add("Jangka waktu : " + jangkaWaktu);
            listBox1.Items.Add("Tenaga kerja : " + tenagaKerja);
            listBox1.Items.Add("Mesin : " + mesin);

        }
    }
}
