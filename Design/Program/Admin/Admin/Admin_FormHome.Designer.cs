﻿namespace Admin
{
    partial class Admin_FormHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelUser = new System.Windows.Forms.Label();
            this.labelDate = new System.Windows.Forms.Label();
            this.buttonSPK = new System.Windows.Forms.Button();
            this.buttonPesanBahanBaku = new System.Windows.Forms.Button();
            this.buttonPenggunaan = new System.Windows.Forms.Button();
            this.buttonJadwalProduksi = new System.Windows.Forms.Button();
            this.buttonProgress = new System.Windows.Forms.Button();
            this.buttonPengiriman = new System.Windows.Forms.Button();
            this.buttonPenerimaan = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelUser
            // 
            this.labelUser.AutoSize = true;
            this.labelUser.Location = new System.Drawing.Point(342, 9);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(29, 13);
            this.labelUser.TabIndex = 0;
            this.labelUser.Text = "User";
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.Location = new System.Drawing.Point(13, 13);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(58, 13);
            this.labelDate.TabIndex = 1;
            this.labelDate.Text = "Date/Time";
            // 
            // buttonSPK
            // 
            this.buttonSPK.Location = new System.Drawing.Point(115, 144);
            this.buttonSPK.Name = "buttonSPK";
            this.buttonSPK.Size = new System.Drawing.Size(93, 70);
            this.buttonSPK.TabIndex = 2;
            this.buttonSPK.Text = "SURAT PERINTAH KERJA";
            this.buttonSPK.UseVisualStyleBackColor = true;
            this.buttonSPK.Click += new System.EventHandler(this.buttonSPK_Click);
            // 
            // buttonPesanBahanBaku
            // 
            this.buttonPesanBahanBaku.Location = new System.Drawing.Point(214, 144);
            this.buttonPesanBahanBaku.Name = "buttonPesanBahanBaku";
            this.buttonPesanBahanBaku.Size = new System.Drawing.Size(93, 70);
            this.buttonPesanBahanBaku.TabIndex = 3;
            this.buttonPesanBahanBaku.Text = "PESAN BAHAN BAKU";
            this.buttonPesanBahanBaku.UseVisualStyleBackColor = true;
            // 
            // buttonPenggunaan
            // 
            this.buttonPenggunaan.Location = new System.Drawing.Point(16, 144);
            this.buttonPenggunaan.Name = "buttonPenggunaan";
            this.buttonPenggunaan.Size = new System.Drawing.Size(93, 70);
            this.buttonPenggunaan.TabIndex = 4;
            this.buttonPenggunaan.Text = "PENGGUNAAN BAHAN BAKU";
            this.buttonPenggunaan.UseVisualStyleBackColor = true;
            this.buttonPenggunaan.Click += new System.EventHandler(this.buttonPenggunaan_Click);
            // 
            // buttonJadwalProduksi
            // 
            this.buttonJadwalProduksi.Location = new System.Drawing.Point(313, 144);
            this.buttonJadwalProduksi.Name = "buttonJadwalProduksi";
            this.buttonJadwalProduksi.Size = new System.Drawing.Size(93, 70);
            this.buttonJadwalProduksi.TabIndex = 5;
            this.buttonJadwalProduksi.Text = "JADWAL PRODUKSI";
            this.buttonJadwalProduksi.UseVisualStyleBackColor = true;
            this.buttonJadwalProduksi.Click += new System.EventHandler(this.buttonJadwalProduksi_Click);
            // 
            // buttonProgress
            // 
            this.buttonProgress.Location = new System.Drawing.Point(115, 220);
            this.buttonProgress.Name = "buttonProgress";
            this.buttonProgress.Size = new System.Drawing.Size(93, 70);
            this.buttonProgress.TabIndex = 6;
            this.buttonProgress.Text = "PROGRESS PRODUKSI";
            this.buttonProgress.UseVisualStyleBackColor = true;
            this.buttonProgress.Click += new System.EventHandler(this.buttonProgress_Click);
            // 
            // buttonPengiriman
            // 
            this.buttonPengiriman.Location = new System.Drawing.Point(214, 220);
            this.buttonPengiriman.Name = "buttonPengiriman";
            this.buttonPengiriman.Size = new System.Drawing.Size(93, 70);
            this.buttonPengiriman.TabIndex = 7;
            this.buttonPengiriman.Text = "PENGIRIMAN BARANG JADI";
            this.buttonPengiriman.UseVisualStyleBackColor = true;
            // 
            // buttonPenerimaan
            // 
            this.buttonPenerimaan.Location = new System.Drawing.Point(313, 220);
            this.buttonPenerimaan.Name = "buttonPenerimaan";
            this.buttonPenerimaan.Size = new System.Drawing.Size(93, 70);
            this.buttonPenerimaan.TabIndex = 8;
            this.buttonPenerimaan.Text = "PENERIMAAN BARANG JADI";
            this.buttonPenerimaan.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(16, 220);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(93, 70);
            this.button1.TabIndex = 10;
            this.button1.Text = "TAMBAH BAHAN BAKU";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(16, 68);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(93, 70);
            this.button2.TabIndex = 11;
            this.button2.Text = "TAMBAH MESIN";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Admin_FormHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(421, 321);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonPenerimaan);
            this.Controls.Add(this.buttonPengiriman);
            this.Controls.Add(this.buttonProgress);
            this.Controls.Add(this.buttonJadwalProduksi);
            this.Controls.Add(this.buttonPenggunaan);
            this.Controls.Add(this.buttonPesanBahanBaku);
            this.Controls.Add(this.buttonSPK);
            this.Controls.Add(this.labelDate);
            this.Controls.Add(this.labelUser);
            this.Name = "Admin_FormHome";
            this.Text = "FormAdmin_Home";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormAdmin_Home_FormClosing);
            this.Load += new System.EventHandler(this.FormAdmin_Home_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelUser;
        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.Button buttonSPK;
        private System.Windows.Forms.Button buttonPesanBahanBaku;
        private System.Windows.Forms.Button buttonPenggunaan;
        private System.Windows.Forms.Button buttonJadwalProduksi;
        private System.Windows.Forms.Button buttonProgress;
        private System.Windows.Forms.Button buttonPengiriman;
        private System.Windows.Forms.Button buttonPenerimaan;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}