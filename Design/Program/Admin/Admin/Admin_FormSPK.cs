﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Admin
{
    public partial class Admin_FormSPK : Form
    {
        string admin_username;
        string admin_kategori;
        public Admin_FormSPK()
        {
            InitializeComponent();
            timer1.Start();
        }

        private void btnStatusSPK_Click(object sender, EventArgs e)
        {
            FormStatusSPK formStatusSPK = new FormStatusSPK();
            formStatusSPK.ShowDialog();
            this.Hide();
        }

        private void btnHapusSPK_Click(object sender, EventArgs e)
        {
            FormHapusSPK formHapusSPK = new FormHapusSPK();
            formHapusSPK.ShowDialog();
            this.Hide();
        }
        public Admin_FormSPK(string a, string b)
        {
            InitializeComponent();
            timer1.Start();
            this.admin_username = a;
            this.admin_kategori = b;
        }
        private void Admin_FormSPK_Load(object sender, EventArgs e)
        {
            labelUser.Text = admin_username + " - " + admin_kategori;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            this.labelDate.Text = dt.ToString();
        }
    }
}
