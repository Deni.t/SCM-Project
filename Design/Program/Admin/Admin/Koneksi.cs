﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Admin
{
    public class Koneksi
    {
        private MySqlConnection koneksi;
        private string namaServer;
        private string namaDatabase;
        private string username;
        private string password;

        #region Properties
        public MySqlConnection KoneksiDB
        {
            get
            {
                return koneksi;
            }

            set
            {
                koneksi = value;
            }
        }

        public string NamaServer
        {
            get
            {
                return namaServer;
            }

            set
            {
                namaServer = value;
            }
        }

        public string NamaDatabase
        {
            get
            {
                return namaDatabase;
            }

            set
            {
                namaDatabase = value;
            }
        }

        public string Username
        {
            get
            {
                return username;
            }

            set
            {
                username = value;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }

            set
            {
                password = value;
            }
        }
        #endregion

        #region Constructor
        public Koneksi()
        {
            //namaServer = "localhost";
            //namaDatabase = "scm";
            //username = "root";
            //password = "";
            koneksi = new MySqlConnection();

            //koneksi.ConnectionString = "server=" + namaServer +
            //    "; database= " + namaDatabase + "; uid=" + username + "; pwd=" + password;
            koneksi.ConnectionString = ConfigurationManager.ConnectionStrings["KonfigurasiKoneksi"].ConnectionString;

            string hasilConnect = Connect();

        }
        public Koneksi(string server, string db, string user, string pass)
        {
            namaServer = server;
            namaDatabase = db;
            username = user;
            password = pass;
            koneksi = new MySqlConnection();
            koneksi.ConnectionString = "server=" + namaServer +
                "; database= " + namaDatabase + "; uid=" + username + "; pwd=" + password;

            string hasilConnect = Connect();

            if(hasilConnect == "sukses")
            {
                updateAppConfig();
            }

        }
        #endregion

        #region Method
        public string Connect()
        {
            try
            {
                if(koneksi.State == System.Data.ConnectionState.Open)
                {
                    koneksi.Close();
                }
                koneksi.Open();
                return "sukses";
            }
            catch(Exception e)
            {
                return e.Message;
            }
        }
        public void updateAppConfig()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            config.ConnectionStrings.ConnectionStrings["KonfigurasiKoneksi"].ConnectionString = koneksi.ConnectionString;

            config.Save(ConfigurationSaveMode.Modified, true);

            ConfigurationManager.RefreshSection("connectionStrings");
        }
        #endregion
    }
}
