﻿namespace FormBuatSPK
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonBuatSPK = new System.Windows.Forms.Button();
            this.buttonStatusSPK = new System.Windows.Forms.Button();
            this.buttonHapusSPK = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonKembali = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.listBoxNama1 = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // buttonBuatSPK
            // 
            this.buttonBuatSPK.Location = new System.Drawing.Point(46, 105);
            this.buttonBuatSPK.Name = "buttonBuatSPK";
            this.buttonBuatSPK.Size = new System.Drawing.Size(259, 125);
            this.buttonBuatSPK.TabIndex = 0;
            this.buttonBuatSPK.Text = "Buat SPK";
            this.buttonBuatSPK.UseVisualStyleBackColor = true;
            this.buttonBuatSPK.Click += new System.EventHandler(this.buttonBuatSPK_Click);
            // 
            // buttonStatusSPK
            // 
            this.buttonStatusSPK.Location = new System.Drawing.Point(335, 105);
            this.buttonStatusSPK.Name = "buttonStatusSPK";
            this.buttonStatusSPK.Size = new System.Drawing.Size(259, 125);
            this.buttonStatusSPK.TabIndex = 1;
            this.buttonStatusSPK.Text = "Status SPK";
            this.buttonStatusSPK.UseVisualStyleBackColor = true;
            // 
            // buttonHapusSPK
            // 
            this.buttonHapusSPK.Location = new System.Drawing.Point(621, 105);
            this.buttonHapusSPK.Name = "buttonHapusSPK";
            this.buttonHapusSPK.Size = new System.Drawing.Size(259, 125);
            this.buttonHapusSPK.TabIndex = 2;
            this.buttonHapusSPK.Text = "Hapus SPK";
            this.buttonHapusSPK.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(43, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "SURAT PERINTAH KERJA";
            // 
            // buttonKembali
            // 
            this.buttonKembali.Location = new System.Drawing.Point(816, 366);
            this.buttonKembali.Name = "buttonKembali";
            this.buttonKembali.Size = new System.Drawing.Size(75, 23);
            this.buttonKembali.TabIndex = 5;
            this.buttonKembali.Text = "Kembali";
            this.buttonKembali.UseVisualStyleBackColor = true;
            this.buttonKembali.Click += new System.EventHandler(this.buttonKembali_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(12, 388);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(116, 20);
            this.dateTimePicker1.TabIndex = 6;
            // 
            // listBoxNama1
            // 
            this.listBoxNama1.FormattingEnabled = true;
            this.listBoxNama1.Location = new System.Drawing.Point(721, 12);
            this.listBoxNama1.Name = "listBoxNama1";
            this.listBoxNama1.Size = new System.Drawing.Size(184, 30);
            this.listBoxNama1.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(917, 430);
            this.Controls.Add(this.listBoxNama1);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.buttonKembali);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonHapusSPK);
            this.Controls.Add(this.buttonStatusSPK);
            this.Controls.Add(this.buttonBuatSPK);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonBuatSPK;
        private System.Windows.Forms.Button buttonStatusSPK;
        private System.Windows.Forms.Button buttonHapusSPK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonKembali;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ListBox listBoxNama1;
    }
}

