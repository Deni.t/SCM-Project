﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;
namespace formpesantambah
{
    public class PesanBahan
    {
        private string kodeBB;

        public string KodeBB
        {
            get { return kodeBB; }
            set { kodeBB = value; }
        }

        private int noSPK;

        public int NoSPK
        {
            get { return noSPK; }
            set { noSPK = value; }
        }
        private int jumlah;

        public int Jumlah
        {
            get { return jumlah; }
            set { jumlah = value; }
        }
        private string kodePemesanan;
        private string keterangan;

        public string Keterangan
        {
            get { return keterangan; }
            set { keterangan = value; }
        }

        public string KodePemesanan { get { return kodePemesanan; } set { kodePemesanan = value; } }

        public PesanBahan()
        {
            kodeBB = "";
            kodePemesanan = "";
            noSPK = 0;
            jumlah = 0;
            keterangan = "";
        }
        public PesanBahan(string kBB,string kP, int nSPK, int jml, string ket)
        {
            kodeBB = kBB;
            kodePemesanan = kP;
            noSPK = nSPK;
            jumlah = jml;
            keterangan = ket;
        }
        public string TambahData(PesanBahan tb)
        {
            Koneksi1 k = new Koneksi1();
            k.Connect();

            string sql = "INSERT INTO formpemesanandetil(kodeBahanBaku, kodePemesanan, noSPK, jumlah, keterangan, statusKirim) VALUES ('" + kodeBB + "','" + kodePemesanan + "','" + noSPK + "','" + jumlah + "','" + keterangan + "','')";

            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB);

            try
            {
                c.ExecuteNonQuery();
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

    }
}
