﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;
namespace formpesantambah
{
    public class DTambah
    {
        private List<TambahBahan> listTambah;

        public List<TambahBahan> ListTambah { get { return listTambah; } }

        public int JumlahBarang
        {
            get { return listTambah.Count; }
        }
        public DTambah()
        {
            listTambah = new List<TambahBahan>();
        }
        public string BacaSemuaData()
        {
            Koneksi1 k = new Koneksi1();
            k.Connect();
            string sql = "Select * From bahanbaku";
            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB);
            try
            {
                MySqlDataReader data = c.ExecuteReader();

                while (data.Read() == true)
                {
                    string kode = data.GetValue(0).ToString();
                    string nama = data.GetValue(1).ToString();
                    string satuan = data.GetValue(2).ToString();
                    
                    TambahBahan tb = new TambahBahan(kode, nama, satuan);
                    listTambah.Add(tb);
                }
                c.Dispose();
                data.Dispose();

                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        public string TambahData(TambahBahan tb)
        {
            Koneksi1 k = new Koneksi1();
            k.Connect();

            string sql = "INSERT INTO bahanbaku(kodeBahanBaku, nama, satuan, stok) VALUES ('" + tb.Kode + "','" + tb.Nama + "','" + tb.Satuan + "','" + "0" + "')";

            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB);

            try
            {
                c.ExecuteNonQuery();
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }

        }
    }
}
