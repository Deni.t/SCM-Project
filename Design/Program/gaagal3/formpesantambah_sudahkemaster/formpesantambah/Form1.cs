﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace formpesantambah
{
    public partial class Form1 : Form
    {
        public Koneksi1 k;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            textBoxid.Text = "root";
            textBoxpass.Text = "";
            textBoxServer.Text = "localhost";
            textBoxDatabase.Text = "scm";
            textBoxid.Enabled = false;
            textBoxpass.Enabled = false;
            textBoxServer.Enabled = false;
            textBoxDatabase.Enabled = false;
        }

        private void buttonEnter_Click(object sender, EventArgs e)
        {
            if (textBoxid.Text != "")
            {

                k = new Koneksi1(textBoxServer.Text, textBoxDatabase.Text, textBoxid.Text, textBoxpass.Text);
                string hasilConnect = k.Connect();

                if (hasilConnect == "sukses")
                {
                    MessageBox.Show("...", "Info");
                    FormPesan frmpsn = new FormPesan();
                    frmpsn.Owner = this;
                    frmpsn.Show();
                }
                else
                {
                    MessageBox.Show("Koneksi gagal. Pesan Kesalahan : " + hasilConnect, "Kesalahan");
                }
            }
            else
            {
                MessageBox.Show("Username tidak boleh dikosongi", "Kesalahan");
            }
        }
    }
}
