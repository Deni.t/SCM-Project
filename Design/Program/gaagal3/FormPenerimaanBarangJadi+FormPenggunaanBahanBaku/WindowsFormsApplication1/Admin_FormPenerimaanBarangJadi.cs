﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
    public partial class Admin_FormPenerimaanBarangJadi : Form
    {
        string admin_username;
        string admin_kategori;
        public Admin_FormPenerimaanBarangJadi()
        {
            InitializeComponent();
        }

        private void Admin_FormPenerimaanBarangJadi_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            labelDateTime.Text = DateTime.Now.ToString("yyyy-MM-dd/hh:mm:ss");
            labelUser.Text = admin_username + " - " + admin_kategori;
        }

        private void buttonKembali_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonCetak_Click(object sender, EventArgs e)
        {
            var loadString = @"server=localhost;database=scm;userid=root;password=;";
            using (var connection = new MySqlConnection(loadString))
            {
                connection.Open();
                var query1 = "SELECT c.nama as nama,c.alamat as alamat,s.kodeProduk as kode,s.biaya as biaya,fp.pembayaranSatu as pembayaranSatu,fp.pembayaranDua as pembayaranDua,fp.kekurangan as kekurangan,fp.caraPembayaranSatu as caraPembayaran from spk s INNER JOIN customer c on s.kodeCustomer=c.kodeCustomer INNER JOIN formpembayaran fp on s.noSPK=fp.noSPK WHERE s.noSPK='" + comboBoxSPK.Text + "'";
                using (var command1 = new MySqlCommand(query1, connection))
                {
                    using (var reader1 = command1.ExecuteReader())
                    {
                        string kode;
                        while (reader1.Read())
                        {
                            listBoxInfo.Items.Add("Nama : " + reader1.GetString("nama"));
                            listBoxInfo.Items.Add("Alamat : " + reader1.GetString("alamat"));
                            listBoxInfo.Items.Add("Kode Produk : " + reader1.GetString("kode"));
                            listBoxInfo.Items.Add("Total Harga : " + reader1.GetString("biaya"));
                            listBoxInfo.Items.Add("Pembayaran1 : " + reader1.GetString("pembayaranSatu"));
                            if (reader1.GetString("pembayaranDua") == "NULL")
                            {
                                listBoxInfo.Items.Add("Pembayaran2 : -");
                            }
                            else
                            {
                                listBoxInfo.Items.Add("Pembayaran2 : " + reader1.GetString("pembayaranDua"));
                            }
                            listBoxInfo.Items.Add("Kekurangan : " + reader1.GetString("kekurangan"));
                            listBoxInfo.Items.Add("Cara pembayaran : " + reader1.GetString("caraPembayaran"));
                            listBoxInfo.Items.Add("Tanggal Penerimaan : " + dateTimePickerTanggal.Value);
                        }
                    }
                }
            }
        }
    }
}
