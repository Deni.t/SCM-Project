﻿namespace Admin
{
    partial class Admin_FormTambahMesin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblUser_A_FTM = new System.Windows.Forms.Label();
            this.lblDateTime_A_FTM = new System.Windows.Forms.Label();
            this.btnKembali_A_FTM = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSimpan_A_FTM = new System.Windows.Forms.Button();
            this.txtNamaMesin_A_FTM = new System.Windows.Forms.TextBox();
            this.lblNamaMesin_A_FTM = new System.Windows.Forms.Label();
            this.timer1_A_FTM = new System.Windows.Forms.Timer(this.components);
            this.txtNoMesin_A_FTM = new System.Windows.Forms.TextBox();
            this.lblNoMesin_A_FTM = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblUser_A_FTM
            // 
            this.lblUser_A_FTM.AutoSize = true;
            this.lblUser_A_FTM.Location = new System.Drawing.Point(311, 19);
            this.lblUser_A_FTM.Name = "lblUser_A_FTM";
            this.lblUser_A_FTM.Size = new System.Drawing.Size(29, 13);
            this.lblUser_A_FTM.TabIndex = 5;
            this.lblUser_A_FTM.Text = "User";
            // 
            // lblDateTime_A_FTM
            // 
            this.lblDateTime_A_FTM.AutoSize = true;
            this.lblDateTime_A_FTM.Location = new System.Drawing.Point(20, 19);
            this.lblDateTime_A_FTM.Name = "lblDateTime_A_FTM";
            this.lblDateTime_A_FTM.Size = new System.Drawing.Size(58, 13);
            this.lblDateTime_A_FTM.TabIndex = 4;
            this.lblDateTime_A_FTM.Text = "Date/Time";
            // 
            // btnKembali_A_FTM
            // 
            this.btnKembali_A_FTM.Location = new System.Drawing.Point(272, 191);
            this.btnKembali_A_FTM.Name = "btnKembali_A_FTM";
            this.btnKembali_A_FTM.Size = new System.Drawing.Size(68, 41);
            this.btnKembali_A_FTM.TabIndex = 6;
            this.btnKembali_A_FTM.Text = "KEMBALI";
            this.btnKembali_A_FTM.UseVisualStyleBackColor = true;
            this.btnKembali_A_FTM.Click += new System.EventHandler(this.btnKembali_A_FTM_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtNoMesin_A_FTM);
            this.groupBox1.Controls.Add(this.lblNoMesin_A_FTM);
            this.groupBox1.Controls.Add(this.btnSimpan_A_FTM);
            this.groupBox1.Controls.Add(this.txtNamaMesin_A_FTM);
            this.groupBox1.Controls.Add(this.lblNamaMesin_A_FTM);
            this.groupBox1.Location = new System.Drawing.Point(23, 57);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(317, 128);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tambah Mesin";
            // 
            // btnSimpan_A_FTM
            // 
            this.btnSimpan_A_FTM.Location = new System.Drawing.Point(145, 91);
            this.btnSimpan_A_FTM.Name = "btnSimpan_A_FTM";
            this.btnSimpan_A_FTM.Size = new System.Drawing.Size(75, 23);
            this.btnSimpan_A_FTM.TabIndex = 2;
            this.btnSimpan_A_FTM.Text = "SIMPAN";
            this.btnSimpan_A_FTM.UseVisualStyleBackColor = true;
            this.btnSimpan_A_FTM.Click += new System.EventHandler(this.btnSimpan_A_FTM_Click);
            // 
            // txtNamaMesin_A_FTM
            // 
            this.txtNamaMesin_A_FTM.Location = new System.Drawing.Point(120, 59);
            this.txtNamaMesin_A_FTM.Name = "txtNamaMesin_A_FTM";
            this.txtNamaMesin_A_FTM.Size = new System.Drawing.Size(100, 20);
            this.txtNamaMesin_A_FTM.TabIndex = 1;
            // 
            // lblNamaMesin_A_FTM
            // 
            this.lblNamaMesin_A_FTM.AutoSize = true;
            this.lblNamaMesin_A_FTM.Location = new System.Drawing.Point(39, 59);
            this.lblNamaMesin_A_FTM.Name = "lblNamaMesin_A_FTM";
            this.lblNamaMesin_A_FTM.Size = new System.Drawing.Size(75, 13);
            this.lblNamaMesin_A_FTM.TabIndex = 0;
            this.lblNamaMesin_A_FTM.Text = "Nama Mesin : ";
            // 
            // timer1_A_FTM
            // 
            this.timer1_A_FTM.Tick += new System.EventHandler(this.timer1_A_FTM_Tick);
            // 
            // txtNoMesin_A_FTM
            // 
            this.txtNoMesin_A_FTM.Location = new System.Drawing.Point(120, 34);
            this.txtNoMesin_A_FTM.Name = "txtNoMesin_A_FTM";
            this.txtNoMesin_A_FTM.Size = new System.Drawing.Size(100, 20);
            this.txtNoMesin_A_FTM.TabIndex = 4;
            // 
            // lblNoMesin_A_FTM
            // 
            this.lblNoMesin_A_FTM.AutoSize = true;
            this.lblNoMesin_A_FTM.Location = new System.Drawing.Point(39, 34);
            this.lblNoMesin_A_FTM.Name = "lblNoMesin_A_FTM";
            this.lblNoMesin_A_FTM.Size = new System.Drawing.Size(61, 13);
            this.lblNoMesin_A_FTM.TabIndex = 3;
            this.lblNoMesin_A_FTM.Text = "No Mesin : ";
            // 
            // Admin_FormTambahMesin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(365, 244);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnKembali_A_FTM);
            this.Controls.Add(this.lblUser_A_FTM);
            this.Controls.Add(this.lblDateTime_A_FTM);
            this.Name = "Admin_FormTambahMesin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Admin_FormTambahMesin";
            this.Load += new System.EventHandler(this.Admin_FormTambahMesin_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUser_A_FTM;
        private System.Windows.Forms.Label lblDateTime_A_FTM;
        private System.Windows.Forms.Button btnKembali_A_FTM;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSimpan_A_FTM;
        private System.Windows.Forms.TextBox txtNamaMesin_A_FTM;
        private System.Windows.Forms.Label lblNamaMesin_A_FTM;
        private System.Windows.Forms.Timer timer1_A_FTM;
        private System.Windows.Forms.TextBox txtNoMesin_A_FTM;
        private System.Windows.Forms.Label lblNoMesin_A_FTM;
    }
}