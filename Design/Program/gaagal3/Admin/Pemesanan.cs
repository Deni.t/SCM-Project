﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Admin
{
    public class Pemesanan
    {
        private string kodePemesanan;

        public string KodePemesanan
        {
            get { return kodePemesanan; }
            set { kodePemesanan = value; }
        }
        private string noSPK;

        public string NoSPK
        {
            get { return noSPK; }
            set { noSPK = value; }
        }
        private DateTime tanggal;

        public DateTime Tanggal
        {
            get { return tanggal; }
            set { tanggal = value; }
        }

        //public string KodePemesanan { get => kodePemesanan; set => kodePemesanan = value; }
        //public string NoSPK { get => noSPK; set => noSPK = value; }
        //public DateTime Tanggal { get => tanggal; set => tanggal = value; }



        public Pemesanan()
        {
            kodePemesanan = "";
            noSPK = "";
            tanggal = new DateTime();


        }
        public Pemesanan(string kP, string nSPK, DateTime tgl)
        {
            kodePemesanan = kP;
            noSPK = nSPK;
            tanggal = tgl;
        }

    }
}
