﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin
{
    public partial class ProgresProduksi : Form
    {
        string username, kategori;
        public ProgresProduksi(string u, string k)
        {
            InitializeComponent();
            this.username = u;
            this.kategori = k;
        }

        private void buttonPenerimaan_Click(object sender, EventArgs e)
        {
            RiwayatProgres ri = new RiwayatProgres();
            ri.ShowDialog();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Admin_FormHome fh = new Admin_FormHome(username, kategori);
            fh.ShowDialog();
        }

        private void ProgresProduksi_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            label2.Text = username + " - " + kategori;
        }

        private void buttonPengiriman_Click(object sender, EventArgs e)
        {
            this.Hide();
            TambahProgress tp = new TambahProgress(username, kategori);
            tp.ShowDialog();
        }
    }
}
