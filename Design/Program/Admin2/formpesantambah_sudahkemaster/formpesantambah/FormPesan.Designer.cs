﻿namespace formpesantambah
{
    partial class FormPesan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonKembali = new System.Windows.Forms.Button();
            this.buttonPesan = new System.Windows.Forms.Button();
            this.buttonHapus = new System.Windows.Forms.Button();
            this.buttonSubmit = new System.Windows.Forms.Button();
            this.buttonTambahBahan = new System.Windows.Forms.Button();
            this.textBoxKeterangan = new System.Windows.Forms.TextBox();
            this.textBoxJumlah = new System.Windows.Forms.TextBox();
            this.comboBoxID = new System.Windows.Forms.ComboBox();
            this.comboBoxNoSpk = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelUser = new System.Windows.Forms.Label();
            this.labelTgl = new System.Windows.Forms.Label();
            this.labelJam = new System.Windows.Forms.Label();
            this.labelkode = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(-64, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Keterangan:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(-64, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Jumlah:";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(50, 267);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(532, 102);
            this.dataGridView1.TabIndex = 31;
            // 
            // buttonKembali
            // 
            this.buttonKembali.Location = new System.Drawing.Point(524, 431);
            this.buttonKembali.Name = "buttonKembali";
            this.buttonKembali.Size = new System.Drawing.Size(75, 23);
            this.buttonKembali.TabIndex = 30;
            this.buttonKembali.Text = "KEMBALI";
            this.buttonKembali.UseVisualStyleBackColor = true;
            this.buttonKembali.Click += new System.EventHandler(this.buttonKembali_Click);
            // 
            // buttonPesan
            // 
            this.buttonPesan.Location = new System.Drawing.Point(171, 425);
            this.buttonPesan.Name = "buttonPesan";
            this.buttonPesan.Size = new System.Drawing.Size(213, 35);
            this.buttonPesan.TabIndex = 29;
            this.buttonPesan.Text = "PESAN";
            this.buttonPesan.UseVisualStyleBackColor = true;
            this.buttonPesan.UseWaitCursor = true;
            this.buttonPesan.Click += new System.EventHandler(this.buttonPesan_Click);
            // 
            // buttonHapus
            // 
            this.buttonHapus.Location = new System.Drawing.Point(524, 231);
            this.buttonHapus.Name = "buttonHapus";
            this.buttonHapus.Size = new System.Drawing.Size(75, 23);
            this.buttonHapus.TabIndex = 28;
            this.buttonHapus.Text = "HAPUS";
            this.buttonHapus.UseVisualStyleBackColor = true;
            this.buttonHapus.Click += new System.EventHandler(this.buttonHapus_Click);
            // 
            // buttonSubmit
            // 
            this.buttonSubmit.Location = new System.Drawing.Point(431, 232);
            this.buttonSubmit.Name = "buttonSubmit";
            this.buttonSubmit.Size = new System.Drawing.Size(75, 23);
            this.buttonSubmit.TabIndex = 27;
            this.buttonSubmit.Text = "SUBMIT";
            this.buttonSubmit.UseVisualStyleBackColor = true;
            this.buttonSubmit.Click += new System.EventHandler(this.buttonSubmit_Click);
            // 
            // buttonTambahBahan
            // 
            this.buttonTambahBahan.Location = new System.Drawing.Point(451, 146);
            this.buttonTambahBahan.Name = "buttonTambahBahan";
            this.buttonTambahBahan.Size = new System.Drawing.Size(120, 41);
            this.buttonTambahBahan.TabIndex = 26;
            this.buttonTambahBahan.Text = "TAMBAH BAHAN";
            this.buttonTambahBahan.UseVisualStyleBackColor = true;
            this.buttonTambahBahan.Click += new System.EventHandler(this.buttonTambahBahan_Click);
            // 
            // textBoxKeterangan
            // 
            this.textBoxKeterangan.Location = new System.Drawing.Point(244, 233);
            this.textBoxKeterangan.Name = "textBoxKeterangan";
            this.textBoxKeterangan.Size = new System.Drawing.Size(161, 20);
            this.textBoxKeterangan.TabIndex = 25;
            // 
            // textBoxJumlah
            // 
            this.textBoxJumlah.Location = new System.Drawing.Point(244, 205);
            this.textBoxJumlah.Name = "textBoxJumlah";
            this.textBoxJumlah.Size = new System.Drawing.Size(88, 20);
            this.textBoxJumlah.TabIndex = 24;
            // 
            // comboBoxID
            // 
            this.comboBoxID.FormattingEnabled = true;
            this.comboBoxID.Location = new System.Drawing.Point(305, 157);
            this.comboBoxID.Name = "comboBoxID";
            this.comboBoxID.Size = new System.Drawing.Size(121, 21);
            this.comboBoxID.TabIndex = 23;
            // 
            // comboBoxNoSpk
            // 
            this.comboBoxNoSpk.FormattingEnabled = true;
            this.comboBoxNoSpk.Location = new System.Drawing.Point(305, 129);
            this.comboBoxNoSpk.Name = "comboBoxNoSpk";
            this.comboBoxNoSpk.Size = new System.Drawing.Size(121, 21);
            this.comboBoxNoSpk.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(159, 236);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "Keterangan:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(159, 208);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Jumlah:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(159, 160);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(133, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "ID - NAMA BAHAN BAKU:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(159, 132);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(139, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "No. SPK - KODE PRODUK:";
            // 
            // labelUser
            // 
            this.labelUser.AutoSize = true;
            this.labelUser.Location = new System.Drawing.Point(451, 13);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(29, 13);
            this.labelUser.TabIndex = 32;
            this.labelUser.Text = "User";
            // 
            // labelTgl
            // 
            this.labelTgl.AutoSize = true;
            this.labelTgl.BackColor = System.Drawing.Color.Transparent;
            this.labelTgl.Location = new System.Drawing.Point(1, 457);
            this.labelTgl.Name = "labelTgl";
            this.labelTgl.Size = new System.Drawing.Size(22, 13);
            this.labelTgl.TabIndex = 37;
            this.labelTgl.Text = "Tgl";
            // 
            // labelJam
            // 
            this.labelJam.AutoSize = true;
            this.labelJam.BackColor = System.Drawing.Color.Transparent;
            this.labelJam.Location = new System.Drawing.Point(16, 436);
            this.labelJam.Name = "labelJam";
            this.labelJam.Size = new System.Drawing.Size(26, 13);
            this.labelJam.TabIndex = 38;
            this.labelJam.Text = "Jam";
            // 
            // labelkode
            // 
            this.labelkode.AutoSize = true;
            this.labelkode.Location = new System.Drawing.Point(379, 29);
            this.labelkode.Name = "labelkode";
            this.labelkode.Size = new System.Drawing.Size(0, 13);
            this.labelkode.TabIndex = 39;
            this.labelkode.Visible = false;
            // 
            // FormPesan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::formpesantambah.Properties.Resources.Admin_Pesan;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(626, 476);
            this.Controls.Add(this.labelkode);
            this.Controls.Add(this.labelTgl);
            this.Controls.Add(this.labelJam);
            this.Controls.Add(this.labelUser);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.buttonKembali);
            this.Controls.Add(this.buttonPesan);
            this.Controls.Add(this.buttonHapus);
            this.Controls.Add(this.buttonSubmit);
            this.Controls.Add(this.buttonTambahBahan);
            this.Controls.Add(this.textBoxKeterangan);
            this.Controls.Add(this.textBoxJumlah);
            this.Controls.Add(this.comboBoxID);
            this.Controls.Add(this.comboBoxNoSpk);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Name = "FormPesan";
            this.Text = "FormPesan";
            this.Load += new System.EventHandler(this.FormPesan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonKembali;
        private System.Windows.Forms.Button buttonPesan;
        private System.Windows.Forms.Button buttonHapus;
        private System.Windows.Forms.Button buttonSubmit;
        private System.Windows.Forms.Button buttonTambahBahan;
        private System.Windows.Forms.TextBox textBoxKeterangan;
        private System.Windows.Forms.TextBox textBoxJumlah;
        private System.Windows.Forms.ComboBox comboBoxID;
        private System.Windows.Forms.ComboBox comboBoxNoSpk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelUser;
        private System.Windows.Forms.Label labelTgl;
        private System.Windows.Forms.Label labelJam;
        private System.Windows.Forms.Label labelkode;
    }
}