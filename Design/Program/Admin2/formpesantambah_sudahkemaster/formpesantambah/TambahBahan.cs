﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;
namespace formpesantambah
{
    public class TambahBahan
    {
        private string kode;

        public string Kode
        {
            get { return kode; }
            set { kode = value; }
        }
        private string nama;

        public string Nama
        {
            get { return nama; }
            set { nama = value; }
        }
        private string satuan;

        public string Satuan
        {
            get { return satuan; }
            set { satuan = value; }
        }

        

        public TambahBahan()
        {
            kode = "";
            nama = "";
            satuan = "";
            
        }
        public TambahBahan(string Kode,string Nama, string Satuan)
        {
            kode = Kode;
            nama = Nama;
            satuan = Satuan;
            
        }
    }
}
