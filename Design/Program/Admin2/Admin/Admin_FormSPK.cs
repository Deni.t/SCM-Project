﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin
{
    public partial class Admin_FormSPK : Form
    {
        public Admin_FormSPK()
        {
            InitializeComponent();
        }

        private void btnStatusSPK_Click(object sender, EventArgs e)
        {
            FormStatusSPK formStatusSPK = new FormStatusSPK();
            formStatusSPK.ShowDialog();
            this.Hide();
        }

        private void btnHapusSPK_Click(object sender, EventArgs e)
        {
            FormHapusSPK formHapusSPK = new FormHapusSPK();
            formHapusSPK.ShowDialog();
            this.Hide();
        }
    }
}
