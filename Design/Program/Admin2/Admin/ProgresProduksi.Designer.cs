﻿namespace Admin
{
    partial class ProgresProduksi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonPenerimaan = new System.Windows.Forms.Button();
            this.buttonPengiriman = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonPenerimaan
            // 
            this.buttonPenerimaan.Location = new System.Drawing.Point(232, 219);
            this.buttonPenerimaan.Name = "buttonPenerimaan";
            this.buttonPenerimaan.Size = new System.Drawing.Size(93, 70);
            this.buttonPenerimaan.TabIndex = 20;
            this.buttonPenerimaan.Text = "RIWAYAT PROGRES";
            this.buttonPenerimaan.UseVisualStyleBackColor = true;
            this.buttonPenerimaan.Click += new System.EventHandler(this.buttonPenerimaan_Click);
            // 
            // buttonPengiriman
            // 
            this.buttonPengiriman.Location = new System.Drawing.Point(133, 219);
            this.buttonPengiriman.Name = "buttonPengiriman";
            this.buttonPengiriman.Size = new System.Drawing.Size(93, 70);
            this.buttonPengiriman.TabIndex = 19;
            this.buttonPengiriman.Text = "TAMBAH PROGRES";
            this.buttonPengiriman.UseVisualStyleBackColor = true;
            this.buttonPengiriman.Click += new System.EventHandler(this.buttonPengiriman_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(434, 42);
            this.label1.TabIndex = 17;
            this.label1.Text = "PROGRESS PRODUKSI";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(335, 345);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(107, 33);
            this.button1.TabIndex = 18;
            this.button1.Text = "Kembali";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(345, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "label2";
            // 
            // ProgresProduksi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 390);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonPenerimaan);
            this.Controls.Add(this.buttonPengiriman);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Name = "ProgresProduksi";
            this.Text = "ProgresProduksi";
            this.Load += new System.EventHandler(this.ProgresProduksi_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonPenerimaan;
        private System.Windows.Forms.Button buttonPengiriman;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
    }
}