﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data.OleDb;


namespace Admin
{
    public partial class FormConnect : Form
    {
        MySqlConnection conn = connectionService.getConnection();
        public static string kategori_user = "";
        public static string username = "";
        public FormConnect()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string loadstring = @"server=localhost;database=scm;userid=root;password=;";
            MySqlConnection conDataBase = new MySqlConnection(loadstring);
            string sql = ("SELECT u.username, j.namaJabatan from userti u inner join jabatan j on u.kodeJabatan = j.kodeJabatan  where u.username='" + textBoxUsername.Text + "' and u.password='"
                + textBoxPassword.Text + "'");
            MySqlDataAdapter da = new MySqlDataAdapter(sql, conDataBase);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count == 0)
            {
                MessageBox.Show(textBoxUsername.Text + " is not exist.", "Not Exists");
            }
            else
            {
                username = (dt.Rows[0][0]).ToString();
                kategori_user = (dt.Rows[0][1]).ToString();
                Admin_FormHome home = new Admin_FormHome(username, kategori_user);
                this.Hide();
                home.ShowDialog();
                MessageBox.Show(kategori_user);
            }
        }

        private void FormConnect_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
        }
    }
}

