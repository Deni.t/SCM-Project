﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.IO;
namespace formpesantambah
{
    public class DPesan
    {
        private List<PesanBahan> listPesan;
        private string kodeTerbaru;
        public List<PesanBahan> ListPesan
        { get { return listPesan; } }

        public int JumlahBarang
        {
            get { return listPesan.Count; }
        }
        public string KodeTerbaru
        {
            get { return kodeTerbaru; }
        }
        public DPesan()
        {
            listPesan = new List<PesanBahan>();
            kodeTerbaru = "pemesanan01";
        }
        public string BacaSemuaData()
        {
            Koneksi1 k = new Koneksi1();
            k.Connect();
            string sql = "Select * From formpemesanandetil";
            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB);
            try
            {
                MySqlDataReader data = c.ExecuteReader();

                while (data.Read() == true)
                {

                    string kbahan = data.GetValue(0).ToString();
                    string kPesan = data.GetValue(1).ToString();
                    string noSPK = data.GetValue(2).ToString();
                    string jml = data.GetValue(3).ToString();
                    string keterangan = data.GetValue(4).ToString();
                    PesanBahan pb = new PesanBahan(kbahan, kPesan, int.Parse(noSPK), int.Parse(jml), keterangan);
                    listPesan.Add(pb);
                }
                c.Dispose();
                data.Dispose();

                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        public string GenerateKode()
        {
            Koneksi1 k = new Koneksi1();
            k.Connect();

            string sql = "SELECT SUBSTRING(kodePemesanan,10,11) FROM formPemesanan ORDER BY kodePemesanan DESC LIMIT 1";
            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB);

            try
            {
                MySqlDataReader data = c.ExecuteReader();
                if (data.Read() == true)
                {
                    int kdTerbaru = int.Parse(data.GetValue(0).ToString()) + 1;
                    kodeTerbaru = kdTerbaru.ToString();
                    
                    if (kodeTerbaru.Length == 1)
                    {
                        kodeTerbaru = "pemesanan0" + kodeTerbaru;
                    }
                    if (kodeTerbaru.Length==2)
                    {
                        kodeTerbaru = "pemesanan" + kodeTerbaru;
                    }
                    
                }
                c.Dispose();
                data.Dispose();

                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        public string TambahData(Pemesanan nota)
        {
            Koneksi1 k = new Koneksi1();
            k.Connect();

            string sql = "INSERT INTO formpemesanan(kodePemesanan, noSPK, tanggalPesan) Values ('" + nota.KodePemesanan + "','" +
                             nota.NoSPK + "','" + nota.Tanggal + "')";
            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB);

            try
            {
                c.ExecuteNonQuery();
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }

        }
    }
}


