﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Admin
{
    public class Koneksi1
    {
        
        #region DATA MEMBER
        private MySqlConnection koneksi;
        private string namaServer;
        private string namaDatabase;
        private string username;
        private string password;
        #endregion

        #region PROPERTIES
        public string NamaServer
        {
            get { return namaServer; }
            set { namaServer = value; }
        }

        public string NamaDatabase
        {
            get { return namaDatabase; }
            set { namaDatabase = value; }
        }

        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        public string Password
        {
            set { password = value; }
        }
        public MySqlConnection KoneksiDB
        {
            get { return koneksi; }
        }
        #endregion

        #region METHOD
        public string Connect()
        {
            try
            {
                if (koneksi.State == System.Data.ConnectionState.Open)
                {
                    koneksi.Close();
                }
                koneksi.Open();
                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public void UpdateAppConfig(string connectionString)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            config.ConnectionStrings.ConnectionStrings["KonfigurasiKoneksi"].ConnectionString = connectionString;

            config.Save(ConfigurationSaveMode.Modified, true);

            ConfigurationManager.RefreshSection("connectionStrings");
        }
        #endregion

        #region CONSTRUCTOR
        public Koneksi1()
        {
            koneksi = new MySqlConnection();
            koneksi.ConnectionString = ConfigurationManager.ConnectionStrings["KonfigurasiKoneksi"].ConnectionString;
            string hasilConnect = Connect();
        }
        public Koneksi1(string server, string database, string user, string pwd)
        {
            namaServer = server;
            namaDatabase = database;
            username = user;
            password = pwd;

            string strCon = "server=" + namaServer + "; database=" + namaDatabase + "; userid=" + username + "; password=" + password;
            koneksi = new MySqlConnection();
            koneksi.ConnectionString = strCon;
            string hasilConnect = Connect();

            if (hasilConnect == "sukses")
            {
                UpdateAppConfig(strCon);
            }
        }
        #endregion
    }
}
