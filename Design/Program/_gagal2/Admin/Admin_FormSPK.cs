﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin
{
    public partial class Admin_FormSPK : Form
    {
        string u, k;
        public Admin_FormSPK(string u,string k)
        {
            InitializeComponent();
            this.u = u;
            this.k = k;
        }

        private void btnStatusSPK_Click(object sender, EventArgs e)
        {
            FormStatusSPK formStatusSPK = new FormStatusSPK(u,k);
            formStatusSPK.ShowDialog();
            this.Hide();
        }

        private void btnHapusSPK_Click(object sender, EventArgs e)
        {
            FormHapusSPK formHapusSPK = new FormHapusSPK();
            formHapusSPK.ShowDialog();
            this.Hide();
        }

        private void Admin_FormSPK_Load(object sender, EventArgs e)
        {
            label1.Text = u + " - " + k;
        }
    }
}
